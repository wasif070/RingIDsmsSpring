package org.ipvision;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SMSApplication {

    private static Logger log = Logger.getLogger("vodCustomLogger");

    public static void main(String[] args) {
        ApplicationContext ctx = (ApplicationContext) SpringApplication.run(SMSApplication.class, args);
        System.out.println("TOTAL BEANS: " + ctx.getBeanDefinitionCount());
        log.info("Application Started : " + SMSApplication.class);
    }
}
