package org.ipvision.service;

import java.util.ArrayList;
import java.util.List;

import org.ipvision.constant.DTableOperationType;
import org.ipvision.domain.Country;
import org.ipvision.domain.SMSRoute;
import org.ipvision.dto.CountryDTO;
import org.ipvision.dto.SMSRouteDTO;
import org.ipvision.repository.SMSRouteRepository;
import org.ipvision.utils.MyAppError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service("smsRouteService")
public class SMSRouteService implements ISMSRouteService {

	private SMSRouteRepository smsRouteRepository;

	@Autowired
	public void setSmsRouteRepository(SMSRouteRepository smsRouteRepository) {
		Assert.notNull(smsRouteRepository);
		this.smsRouteRepository = smsRouteRepository;
	}

	private SMSRouteDTO findSelection(SMSRouteDTO pageSMSRouteDTO) {
		int page = (int) (pageSMSRouteDTO.getViewablePage() - 1);
		int size = pageSMSRouteDTO.getViewableRows().intValue();
		System.out.println("PAGE: " + page + " SIZE: " + size);
		Page<SMSRoute> pageSMSRoutes = smsRouteRepository.findAll(new PageRequest(page, size));
		pageSMSRouteDTO.setListItems(pageSMSRoutes.getContent());

		return pageSMSRouteDTO;
	}

	@Override
	public SMSRouteDTO findPageableDTO(Long vRows, Long vPage, String searchKey, DTableOperationType operation,
			Long goPage) {
		// TODO Auto-generated method stub
		System.out.println("vRows: " + vRows);
		System.out.println("vPage: " + vPage);
		System.out.println("searchKey: " + searchKey);
		System.out.println("goPageNo: " + goPage);

		SMSRouteDTO pageSMSRouteDTO;
		
		if (operation == DTableOperationType.SELECT) {

			vPage = 1L;
			goPage = 1L;
		}
		
		if (vRows != null) {
			pageSMSRouteDTO = new SMSRouteDTO(vRows);
		} else {
			pageSMSRouteDTO = new SMSRouteDTO();
		}

		pageSMSRouteDTO.setTotalRows(smsRouteRepository.count());
		pageSMSRouteDTO.setTotalPages(
				(long) Math.ceil(pageSMSRouteDTO.getTotalRows() * 1.0 / pageSMSRouteDTO.getViewableRows()));

		if (operation != null) {
			pageSMSRouteDTO.setOperation(operation);
			if (operation == DTableOperationType.NEXT) {
				Long page = pageSMSRouteDTO.getViewablePage() + 1;
				page = page > pageSMSRouteDTO.getTotalPages() ? page - 1 : page; // Last
				// page
				// checking
				pageSMSRouteDTO.setViewablePage(page);
				pageSMSRouteDTO = findSelection(pageSMSRouteDTO);

			} else if (operation == DTableOperationType.PREV) {
				Long page = pageSMSRouteDTO.getViewablePage() - 1;

				page = page == 0 ? 1 : page; // First Page checking

				pageSMSRouteDTO.setViewablePage(page);
				pageSMSRouteDTO = findSelection(pageSMSRouteDTO);

			} else if (operation == DTableOperationType.GO_PAGE) {

				pageSMSRouteDTO.setViewablePage(goPage);
				pageSMSRouteDTO = findSelection(pageSMSRouteDTO);

			} else if (operation == DTableOperationType.SELECT || searchKey.equals("")) {
				pageSMSRouteDTO = findSelection(pageSMSRouteDTO);
			} else {
				pageSMSRouteDTO.setTotalPages(1L); // All search result shown in
													// one
													// page
				pageSMSRouteDTO.setSearchKey(searchKey);
				List<SMSRoute> smsRoutes = smsRouteRepository.findSMSRoutesBySearchKey(searchKey.toLowerCase());
				pageSMSRouteDTO.setListItems(smsRoutes);
			}

		} else {
			pageSMSRouteDTO = findSelection(pageSMSRouteDTO);
		}

		return pageSMSRouteDTO;
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		smsRouteRepository.delete(id);
	}

	@Override
	public SMSRoute findById(Long id) {
		// TODO Auto-generated method stub
		return smsRouteRepository.findOne(id);
	}

	@Override
	public SMSRoute save(SMSRoute smsRoute) {
		// TODO Auto-generated method stub
		return smsRouteRepository.save(smsRoute);
	}

	@Override
	public List<SMSRoute> findAll() {
		// TODO Auto-generated method stub
		return smsRouteRepository.findAll();
	}
	
	@Override
    public Boolean existsSmsRoute(Long id, String brandName) {
        // TODO Auto-generated method stub
        return smsRouteRepository.existsSmsRoute(id, brandName);
    }

    @Override
    public Boolean existsSmsRoute(String brandName) {
        // TODO Auto-generated method stub
        return smsRouteRepository.existsSmsRoute(brandName);
    }

	@Override
	public MyAppError updateBrandBalance(SMSRoute brandDTO) {
		// TODO Auto-generated method stub
                MyAppError error=new MyAppError();
                int nRow=smsRouteRepository.updateBrandBalance(brandDTO);
                if(nRow>0)
                {
                    error.setERROR_TYPE(MyAppError.NOERROR);
                    error.setErrorMessage("Balance Updated Successfully");
                }
                else
                {
                    error.setERROR_TYPE(MyAppError.OTHERERROR);
                    error.setErrorMessage("Balance Updated Failed");
                }
		return error;
	}

    @Override
    public int getBrandIdByName(String csNetworks) {
        return smsRouteRepository.getBrandIdByName(csNetworks);
    }
}
