package org.ipvision.service;


import org.ipvision.domain.User;
import org.ipvision.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	UserRepository userRepository;

	@Autowired
	private void setUserRepository(UserRepository repository) {
		Assert.notNull(repository);
		this.userRepository = repository;
	}

	@Override
	public UserDetails loadUserByUsername(String username) {
		User u = userRepository.findByUsername(username);
		return u;

	}

}
