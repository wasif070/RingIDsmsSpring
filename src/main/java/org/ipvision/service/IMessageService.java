package org.ipvision.service;

import java.util.List;

import org.ipvision.constant.DTableOperationType;
import org.ipvision.domain.Message;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.dto.SearchDTO;
import org.ipvision.dto.SummaryDTO;
import org.ipvision.utils.MyAppError;

public interface IMessageService extends IPageableService<Message, MessagesDTO> {

	MessagesDTO findSuccessSmsList(Long viewableRows, Long currentViewablePage, String searchKey, DTableOperationType operation,
			Long goPageNo, Long brandId, Long type, String formDate, String toDate);

	List<MessagesDTO> getAllSmsList(Message message);

	List<MessagesDTO> getSuccessSmsList(Message message);

	List<MessagesDTO> getFailSmsList(Message message);

	List<SummaryDTO> getSummarySmsList(Message message);
	public MyAppError addMessage(MessagesDTO messagesDTO);

	MessagesDTO getMessage(Integer brandId, String msgId);

	MyAppError updateDeliverySMSStatus(MessagesDTO msDTO);

	MyAppError updateSuccessfulMessageVerificationStatus(MessagesDTO dto);

	String verificationCode(String mobileNo);

	List<Integer> getBrandId(int countryId);
}
