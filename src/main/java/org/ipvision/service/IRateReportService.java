package org.ipvision.service;

import java.util.List;
import java.util.Map;

import org.ipvision.domain.RateReport;
import org.ipvision.dto.RateReportDTO;
import org.ipvision.dto.SummaryDTO;

public interface IRateReportService extends IPageableService<RateReport, RateReportDTO> {
	List<RateReport> findAllRate();
	RateReportDTO getSMSRoutesList();
	RateReportDTO getFailSmsList();
	RateReportDTO getSuccessSmsList();
	RateReportDTO getAllSmsList();
	List<SummaryDTO> getSummarySms();
}
