package org.ipvision.service;

import java.util.ArrayList;
import java.util.List;

import org.ipvision.constant.DTableOperationType;
import org.ipvision.domain.Country;
import org.ipvision.dto.CountryDTO;
import org.ipvision.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service("countryService")
public class CountryService implements ICountryService {

    @Autowired
    private CountryRepository countryRepository;

    @Override
    public void delete(Long id) {
        // TODO Auto-generated method stub
        countryRepository.delete(id);
    }

    @Override
    public Country save(Country country) {
        // TODO Auto-generated method stub
        return countryRepository.save(country);
    }

    private CountryDTO findSelection(CountryDTO pageCountryDTO) {
        int page = (int) (pageCountryDTO.getViewablePage() - 1);
        int size = pageCountryDTO.getViewableRows().intValue();
        System.out.println("PAGE: " + page + " SIZE: " + size);
        Page<Country> pageCountries = countryRepository.findAll(new PageRequest(page, size));
        pageCountryDTO.setListItems(pageCountries.getContent());

        return pageCountryDTO;
    }

    @Override
    public CountryDTO findPageableDTO(Long vRows, Long vPage, String searchKey, DTableOperationType operation,
            Long goPage) {
        System.out.println("vRows: " + vRows);
        System.out.println("vPage: " + vPage);
        System.out.println("searchKey: " + searchKey);
        System.out.println("goPageNo: " + goPage);

        if (operation == DTableOperationType.SELECT) {
            vPage = 1L;
            goPage = 1L;
        }

        CountryDTO pageCountryDTO;

        if (vRows != null) {
            pageCountryDTO = new CountryDTO(vRows);
        } else {
            pageCountryDTO = new CountryDTO();
        }

        if (vPage != null) {
            pageCountryDTO.setViewablePage(vPage);
        }

        pageCountryDTO.setTotalRows(countryRepository.count());
        pageCountryDTO.setTotalPages(
                (long) Math.ceil(pageCountryDTO.getTotalRows() * 1.0 / pageCountryDTO.getViewableRows()));

        if (operation != null) {
            pageCountryDTO.setOperation(operation);
            if (operation == DTableOperationType.NEXT) {
                Long page = pageCountryDTO.getViewablePage() + 1;
                page = page > pageCountryDTO.getTotalPages() ? page - 1 : page; // Last
                // page
                // checking
                pageCountryDTO.setViewablePage(page);
                pageCountryDTO = findSelection(pageCountryDTO);

            } else if (operation == DTableOperationType.PREV) {
                Long page = pageCountryDTO.getViewablePage() - 1;

                page = page == 0 ? 1 : page; // First Page checking

                pageCountryDTO.setViewablePage(page);
                pageCountryDTO = findSelection(pageCountryDTO);

            } else if (operation == DTableOperationType.GO_PAGE) {

                pageCountryDTO.setViewablePage(goPage);
                pageCountryDTO = findSelection(pageCountryDTO);

            } else if (operation == DTableOperationType.SELECT || searchKey.equals("")) {
                pageCountryDTO = findSelection(pageCountryDTO);
            } else {
                pageCountryDTO.setTotalPages(1L); // All search result shown in
                // one
                // page
                pageCountryDTO.setSearchKey(searchKey);
                List<Country> Countries = countryRepository.findCountriesBySearchKey(searchKey.toLowerCase());
                pageCountryDTO.setListItems(Countries);
            }

        } else {
            pageCountryDTO = findSelection(pageCountryDTO);
        }

        return pageCountryDTO;
    }

    @Override
    public Country findById(Long id) {
        // TODO Auto-generated method stub

        return countryRepository.findOne(id);
    }

    @Override
    public List<Country> findAll() {
        // TODO Auto-generated method stub
        return countryRepository.findAll();
    }

    @Override
    public Boolean existsCountry(String countrySymbol, String countryName, String countryCode) {
        // TODO Auto-generated method stub
        return countryRepository.existsCountry(countrySymbol, countryName, countryCode);
    }

    @Override
    public List<Country> csvToCountryList(MultipartFile file) {
        // TODO Auto-generated method stub
        List<Country> countries = new ArrayList<>();

        try {

            byte[] fileData = file.getBytes();
            String data = new String(fileData);
            data = data.replaceAll("\\r", "");
            String countryArray[] = data.split("\\n");
            for (String s : countryArray) {

                String[] value = s.split(",");
                if (value[2].equalsIgnoreCase("CountryCode") || value[2].equalsIgnoreCase("Country Code") || value[1].equalsIgnoreCase("CountryName") || value[1].equalsIgnoreCase("Country Name")) {
                    continue;
                }

                Country country = new Country();
                country.setCountrySymbol(value[0]);
                country.setCountryName(value[1]);

                if (!value[2].startsWith("+")) {
                    value[2] = "+" + value[2];
                }
                country.setCountryCode(value[2]);

                countries.add(country);
                // Boolean flag = countryRepository.existsCountry(value[0],
                // value[1], value[2]);
                // if (flag) {
                // System.out.println("Country Exist");
                // log.info("ContentType --> " + file.getContentType() + " File
                // --> " + file + " File Content --> "
                // + s);
                // System.out.println("ContentType --> " + file.getContentType()
                // + " File --> " + file
                // + " File Content --> " + s);
                // }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return countries;
    }

    @Override
    public Boolean existsCountry(Long id, String countrySymbol, String countryName, String countryCode) {
        // TODO Auto-generated method stub
        return countryRepository.existsCountry(id, countrySymbol, countryName, countryCode);
    }

    @Override
    public Country findByCode(String countryCode) {
        // TODO Auto-generated method stub
        return countryRepository.findByCode(countryCode);
    }

    // @Override
    // public Country findByCountrycode(String countryCode) {
    // // TODO Auto-generated method stub
    // return countryRepository.findByCountrycode(countryCode);
    // }
}
