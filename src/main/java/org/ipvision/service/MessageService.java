package org.ipvision.service;

import java.util.ArrayList;
import java.util.List;

import org.ipvision.constant.DTableOperationType;
import org.ipvision.domain.Message;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.dto.SearchDTO;
import org.ipvision.dto.SummaryDTO;
import org.ipvision.repository.MessageRepository;
import org.ipvision.utils.MyAppError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service("messageService")
public class MessageService implements IMessageService {

    @Autowired
    private MessageRepository messageRepository;

    private MessagesDTO findSelection(MessagesDTO pageMessagesDTO) {

        int page = (int) (pageMessagesDTO.getViewablePage() - 1);
        int size = pageMessagesDTO.getViewableRows().intValue();
        System.out.println("PAGE: " + page + " SIZE: " + size);

        List<Message> successSmsList = messageRepository.findAll(new PageRequest(page, size));

        pageMessagesDTO.setListItems(successSmsList);

        return pageMessagesDTO;
    }

    @Override
    public MessagesDTO findPageableDTO(Long vRows, Long vPage, String searchKey, DTableOperationType operation,
            Long goPage) {
        // TODO Auto-generated method stub
        System.out.println("vRows: " + vRows);
        System.out.println("vPage: " + vPage);
        System.out.println("searchKey: " + searchKey);
        System.out.println("goPageNo: " + goPage);

        if (operation == DTableOperationType.SELECT) {

            vPage = 1L;
            goPage = 1L;
        }

        MessagesDTO pageMessagesDTO;

        SearchDTO searchDTO = new SearchDTO();

        if (vRows != null) {
            pageMessagesDTO = new MessagesDTO(vRows);
        } else {
            pageMessagesDTO = new MessagesDTO();
        }

        if (vPage != null) {
            pageMessagesDTO.setViewablePage(vPage);
        }

        pageMessagesDTO.setTotalRows(100L);
        pageMessagesDTO.setTotalPages(
                (long) Math.ceil(pageMessagesDTO.getTotalRows() * 1.0 / pageMessagesDTO.getViewableRows()));

        if (operation != null) {
            pageMessagesDTO.setOperation(operation);
            if (operation == DTableOperationType.NEXT) {
                Long page = pageMessagesDTO.getViewablePage() + 1;
                page = page > pageMessagesDTO.getTotalPages() ? page - 1 : page; // Last
                // page
                // checking
                pageMessagesDTO.setViewablePage(page);
                pageMessagesDTO = findSelection(pageMessagesDTO);

            } else if (operation == DTableOperationType.PREV) {
                Long page = pageMessagesDTO.getViewablePage() - 1;

                page = page == 0 ? 1 : page; // First Page checking

                pageMessagesDTO.setViewablePage(page);
                pageMessagesDTO = findSelection(pageMessagesDTO);

            } else if (operation == DTableOperationType.GO_PAGE) {

                pageMessagesDTO.setViewablePage(goPage);
                pageMessagesDTO = findSelection(pageMessagesDTO);

            } else if (operation == DTableOperationType.SELECT || searchKey.equals("")) {
                pageMessagesDTO = findSelection(pageMessagesDTO);
            } else {
                pageMessagesDTO.setTotalPages(1L); // All search result shown in
                // one
                // page
                pageMessagesDTO.setSearchKey(searchKey);
                // List<Country> Countries =
                // countryRepository.findCountriesBySearchKey(searchKey.toLowerCase());
                // pageMessagesDTO.setListItems(Countries);
            }

        } else {
            pageMessagesDTO = findSelection(pageMessagesDTO);
        }

        return pageMessagesDTO;
    }

    @Override
    public void delete(Long id) {
        // TODO Auto-generated method stub

    }

    @Override
    public Message findById(Long id) {
        // TODO Auto-generated method stub
        return messageRepository.findOne(id);
    }

    @Override
    public Message save(Message t) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public MessagesDTO findSuccessSmsList(Long vRows, Long vPage, String searchKey, DTableOperationType operation,
            Long goPage, Long brandId, Long type, String formDate, String toDate) {
        // TODO Auto-generated method stub
        System.out.println("vRows: " + vRows);
        System.out.println("vPage: " + vPage);
        System.out.println("searchKey: " + searchKey);
        System.out.println("goPageNo: " + goPage);

        if (operation == DTableOperationType.SELECT) {

            vPage = 1L;
            goPage = 1L;
        }

        MessagesDTO pageMessagesDTO;

        SearchDTO searchDTO = new SearchDTO();

        if (vRows != null) {
            pageMessagesDTO = new MessagesDTO(vRows);
        } else {
            pageMessagesDTO = new MessagesDTO();
        }

        if (vPage != null) {
            pageMessagesDTO.setViewablePage(vPage);
        }

        if (brandId > 0) {
            searchDTO.setSearchByRoutesId(true);
            searchDTO.setRoutesId(brandId.intValue());
        }
        if (formDate != null && formDate.length() > 0) {
            searchDTO.setSerachByFromDate(true);
            searchDTO.setFromDate(formDate);
        }
        if (toDate != null && toDate.length() > 0) {
            searchDTO.setSerachByToDate(true);
            searchDTO.setToDate(toDate);
        }
        if (type > 0) {
            searchDTO.setType(type.intValue());
        }

        // int pageNo = 1;
        // if (message.getPageNo() > 0) {
        // pageNo = message.getPageNo();
        // }
        //
        // long start = (pageNo - 1) * message.getRecordPerPage();
        // int limit = message.getRecordPerPage();
        searchDTO.setSuccess(true);

        // pageMessagesDTO.setTotalRows(countryRepository.count());
        pageMessagesDTO.setTotalPages(
                (long) Math.ceil(pageMessagesDTO.getTotalRows() * 1.0 / pageMessagesDTO.getViewableRows()));

        if (operation != null) {
            pageMessagesDTO.setOperation(operation);
            if (operation == DTableOperationType.NEXT) {
                Long page = pageMessagesDTO.getViewablePage() + 1;
                page = page > pageMessagesDTO.getTotalPages() ? page - 1 : page; // Last
                // page
                // checking
                pageMessagesDTO.setViewablePage(page);
                pageMessagesDTO = findSelection(pageMessagesDTO);

            } else if (operation == DTableOperationType.PREV) {
                Long page = pageMessagesDTO.getViewablePage() - 1;

                page = page == 0 ? 1 : page; // First Page checking

                pageMessagesDTO.setViewablePage(page);
                pageMessagesDTO = findSelection(pageMessagesDTO);

            } else if (operation == DTableOperationType.GO_PAGE) {

                pageMessagesDTO.setViewablePage(goPage);
                pageMessagesDTO = findSelection(pageMessagesDTO);

            } else if (operation == DTableOperationType.SELECT || searchKey.equals("")) {
                pageMessagesDTO = findSelection(pageMessagesDTO);
            } else {
                pageMessagesDTO.setTotalPages(1L); // All search result shown in
                // one
                // page
                pageMessagesDTO.setSearchKey(searchKey);
                // List<Country> Countries =
                // countryRepository.findCountriesBySearchKey(searchKey.toLowerCase());
                // pageMessagesDTO.setListItems(Countries);
            }

        } else {
            pageMessagesDTO = findSelection(pageMessagesDTO);
        }

        return pageMessagesDTO;
    }

    @Override
    public List<MessagesDTO> getAllSmsList(Message message) {
        // TODO Auto-generated method stub

        List<MessagesDTO> list = new ArrayList<>();

        SearchDTO searchDTO = new SearchDTO();

        if (message.getRoutesId() > 0) {
            searchDTO.setSearchByRoutesId(true);
            searchDTO.setRoutesId(message.getRoutesId());
        }
        if (message.getCountryId() > 0) {
            searchDTO.setSearchByCountryId(true);
            searchDTO.setCountryId(message.getCountryId());
        }

        if (message.getFromDate() != null && message.getFromDate().length() > 0) {
            searchDTO.setSerachByFromDate(true);
            searchDTO.setFromDate(message.getFromDate());
        }

        if (message.getToDate() != null && message.getToDate().length() > 0) {
            searchDTO.setSerachByToDate(true);
            searchDTO.setToDate(message.getToDate());
        }

        if (message.getSmsStatus() != null && message.getSmsStatus().length() > 0) {
            searchDTO.setSearchByStatus(true);
            searchDTO.setSmsStatus(message.getSmsStatus());
            message.setSmsStatus(message.getSmsStatus());
        }

        int pageNo = 1;
        if (message.getPageNo() > 0) {
            pageNo = message.getPageNo();
        }

        long start = (pageNo - 1) * message.getRecordPerPage();
        int limit = message.getRecordPerPage();

        Integer total = messageRepository.getMessagesCount(searchDTO);
        list = messageRepository.getMessagesList(start, limit, searchDTO);

        message.setTotalEntries(total);
        // request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
        int dataListSize = 0;
        if (list != null) {
            dataListSize = total;
            // logger.debug("DataSize: " + dataListSize);
        }
        int totalPages = 1;
        if (dataListSize > 0) {
            totalPages = dataListSize / message.getRecordPerPage();
            if (dataListSize % message.getRecordPerPage() != 0) {
                totalPages++;
            }
        }
        message.setTotalPages(totalPages);
        if (totalPages < pageNo) {
            pageNo = totalPages;
        }
        return list;
    }

    @Override
    public List<MessagesDTO> getSuccessSmsList(Message message) {
        // TODO Auto-generated method stub
        // String DATE_FORMAT = "yyyy-MM-dd";
        // Create object of SimpleDateFormat and pass the desired date format.
        // SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        // try {
        // message message = (message) form;
        List<MessagesDTO> list = new ArrayList<>();
        SearchDTO searchDTO = new SearchDTO();
        // MessageScheduler scheduler = new MessageScheduler();
        if (message.getRoutesId() > 0) {
            searchDTO.setSearchByRoutesId(true);
            searchDTO.setRoutesId(message.getRoutesId());
        }
        if (message.getFromDate() != null && message.getFromDate().length() > 0) {
            searchDTO.setSerachByFromDate(true);
            searchDTO.setFromDate(message.getFromDate());
        }
        if (message.getToDate() != null && message.getToDate().length() > 0) {
            searchDTO.setSerachByToDate(true);
            searchDTO.setToDate(message.getToDate());
        }
        if (message.getType() > 0) {
            searchDTO.setType(message.getType());
        }

        int pageNo = 1;
        if (message.getPageNo() > 0) {
            pageNo = message.getPageNo();
        }

        long start = (pageNo - 1) * message.getRecordPerPage();
        int limit = message.getRecordPerPage();

        searchDTO.setSuccess(true);
        Integer total = messageRepository.getMessagesCount(searchDTO);
        list = messageRepository.getMessagesList(start, limit, searchDTO);

        message.setTotalEntries(total);
        // request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
        int dataListSize = 0;
        if (list != null) {
            dataListSize = total;
            // logger.debug("DataSize: " + dataListSize);
        }
        int totalPages = 1;
        if (dataListSize > 0) {
            totalPages = dataListSize / message.getRecordPerPage();
            if (dataListSize % message.getRecordPerPage() != 0) {
                totalPages++;
            }
        }
        message.setTotalPages(totalPages);
        if (totalPages < pageNo) {
            pageNo = totalPages;
        }
        return list;
    }

    @Override
    public List<MessagesDTO> getFailSmsList(Message message) {
        // TODO Auto-generated method stub
        // int action = 0;
        // message message = (message) form;
        List<MessagesDTO> list = new ArrayList<>();

        SearchDTO searchDTO = new SearchDTO();
        // MessageScheduler scheduler = new MessageScheduler();
        if (message.getRoutesId() > 0) {
            searchDTO.setSearchByRoutesId(true);
            searchDTO.setRoutesId(message.getRoutesId());
        }
        if (message.getFromDate() != null && message.getFromDate().length() > 0) {
            searchDTO.setSerachByFromDate(true);
            searchDTO.setFromDate(message.getFromDate());
        }
        if (message.getToDate() != null && message.getToDate().length() > 0) {
            searchDTO.setSerachByToDate(true);
            searchDTO.setToDate(message.getToDate());
        }

        int pageNo = 1;
        if (message.getPageNo() > 0) {
            pageNo = message.getPageNo();
        }

        long start = (pageNo - 1) * message.getRecordPerPage();
        int limit = message.getRecordPerPage();

        Integer total = messageRepository.getMessagesCount(searchDTO);
        list = messageRepository.getMessagesList(start, limit, searchDTO);

        message.setTotalEntries(total);
        // request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
        int dataListSize = 0;
        if (list != null) {
            dataListSize = total;
            // logger.debug("DataSize: " + dataListSize);
        }
        int totalPages = 1;
        if (dataListSize > 0) {
            totalPages = dataListSize / message.getRecordPerPage();
            if (dataListSize % message.getRecordPerPage() != 0) {
                totalPages++;
            }
        }
        message.setTotalPages(totalPages);
        if (totalPages < pageNo) {
            pageNo = totalPages;
        }
        return list;
    }

    @Override
    public List<SummaryDTO> getSummarySmsList(Message message) {
        // TODO Auto-generated method stub

        List<SummaryDTO> list = new ArrayList<>();

        SearchDTO searchDTO = new SearchDTO();
        // MessageScheduler scheduler = new MessageScheduler();

        if (message.getCountryId() > 0) {
            searchDTO.setSearchByCountryId(true);
            searchDTO.setCountryId(message.getCountryId());
        }
        if (message.getRoutesId() > 0) {
            searchDTO.setSearchByRoutesId(true);
            searchDTO.setRoutesId(message.getRoutesId());
        }
        if (message.getFromDate() != null && message.getFromDate().length() > 0) {
            searchDTO.setSerachByFromDate(true);
            searchDTO.setFromDate(message.getFromDate());
        }
        if (message.getToDate() != null && message.getToDate().length() > 0) {
            searchDTO.setSerachByToDate(true);
            searchDTO.setToDate(message.getToDate());
        }

        list = messageRepository.getSummaryList(searchDTO);
        message.setTotalEntries(list.size());
        int dataListSize = 0;
        int pageNo = 1;
        if (message.getPageNo() > 0) {
            pageNo = message.getPageNo();
        }
        if (list != null) {
            dataListSize = list.size();
            //logger.debug("DataSize: " + dataListSize);
        }
        int totalPages = 1;
        if (dataListSize > 0) {
            totalPages = dataListSize / message.getRecordPerPage();
            if (dataListSize % message.getRecordPerPage() != 0) {
                totalPages++;
            }
        }
        if (totalPages < pageNo) {
            pageNo = totalPages;
        }
        return list;
    }

    @Override
    public MyAppError addMessage(MessagesDTO messagesDTO) {
        // TODO Auto-generated method stub
        return messageRepository.addMessage(messagesDTO);
    }

    @Override
    public MessagesDTO getMessage(Integer brandId, String msgId) {
        // TODO Auto-generated method stub
        return messageRepository.getMessage(brandId, msgId);
    }

    @Override
    public MyAppError updateDeliverySMSStatus(MessagesDTO msDTO) {
        // TODO Auto-generated method stub
        return messageRepository.updateDeliverySMSStatus(msDTO);
    }

    @Override
    public MyAppError updateSuccessfulMessageVerificationStatus(MessagesDTO dto) {
        // TODO Auto-generated method stub
        return messageRepository.updateSuccessfulMessageVerificationStatus(dto);
    }

    @Override
    public String verificationCode(String mobileNo) {
        // TODO Auto-generated method stub
        return messageRepository.verificationCode(mobileNo);
    }

    @Override
    public List<Integer> getBrandId(int countryId) {
        // TODO Auto-generated method stub
        return messageRepository.getBrandId(countryId);
    }

}
