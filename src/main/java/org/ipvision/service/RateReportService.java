package org.ipvision.service;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ipvision.constant.DTableOperationType;
import org.ipvision.domain.RateReport;
import org.ipvision.domain.SMSRoute;
import org.ipvision.dto.BrandRate;
import org.ipvision.dto.SummaryDTO;
import org.ipvision.dto.RateReportDTO;
import org.ipvision.repository.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service("rateReportService")
public class RateReportService implements IRateReportService {

	private ReportRepository reportRepository;

	@Autowired
	private ISMSRouteService smsRouteService;

	@Autowired
	public void setReportRepository(ReportRepository reportRepository) {
		Assert.notNull(reportRepository);
		this.reportRepository = reportRepository;
	}

	@Override
	public List<RateReport> findAllRate() {
		// TODO Auto-generated method stub
		List<RateReport> rateReports = reportRepository.findAllRate();
		List<SMSRoute> smsRoutes = smsRouteService.findAll();

		return null;
	}

	@Override
	public RateReportDTO findPageableDTO(Long vRows, Long vPage, String searchKey, DTableOperationType operation,
			Long goPage) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public RateReport findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RateReport save(RateReport t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RateReportDTO getSMSRoutesList() {
		// TODO Auto-generated method stub

		Map<String, BrandRate> map = new HashMap<>();

		// SMSRoutesTaskSchedular schedular = new SMSRoutesTaskSchedular();
		List<Map.Entry<Integer, RateReport>> maps = reportRepository.getSMSRoutesList();
		ArrayList<RateReport> smsRoutesList = new ArrayList<>();
		for (Map.Entry<Integer, RateReport> e : maps) {
			smsRoutesList.add(e.getValue());
		}
		for (RateReport sms : smsRoutesList) {
			String key = sms.getOperatorName() + sms.getCountryName();
			BrandRate dto = new BrandRate();
			dto.setCountryName(sms.getCountryName());
			if (map.containsKey(key)) {
				dto = map.get(key);
			} else {
				map.put(key, dto);
			}
			dto.getBrands().put(sms.getBrandId(), sms.getSmsRates());
		}
		List list = new ArrayList<BrandRate>(map.values());
		Map<Integer, String> brandList = new HashMap<Integer, String>();
		List<SMSRoute> brandNameAndId = smsRouteService.findAll();
		for (SMSRoute next : brandNameAndId) {
			brandList.put(next.getId().intValue(), next.getBrandName());
		}
		RateReportDTO rateReportDTO = new RateReportDTO();
		rateReportDTO.setDataRows(list);
		rateReportDTO.setVendors(brandList);
		return rateReportDTO;
	}

	@Override
	public RateReportDTO getFailSmsList() {
		// TODO Auto-generated method stub

		List<RateReport> rateReports = reportRepository.getFailSmsList();
		RateReportDTO rateReportDTO = new RateReportDTO();
		rateReportDTO.setViewablePage(1L);
		rateReportDTO.setViewableRows((long) rateReports.size());
		rateReportDTO.setListItems(rateReports);
		return rateReportDTO;
	}

	@Override
	public RateReportDTO getSuccessSmsList() {
		// TODO Auto-generated method stub

		List<RateReport> rateReports = reportRepository.getSuccessSmsList();
		RateReportDTO rateReportDTO = new RateReportDTO();
		rateReportDTO.setViewablePage(1L);
		rateReportDTO.setViewableRows((long) rateReports.size());
		rateReportDTO.setListItems(rateReports);
		return rateReportDTO;
	}

	@Override
	public RateReportDTO getAllSmsList() {
		// TODO Auto-generated method stub

		List<RateReport> rateReports = reportRepository.getAllSmsList();
		RateReportDTO rateReportDTO = new RateReportDTO();
		rateReportDTO.setViewablePage(1L);
		rateReportDTO.setViewableRows((long) rateReports.size());
		rateReportDTO.setListItems(rateReports);
		return rateReportDTO;
	}

	@Override
	public List<SummaryDTO> getSummarySms() {
		// TODO Auto-generated method stub

		List<SummaryDTO> smsSummaries = reportRepository.getSummarySms();
		return smsSummaries;
	}
}
