package org.ipvision.service;

import java.util.List;

import org.ipvision.constant.DTableOperationType;
import org.ipvision.domain.User;
import org.ipvision.dto.UserDTO;
import org.ipvision.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service("userService")
public class UserService implements IUserService {

	private UserRepository userRepository;

	@Autowired
	private void setUserRepository(UserRepository repository) {
		Assert.notNull(repository);
		this.userRepository = repository;
	}

	@Override
	public UserDTO findPageableDTO(Long vRows, Long vPage, String searchKey, DTableOperationType operation,
			Long goPage) {
		// TODO Auto-generated method stub
		UserDTO userDTO;

		if (operation == DTableOperationType.SELECT) {

			vPage = 1L;
			goPage = 1L;
		}

		if (vRows != null) {
			userDTO = new UserDTO(vRows);
		} else {
			userDTO = new UserDTO();
		}

		userDTO.setTotalRows(userRepository.count());
		userDTO.setTotalPages((long) Math.ceil(userDTO.getTotalRows() * 1.0 / userDTO.getViewableRows()));

		if (operation != null) {
			userDTO.setOperation(operation);
			if (operation == DTableOperationType.NEXT) {
				Long page = userDTO.getViewablePage() + 1;
				page = page > userDTO.getTotalPages() ? page - 1 : page; // Last
																			// page
																			// checking
				userDTO.setViewablePage(page);
				userDTO = findSelection(userDTO);

			} else if (operation == DTableOperationType.PREV) {
				Long page = userDTO.getViewablePage() - 1;

				page = page == 0 ? 1 : page; // First Page checking

				userDTO.setViewablePage(page);
				userDTO = findSelection(userDTO);

			} else if (operation == DTableOperationType.GO_PAGE) {

				userDTO.setViewablePage(goPage);
				userDTO = findSelection(userDTO);

			} else if (operation == DTableOperationType.SELECT || searchKey.equals("")) {
				userDTO = findSelection(userDTO);
			} else {
				userDTO.setTotalPages(1L); // All search result shown in one
											// page
				userDTO.setSearchKey(searchKey);
				List<User> users = userRepository.findUsersBySearchKey(searchKey.toLowerCase());
				userDTO.setListItems(users);
			}

		} else {
			userDTO = findSelection(userDTO);
		}

		return userDTO;
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		userRepository.delete(id);

	}

	@Override
	public User findById(Long id) {
		// TODO Auto-generated method stub
		return userRepository.findOne(id);
	}

	@Override
	public User save(User t) {
		// TODO Auto-generated method stub
		return userRepository.save(t);
	}

	private UserDTO findSelection(UserDTO userDTO) {

		int page = (int) (userDTO.getViewablePage() - 1);
		int size = userDTO.getViewableRows().intValue();
		Page<User> pageUsers = userRepository.findAll(new PageRequest(page, size));
		userDTO.setListItems(pageUsers.getContent());
		return userDTO;
	}

	@Override
	public User findByUsername(String username) {
		// TODO Auto-generated method stub
		return userRepository.findByUsername(username);
	}

}
