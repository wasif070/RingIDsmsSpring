package org.ipvision.service;

import org.ipvision.domain.User;
import org.ipvision.dto.UserDTO;

public interface IUserService extends IPageableService<User, UserDTO>{
	
	User findByUsername(String username);

}
