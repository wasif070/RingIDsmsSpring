package org.ipvision.service;

import java.util.List;

import org.ipvision.constant.DTableOperationType;
import org.ipvision.domain.Rateplan;
import org.ipvision.dto.RateplanDTO;
import org.ipvision.repository.RateplanRepository;
import org.ipvision.utils.MyAppError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service("rateplanService")
public class RateplanService implements IRateplanService {

	private RateplanRepository rateplanRepository;

	@Autowired
	public void setRateplanRepository(RateplanRepository rateplanRepository) {
		Assert.notNull(rateplanRepository);
		this.rateplanRepository = rateplanRepository;
	}

	private RateplanDTO findSelection(RateplanDTO pageRateplanDTO) {

		int page = (int) (pageRateplanDTO.getViewablePage() - 1);
		int size = pageRateplanDTO.getViewableRows().intValue();
		System.out.println("PAGE: " + page + " SIZE: " + size);
		// List<Rateplan> pageRateplans =rateplanRepository.findAllRateplans();
		List<Rateplan> pageRateplans = rateplanRepository.findAllRateplans(page, size);

		// Page<Rateplan> pageRateplans =
		// rateplanRepository.findAllRateplans(new PageRequest(page, size));
		pageRateplanDTO.setListItems(pageRateplans);
		System.out.println("Total Rows : " + pageRateplans.size());
		return pageRateplanDTO;
	}

	@Override
	public RateplanDTO findPageableDTO(Long vRows, Long vPage, String searchKey, DTableOperationType operation,
			Long goPage) {
		// TODO Auto-generated method stub
		System.out.println("vRows: " + vRows);
		System.out.println("vPage: " + vPage);
		System.out.println("searchKey: " + searchKey);
		System.out.println("goPageNo: " + goPage);

		if (operation == DTableOperationType.SELECT) {

			vPage = 1L;
			goPage = 1L;
		}
		
		RateplanDTO pageRateplanDTO;

		if (vRows != null) {
			pageRateplanDTO = new RateplanDTO(vRows);
		} else {
			pageRateplanDTO = new RateplanDTO();
		}

		if (vPage != null)
			pageRateplanDTO.setViewablePage(vPage);

		pageRateplanDTO.setTotalRows(rateplanRepository.count());

		pageRateplanDTO.setTotalPages(
				(long) Math.ceil(pageRateplanDTO.getTotalRows() * 1.0 / pageRateplanDTO.getViewableRows()));

		if (operation != null) {
			pageRateplanDTO.setOperation(operation);
			if (operation == DTableOperationType.NEXT) {
				Long page = pageRateplanDTO.getViewablePage() + 1;
				page = page > pageRateplanDTO.getTotalPages() ? page - 1 : page; // Last
				// page
				// checking
				pageRateplanDTO.setViewablePage(page);
				pageRateplanDTO = findSelection(pageRateplanDTO);

			} else if (operation == DTableOperationType.PREV) {
				Long page = pageRateplanDTO.getViewablePage() - 1;

				page = page == 0 ? 1 : page; // First Page checking

				pageRateplanDTO.setViewablePage(page);
				pageRateplanDTO = findSelection(pageRateplanDTO);

			} else if (operation == DTableOperationType.GO_PAGE) {

				pageRateplanDTO.setViewablePage(goPage);
				pageRateplanDTO = findSelection(pageRateplanDTO);

			} else if (operation == DTableOperationType.SELECT || searchKey.equals("")) {
				pageRateplanDTO = findSelection(pageRateplanDTO);
			} else {
				pageRateplanDTO.setTotalPages(1L); // All search result shown in
													// one
													// page
				pageRateplanDTO.setSearchKey(searchKey);
				List<Rateplan> Rateplans = rateplanRepository.findAllRateplans(searchKey.toLowerCase());
				// List<Rateplan> Rateplans =
				// rateplanRepository.findCountriesBySearchKey(searchKey.toLowerCase());
				pageRateplanDTO.setListItems(Rateplans);
			}

		} else {
			pageRateplanDTO = findSelection(pageRateplanDTO);
		}

		return pageRateplanDTO;
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		rateplanRepository.deleteById(id);
	}

	@Override
	public Rateplan findById(Long id) {
		// TODO Auto-generated method stub
		// return rateplanRepository.findById(id);
		return rateplanRepository.findOne(id);
	}

	@Override
	public Rateplan save(Rateplan rateplan) {
		// TODO Auto-generated method stub
		// return null;
		return rateplanRepository.save(rateplan);
	}

	@Override
	public double getSMSRate(int countryId, int brandId) {
		// TODO Auto-generated method stub
		return rateplanRepository.getSMSRate(countryId, brandId);
	}

	@Override
	public MyAppError addSMSRoutesInfo(Rateplan rateplan) {
		// TODO Auto-generated method stub
		return rateplanRepository.addSMSRoutesInfo(rateplan);
	}

}
