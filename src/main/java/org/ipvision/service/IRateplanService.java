package org.ipvision.service;

import java.util.List;

import org.ipvision.domain.Rateplan;
import org.ipvision.dto.RateplanDTO;
import org.ipvision.utils.MyAppError;

public interface IRateplanService extends IPageableService<Rateplan, RateplanDTO> {

	public double getSMSRate(int countryId, int brandId);
	
	public MyAppError addSMSRoutesInfo(Rateplan rateplan);
}
