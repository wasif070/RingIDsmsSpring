package org.ipvision.service;

import java.util.List;

import org.ipvision.constant.DTableOperationType;
import org.ipvision.dto.PageableDTO;


public interface IPageableService<T, E extends PageableDTO<T>> {
	
	public E findPageableDTO(Long vRows, Long vPage, String searchKey, DTableOperationType operation, Long goPage);

	public void delete(Long id);

	public T findById(Long id);

	public T save(T t);
}
