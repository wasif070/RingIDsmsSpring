package org.ipvision.service;

import java.util.List;

import org.ipvision.domain.Country;
import org.ipvision.dto.CountryDTO;
import org.springframework.data.repository.query.Param;
import org.springframework.web.multipart.MultipartFile;

public interface ICountryService extends IPageableService<Country, CountryDTO> {

//	Country findByCountrycode(String countryCode);
    public List<Country> findAll();

    public Boolean existsCountry(String countrySymbol, String countryName, String countryCode);

    public List<Country> csvToCountryList(MultipartFile file);

    public Boolean existsCountry(Long id, String countrySymbol, String countryName, String countryCode);

    public Country findByCode(String countryCode);
}
