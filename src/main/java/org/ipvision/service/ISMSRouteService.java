package org.ipvision.service;

import java.util.ArrayList;
import java.util.List;

import org.ipvision.domain.Country;
import org.ipvision.domain.SMSRoute;
import org.ipvision.dto.SMSRouteDTO;
import org.ipvision.utils.MyAppError;

public interface ISMSRouteService extends IPageableService<SMSRoute, SMSRouteDTO> {

    public List<SMSRoute> findAll();

    public Boolean existsSmsRoute(Long id, String brandName);

    public Boolean existsSmsRoute(String brandName);

    public MyAppError updateBrandBalance(SMSRoute brandDTO);

    public int getBrandIdByName(String csNetworks);

}
