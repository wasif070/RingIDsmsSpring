package org.ipvision.sms.brands;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.log4j.Logger;
import org.ipvision.domain.SMSRoute;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.service.IMessageService;
import org.ipvision.service.ISMSRouteService;
import org.ipvision.sms.routes.SMSSendManager;
import org.ipvision.sms.routes.SmsStatusDTO;
import org.ipvision.utils.Constants;
import org.json.JSONObject;

public class SMSWaalaApi extends SMSSendManager {

    ISMSRouteService smsRouteService;

    private static Logger logger = Logger.getLogger("authCommunicationLogger");
    private MessagesDTO messagesDTO = new MessagesDTO();
    String destinationAddress;

    public SMSWaalaApi(IMessageService messageService) {
        super(messageService);
    }

    public SMSWaalaApi(String destinationAddress, IMessageService messageService) {
        super(messageService);
        this.destinationAddress = destinationAddress;
    }

    public SMSWaalaApi(ISMSRouteService smsRouteService, IMessageService messageService) {
        super(messageService);
        this.smsRouteService = smsRouteService;
    }

    @Override
    public MessagesDTO sendSMS(int countryId, String vCode, int brandId, String mobileNumber, String msg, Double rate) {
        String result = Constants.SUCCESS;
        String apiMsgID = "";
        try {
            // (1). brandDTO
            brandDTO = smsRouteService.findById(new Long(brandId));

            // (2). Construct data
            String data = prepareData(brandDTO, msg, mobileNumber);

            // (3)-1. Preparing messageDTO
            messagesDTO.setSmsRate(rate);
            messagesDTO.setCountryId(countryId);
            messagesDTO.setvCode(vCode);
            messagesDTO.setVerifiedSend(0);
            messagesDTO.setRoutesId(brandId);
            messagesDTO.setDestination(mobileNumber);

            String sentTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
            logger.debug("sent Date:" + sentTime);
            messagesDTO.setDate(sentTime);
            messagesDTO.setMessage(msg);

            // (4). Send data
            String response = sendData(data, brandDTO);
            logger.debug("SMSWaalaApi response : " + response);

            // (3)-2. Preparing messageDTO
            messagesDTO.setBrandResponse(response);

            // 4829782330OK
            if (response.startsWith("-1")) {
                result = Constants.FAILED;
                messagesDTO.setDlStatus(-1);
                messagesDTO.setVerifiedSend(-1);
                messagesDTO.setResponseId("-1");
                int i = 0;
                for (i = 0; i < response.length(); i++) {
                    try {
                        Integer.valueOf(response.charAt(i));
                    } catch (Exception e) {
                        break;
                    }
                }
                String errorText = response.substring(i);
                logger.error("errorText --> " + errorText);
                logger.error("Message submission failed : " + response);
                logger.error("Message content : " + msg);
            } else if (response.endsWith("OK")) {
                result = Constants.SUCCESS;
                apiMsgID = response.substring(0, response.length() - ("OK".length() + "0".length()));
                messagesDTO.setResponseId(apiMsgID);

                logger.info("Message submitted successfully and stored in repository");
            }
        } catch (Exception e) {
            result = Constants.FAILED;
            messagesDTO.setDlStatus(-1);
            messagesDTO.setVerifiedSend(-1);
            messagesDTO.setResponseId("-1");
            String errorText = e.toString();
            logger.error("Error SMS " + errorText);
            messagesDTO.setBrandResponse(
                    messagesDTO.getBrandResponse() != null && messagesDTO.getBrandResponse().length() < 1 ? errorText
                    : messagesDTO.getBrandResponse());
        }
        // (6). add to DB
        messagesDTO.setSmsStatus(result);
        messagesDTO.setDbInsert(addToDB(messagesDTO));

        return messagesDTO;
    }

    @Override
    public String prepareData(SMSRoute brandDTO, String msg, String destinationAddress) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("USERNAME=").append(brandDTO.getUserName()).append("&PASSWORD=")
                .append(brandDTO.getPassword()).append("&DESTADDR=").append(destinationAddress).append("&SOURCEADDR=")
                .append(brandDTO.getSenderId()).append("&MESSAGE=").append(msg);
        return stringBuilder.toString();
    }
    // http://192.165.68.7/EMG/index.php?r=api/recdlr&user=USER_NAME&msgid=XXXXXXXX&phone=DESTINATION_ADD&msgtype=DND

    public SmsStatusDTO getDeliveryStatus(int brandId, String smsid) {
        SmsStatusDTO smsStatusDTO = new SmsStatusDTO();
        brandDTO = smsRouteService.findById(new Long(brandId));
        try {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                @Override
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};

            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

            String data = prepareDeliveryStatusData(brandDTO, smsid);
            String combinedUrl = "https://192.165.68.7/EMG/index.php?" + data;
            HttpsURLConnection conn = (HttpsURLConnection) new URL(combinedUrl).openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
            conn.getOutputStream().write(data.getBytes("UTF-8"));
            /*
			 * String strr = "https://192.165.68.7/EMG/index.php?"+data;
			 * System.out.println("str --> " + strr); HttpsURLConnection conn_n
			 * = (HttpsURLConnection)new URL(strr).openConnection();
			 * 
			 * InputStream ins = conn_n.getInputStream(); InputStreamReader isr
			 * = new InputStreamReader(ins); BufferedReader in = new
			 * BufferedReader(isr);
			 * 
			 * String inputLine;
			 * 
			 * while ((inputLine = in.readLine()) != null) {
			 * System.out.println(inputLine); }
			 * 
			 * in.close();
             */

            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            final StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                stringBuilder.append(line);
            }
            rd.close();
            String str = stringBuilder.toString();
            System.err.println("########### --> " + str);
            logger.info("Delivery API response [SMSWala] : " + str);
            smsStatusDTO = parseResponse(str);
        } catch (Exception e) {
            logger.error("error in getStatus() " + e);
            smsStatusDTO.setStatus(SmsStatusDTO.STATUS.ERROR);
        }
        return smsStatusDTO;
    }

    @Override
    public String prepareDeliveryStatusData(SMSRoute brandDTO, String messageId) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("r=api/recdlr").append("&user=").append(brandDTO.getUserName()).append("&msgid=")
                .append(messageId).append("&phone=").append(this.destinationAddress).append("&msgtype=DND");
        return stringBuilder.toString();
    }

    @Override
    public SmsStatusDTO parseResponse(String response) {
        // {"msg":"fail"}
        SmsStatusDTO smsStatusDTO = new SmsStatusDTO();
        try {
            JSONObject jSONObject = new JSONObject(response);
            if (jSONObject.getString("msg").equals("success")) {
                smsStatusDTO.setStatus(SmsStatusDTO.STATUS.SUCCESS);
            } else if (jSONObject.getString("msg").equals("fail")) {
                smsStatusDTO.setStatus(SmsStatusDTO.STATUS.ERROR);
            }
        } catch (Exception e) {
            logger.error("Error while sms delivery checking (SMSWala) :: " + e);
        }
        System.out.println("" + smsStatusDTO);
        return smsStatusDTO;
    }

    public static void main(String[] args) {
        SMSWaalaApi sMSWaalaApi = new SMSWaalaApi("8801913367007", null);
        String response = "482995521";

        // response = "<?xml version=\"1.0\" encoding=\"UTF-8\"
        // standalone=\"yes\"?><ServiceClass
        // xmlns=\"http://cmp.aktel.com/\"><MessageId>54281820</MessageId><Status>0</Status><StatusText>pending</StatusText><ErrorCode>0</ErrorCode><SMSCount>1</SMSCount><CurrentCredit>198339</CurrentCredit></ServiceClass>";
        sMSWaalaApi.getDeliveryStatus(12, response);
    }
}
