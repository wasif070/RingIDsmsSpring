package org.ipvision.sms.brands;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import org.ipvision.domain.SMSRoute;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.service.ICountryService;
import org.ipvision.service.IMessageService;
import org.ipvision.service.ISMSRouteService;
import org.ipvision.sms.routes.SMSSendManager;
import org.ipvision.sms.routes.SmsStatusDTO;
import org.ipvision.utils.Constants;
import org.ipvision.utils.SmsServerConstant;

public class GP extends SMSSendManager {

    ISMSRouteService smsRouteService;

    ICountryService countryService;

    private static final Logger logger = Logger.getLogger("authCommunicationLogger");
    private MessagesDTO messagesDTO = new MessagesDTO();
    public String result = Constants.SUCCESS;
    String countryCode;

    public GP(ISMSRouteService smsRouteService, IMessageService messageService) {
        super(messageService);
        this.smsRouteService = smsRouteService;
    }

    @Override
    public MessagesDTO sendSMS(int countryId, String vCode, int brandId, String mobileNumber, String msg, Double smsRate) {
        try {
            //---------------- Only for GP [start] -------------------            
            countryCode = countryService.findById(new Long(countryId)).getCountryCode();
            if (countryCode.startsWith("+")) {
                countryCode = countryCode.substring(1, countryCode.length());
            }

            String tempMobileNumber = mobileNumber;
            if (mobileNumber.startsWith(countryCode)) {
                tempMobileNumber = mobileNumber.substring(2);
            }

            //---------------- Only for GP [end] -------------------
            // (1). brandDTO
            brandDTO = smsRouteService.findById(new Long(brandId));
            // (2). Construct data
            String data = prepareData(brandDTO, msg, tempMobileNumber);
            if (data.length() < 1) {
                throw new Exception("URL formation failed for GP --> & [msg] --> " + msg + " & [mobileNumber] --> " + mobileNumber);
            }

            // (3)-1. Preparing messageDTO
            //messagesDTO.//setSmsRate(rate);
            messagesDTO.setCountryId(countryId);
            messagesDTO.setvCode(vCode);
            messagesDTO.setVerifiedSend(0);
            messagesDTO.setRoutesId(brandId);
            messagesDTO.setDestination(mobileNumber);
            messagesDTO.setMessage(msg);

            String sentTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
            //logger.debug("sent Date:" + sentTime);

            messagesDTO.setDate(sentTime);

            // (4). Send data
            String response = sendData(data, brandDTO); //"8801913367007-201510144028748"

            // (3)-2. Preparing messageDTO
            messagesDTO.setBrandResponse(response);

            logger.debug("GP response " + response);

            // (5). Response analysis
            String msgId = null;
            Integer status = null;
            String statusText = "";
            try {
                String[] responseArray = response.split(",");
                status = Integer.parseInt(responseArray[0]);
                if (status == 200) {
                    msgId = responseArray[1];
                } else {
                    statusText = responseArray[1];
                }
                logger.debug("status --> " + status);
                logger.debug("msgId --> " + msgId);
            } catch (Exception e) {
                logger.error("Exception while parsing GP response --> response --> " + response + " --> exception --> " + e);
            }

            if (msgId != null && status == 200) {
                result = Constants.SUCCESS;
                messagesDTO.setResponseId(msgId);
                logger.info("msg submitted - waiting for confirmation [GP]");
            } else {
                result = Constants.FAILED;
                messagesDTO.setDlStatus(-1);
                messagesDTO.setVerifiedSend(-1);
                messagesDTO.setResponseId("-1");
                logger.error("Message submission failed : " + statusText);
                logger.error("Message content : " + msg);
            }
        } catch (Exception e) {
            result = Constants.FAILED;
            messagesDTO.setDlStatus(-1);
            messagesDTO.setVerifiedSend(-1);
            messagesDTO.setResponseId("-1");
            String errorText = e.toString();
            logger.error("Error SMS " + errorText);
            messagesDTO.setBrandResponse(messagesDTO.getBrandResponse() != null && messagesDTO.getBrandResponse().length() < 1 ? errorText : messagesDTO.getBrandResponse());
        }

        // (6). add to DB
        messagesDTO.setSmsStatus(result);
        messagesDTO.setDbInsert(addToDB(messagesDTO));
        return messagesDTO;
    }

    public SmsStatusDTO getDeliveryStatus(int brandId, MessagesDTO messagesDTO) {
        SmsStatusDTO smsStatusDTO = new SmsStatusDTO();
        brandDTO = smsRouteService.findById(new Long(brandId));
        try {
            String data = prepareDeliveryStatusData(brandDTO, messagesDTO.getResponseId());

            countryCode = countryService.findById(new Long(messagesDTO.getCountryId())).getCountryCode();

            if (countryCode.startsWith("+")) {
                countryCode = countryCode.substring(1, countryCode.length());
            }

            String tempMobileNumber = messagesDTO.getDestination();

            if (messagesDTO.getDestination().startsWith(countryCode)) {
                tempMobileNumber = messagesDTO.getDestination().substring(2);
            }

            data += "&msisdn=" + tempMobileNumber;
            logger.debug("data delivery --> " + data);

            SMSRoute b = new SMSRoute();
            b.setApiUrl("http://smsbd.ringid.com/verifySMS");
            String str = sendData(data, b);
            logger.info("Delivery API response [GP] : " + str);
            smsStatusDTO = parseResponse(str);
        } catch (Exception e) {
            logger.error("error in getStatus() " + e);
            smsStatusDTO.setStatus(SmsStatusDTO.STATUS.ERROR);
        }
        return smsStatusDTO;

//        <?xml version="1.0" encoding="utf-8"?><DataSet xmlns="http://wavecell.com/">  <xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">    <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">      <xs:complexType>        <xs:choice minOccurs="0" maxOccurs="unbounded">          <xs:element name="Table">            <xs:complexType>              <xs:sequence>                <xs:element name="Status" type="xs:string" minOccurs="0" />                <xs:element name="UMID" type="xs:string" minOccurs="0" />              </xs:sequence>            </xs:complexType>          </xs:element>        </xs:choice>      </xs:complexType>    </xs:element>  </xs:schema>  <diffgr:diffgram xmlns:msdata="urn:schemas-microsoft-com:xml-msdata" xmlns:diffgr="urn:schemas-microsoft-com:xml-diffgram-v1" /></DataSet>
    }

    @Override
    public String prepareData(SMSRoute brandDTO, String msg, String mobileNumber) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < msg.length(); i++) {
                sb.append(Integer.toHexString(msg.charAt(i) | 0x10000).substring(1));
            }

            stringBuilder
                    .append("brandId=").append(SmsServerConstant.GP)
                    .append("&countrycode=").append(countryCode)
                    .append("&messagetype=3")
                    .append("&message=").append(sb.toString())
                    //.append("&message=").append(URLEncoder.encode(msg, "UTF-8"))
                    .append("&msisdn=").append(mobileNumber);
        } /*catch (UnsupportedEncodingException ex) {
         logger.error("UnsupportedEncodingException GP --> " + ex);
         }*/ catch (Exception ex) {
            logger.error("Exception GP --> " + ex);
        }
        return stringBuilder.toString();
    }

    @Override
    public String prepareDeliveryStatusData(SMSRoute brandDTO, String messageId) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            stringBuilder
                    .append("brandId=").append(SmsServerConstant.GP)
                    .append("&messagetype=3")
                    .append("&messageid=").append(messageId);
        } catch (Exception ex) {
            logger.error("Exception [prepareDeliveryStatusData] GP --> " + ex);
        }
        return stringBuilder.toString();
    }

    @Override
    public SmsStatusDTO parseResponse(String response) {
        SmsStatusDTO smsStatusDTO = new SmsStatusDTO();
        Integer status;
        String statusText;
        try {
            String[] responseArray = response.split(",");
            status = Integer.parseInt(responseArray[0]);
            statusText = responseArray[1];

            if (status == 200) {
                smsStatusDTO.setStatus(SmsStatusDTO.STATUS.SUCCESS);
            } else {
                smsStatusDTO.setStatus(SmsStatusDTO.STATUS.ERROR);
            }
            logger.debug("status [delvr status check : ] --> " + status + " --> statusText --> " + statusText);
        } catch (Exception e) {
            logger.error("Exception while parsing GP response --> response --> " + response + " --> exception --> " + e);
        }

        return smsStatusDTO;
    }

}
