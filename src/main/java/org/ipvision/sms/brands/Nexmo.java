package org.ipvision.sms.brands;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;

import org.apache.log4j.Logger;
import org.ipvision.domain.SMSRoute;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.service.ISMSRouteService;
import org.ipvision.sms.routes.SMSSendManager;
import org.ipvision.sms.routes.SmsStatusDTO;
import org.ipvision.utils.Constants;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.ipvision.service.IMessageService;

public class Nexmo extends SMSSendManager {

    ISMSRouteService smsRouteService;

    private static final Logger logger = Logger.getLogger("authCommunicationLogger");
    private MessagesDTO messagesDTO = new MessagesDTO();
    public String result = Constants.SUCCESS;

    public Nexmo(ISMSRouteService smsRouteService, IMessageService messageService) {
        super(messageService);
        this.smsRouteService = smsRouteService;
    }

    @Override
    public MessagesDTO sendSMS(int countryId, String vCode, int brandId, String mobileNumber, String msg, Double rate) {
        try {
            // (1). brandDTO
            brandDTO = smsRouteService.findById(new Long(brandId));
            // (2). Construct data
            String data = prepareData(brandDTO, msg, mobileNumber);
            if (data.length() < 1) {
                throw new Exception("URL formation failed for Nexmo --> & [msg] --> " + msg + " & [mobileNumber] --> "
                        + mobileNumber);
            }

            // (3)-1. Preparing messageDTO
            messagesDTO.setSmsRate(rate);
            messagesDTO.setCountryId(countryId);
            messagesDTO.setvCode(vCode);
            messagesDTO.setVerifiedSend(0);
            messagesDTO.setRoutesId(brandId);
            messagesDTO.setDestination(mobileNumber);
            messagesDTO.setMessage(msg);

            String sentTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
            // logger.debug("sent Date:" + sentTime);

            messagesDTO.setDate(sentTime);

            // (4). Send data
            String response = sendData(data, brandDTO); // "8801913367007-201510144028748"

            // (3)-2. Preparing messageDTO
            messagesDTO.setBrandResponse(response);

            logger.debug("Nexmo response " + response);

            // {
            // "message-count": "1",
            // "messages": [
            // {
            // "to": "8801913367007",
            // "message-id": "0500000033E8BD67",
            // "status": "0",
            // "remaining-balance": "6.91800000",
            // "message-price": "0.04100000",
            // "network": "47003"
            // }
            // ]
            // }
            // (5). Response analysis
            String msgId = null;
            Integer status = null;
            String statusText = "";
            try {
                JsonObject jobject = new JsonParser().parse(response).getAsJsonObject();
                JsonArray jarray = jobject.getAsJsonArray("messages");
                jobject = jarray.get(0).getAsJsonObject();
                msgId = jobject.get("message-id").getAsString();
                status = jobject.get("status").getAsInt();
                if (status > 0) {
                    statusText = jobject.get("error-text").getAsString();
                }
                logger.debug("status --> " + status);
                logger.debug("msgId --> " + msgId);
            } catch (Exception e) {
                logger.error("Exception while parsing Nexmo response --> " + e);
            }

            if (msgId != null && status == 0) {
                result = Constants.SUCCESS;
                messagesDTO.setResponseId(msgId);
                logger.info("msg submitted - waiting for confirmation [Nexmo]");
            } else {
                result = Constants.FAILED;
                messagesDTO.setDlStatus(-1);
                messagesDTO.setVerifiedSend(-1);
                messagesDTO.setResponseId("-1");
                logger.error("Message submission failed : " + statusText);
                logger.error("Message content : " + msg);
            }
        } catch (Exception e) {
            result = Constants.FAILED;
            messagesDTO.setDlStatus(-1);
            messagesDTO.setVerifiedSend(-1);
            messagesDTO.setResponseId("-1");
            String errorText = e.toString();
            logger.error("Error SMS " + errorText);
            messagesDTO.setBrandResponse(
                    messagesDTO.getBrandResponse() != null && messagesDTO.getBrandResponse().length() < 1 ? errorText
                    : messagesDTO.getBrandResponse());
        }

        // (6). add to DB
        messagesDTO.setSmsStatus(result);
        messagesDTO.setDbInsert(addToDB(messagesDTO));
        return messagesDTO;
    }

    @Override
    public String prepareData(SMSRoute brandDTO, String msg, String mobileNumber) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            stringBuilder.append("api_key=").append(brandDTO.getUserName()).append("&api_secret=")
                    .append(brandDTO.getPassword()).append("&text=").append(URLEncoder.encode(msg, "UTF-8"))
                    .append("&from=").append(brandDTO.getSenderId()).append("&to=").append(mobileNumber);
        } catch (UnsupportedEncodingException ex) {
            java.util.logging.Logger.getLogger(Nexmo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return stringBuilder.toString();
    }

    @Override
    public String prepareDeliveryStatusData(SMSRoute brandDTO, String messageId) {
        throw new UnsupportedOperationException("Not supported yet."); // To
        // change
        // body
        // of
        // generated
        // methods,
        // choose
        // Tools
        // |
        // Templates.
    }

    @Override
    public SmsStatusDTO parseResponse(String response) {
        throw new UnsupportedOperationException("Not supported yet."); // To
        // change
        // body
        // of
        // generated
        // methods,
        // choose
        // Tools
        // |
        // Templates.
    }

    private static void printSuccessfulResponse() {
        System.out.println("{\n" + "  \"message-count\": \"1\",\n" + "  \"messages\": [\n" + "    {\n"
                + "      \"to\": \"8801913367007\",\n" + "      \"message-id\": \"0500000033E8BD67\",\n"
                + "      \"status\": \"0\",\n" + "      \"remaining-balance\": \"6.91800000\",\n"
                + "      \"message-price\": \"0.04100000\",\n" + "      \"network\": \"47003\"\n" + "    }\n" + "  ]\n"
                + "}");
    }

    private static void printFailedResponse() {
        System.out.println("{\n" + "  \"message-count\": \"1\",\n" + "  \"messages\": [\n" + "    {\n"
                + "      \"status\": \"2\",\n" + "      \"error-text\": \"Missing username\"\n" + "    }\n" + "  ]\n"
                + "}");
    }

    public static void main(String[] args) {
        String jsonLine = "{\n" + "  \"message-count\": \"1\",\n" + "  \"messages\": [\n" + "    {\n"
                + "      \"to\": \"8801913367007\",\n" + "      \"message-id\": \"0500000033E8BD67\",\n"
                + "      \"status\": \"0\",\n" + "      \"remaining-balance\": \"6.91800000\",\n"
                + "      \"message-price\": \"0.04100000\",\n" + "      \"network\": \"47003\"\n" + "    }\n" + "  ]\n"
                + "}";

        // JsonElement jelement = new JsonParser().parse(jsonLine);
        JsonObject jobject = new JsonParser().parse(jsonLine).getAsJsonObject();
        JsonArray jarray = jobject.getAsJsonArray("messages");
        jobject = jarray.get(0).getAsJsonObject();
        System.out.println("message-id --> " + jobject.get("message-id"));
        System.out.println("resp : \n\n" + jsonLine);

    }
}
