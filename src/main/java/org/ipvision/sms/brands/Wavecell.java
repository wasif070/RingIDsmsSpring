package org.ipvision.sms.brands;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import org.ipvision.domain.SMSRoute;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.service.IMessageService;
import org.ipvision.service.ISMSRouteService;
import org.ipvision.sms.routes.SMSSendManager;
import org.ipvision.sms.routes.SmsStatusDTO;
import org.ipvision.utils.Constants;
import org.ipvision.utils.Utils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Wavecell extends SMSSendManager {

    ISMSRouteService smsRouteService;

    private static Logger logger = Logger.getLogger("authCommunicationLogger");
    private MessagesDTO messagesDTO = new MessagesDTO();

    public Wavecell(ISMSRouteService smsRouteService, IMessageService messageService) {
        super(messageService);
        this.smsRouteService = smsRouteService;
    }

    @Override
    public MessagesDTO sendSMS(int countryId, String vCode, final int brandId, String mobileNumber, String msg, Double rate) {
        String result;

        try {
            // (1). brandDTO       
            brandDTO = smsRouteService.findById(new Long(brandId));
            // (2). Construct data
            String data = prepareData(brandDTO, msg, mobileNumber);

            // (3)-1. Preparing messageDTO
            messagesDTO.setSmsRate(rate);
            messagesDTO.setCountryId(countryId);
            messagesDTO.setvCode(vCode);
            messagesDTO.setVerifiedSend(0);
            messagesDTO.setRoutesId(brandId);
            messagesDTO.setDestination(mobileNumber);

            String sentTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
            logger.debug("sent Date:" + sentTime);
            messagesDTO.setDate(sentTime);
            messagesDTO.setMessage(msg);
            messagesDTO.setSmsRate(rate);

            // (4). Send data
            String response = sendData(data, brandDTO);
//          <?xml version="1.0" encoding="utf-8"?><string xmlns="http://wavecell.com/">RECEIVED:1ac599e0-22fe-4017-a9ad-7ab13e0cc23b</string>
//          <?xml version="1.0" encoding="utf-8"?><string xmlns="http://wavecell.com/">ERROR: Unauthorized:5f0a1de7-5eff-4f75-9cdb-135f2ed5dbb8</string> 

            Document doc = Utils.convertStringToDocument(response);
            doc.getDocumentElement().normalize();
            String reciveReport = doc.getDocumentElement().getTextContent();
//            double balance = brandDTO.getBalance() - rate;

            // (3)-2. Preparing messageDTO
            messagesDTO.setBrandResponse(reciveReport);

            logger.debug("Wavecell response : " + response);

            // (5). Response analysis
            if (reciveReport.contains("RECEIVED")) {
                result = Constants.SUCCESS;
                final String apiMsgID = reciveReport.substring("RECEIVED:".length(), reciveReport.length());
                messagesDTO.setResponseId(apiMsgID);

                logger.info("Message submitted successfully and stored in repository");
            } else {
                result = Constants.FAILED;
                messagesDTO.setDlStatus(-1);
                messagesDTO.setVerifiedSend(-1);
                messagesDTO.setResponseId("-1");
                logger.error("Message submission failed : " + response);
                logger.error("Message content : " + msg);
            }

        } catch (Exception e) {
            result = Constants.FAILED;
            messagesDTO.setDlStatus(-1);
            messagesDTO.setVerifiedSend(-1);
            messagesDTO.setResponseId("-1");
            String errorText = e.toString();
            logger.error("Error SMS " + errorText);
            messagesDTO.setBrandResponse(messagesDTO.getBrandResponse() != null && messagesDTO.getBrandResponse().length() < 1 ? errorText : messagesDTO.getBrandResponse());
        }

        // (6). add to DB
        messagesDTO.setSmsStatus(result);
        messagesDTO.setDbInsert(addToDB(messagesDTO));

        return messagesDTO;
    }

    public SmsStatusDTO getDeliveryStatus(int brandId, String messageId) {
        SmsStatusDTO smsStatusDTO = new SmsStatusDTO();
        brandDTO = smsRouteService.findById(new Long(brandId));
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL("http://wms1.wavecell.com/getDLRApi.asmx/SMSDLR?").openConnection();
            String data = prepareDeliveryStatusData(brandDTO, messageId);
            conn.setDoOutput(true);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
            conn.getOutputStream().write(data.getBytes("UTF-8"));
            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            final StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                stringBuilder.append(line);
            }
            rd.close();
            String str = stringBuilder.toString();
            logger.info("Delivery API response [Wavecell] : " + str);
            smsStatusDTO = parseResponse(str);
        } catch (Exception e) {
            logger.error("error in getStatus() " + e);
            smsStatusDTO.setStatus(SmsStatusDTO.STATUS.ERROR);
        }
        return smsStatusDTO;

//        <?xml version="1.0" encoding="utf-8"?><DataSet xmlns="http://wavecell.com/">  <xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">    <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">      <xs:complexType>        <xs:choice minOccurs="0" maxOccurs="unbounded">          <xs:element name="Table">            <xs:complexType>              <xs:sequence>                <xs:element name="Status" type="xs:string" minOccurs="0" />                <xs:element name="UMID" type="xs:string" minOccurs="0" />              </xs:sequence>            </xs:complexType>          </xs:element>        </xs:choice>      </xs:complexType>    </xs:element>  </xs:schema>  <diffgr:diffgram xmlns:msdata="urn:schemas-microsoft-com:xml-msdata" xmlns:diffgr="urn:schemas-microsoft-com:xml-diffgram-v1" /></DataSet>
    }

    @Override
    public String prepareData(SMSRoute brandDTO, String msg, String mobileNumber) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("AccountId=").append(brandDTO.getUserName())
                .append("&SubAccountId=").append("ipvision_std")
                .append("&Password=").append(brandDTO.getPassword())
                .append("&Destination=").append(mobileNumber)
                .append("&Source=").append(brandDTO.getSenderId())
                .append("&Body=").append(msg)
                .append("&Encoding=").append("ASCII")
                .append("&ScheduledDateTime=")
                .append("&UMID=");
        return stringBuilder.toString();
    }

    @Override
    public String prepareDeliveryStatusData(SMSRoute brandDTO, String messageId) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("AccountId=").append(brandDTO.getUserName())
                .append("&SubAccountId=").append("ipvision_std")
                .append("&Password=").append(brandDTO.getPassword())
                .append("&UMID=").append(messageId);
        return stringBuilder.toString();
    }

    @Override
    public SmsStatusDTO parseResponse(String response) {
        SmsStatusDTO smsStatusDTO = new SmsStatusDTO();
        Document doc = Utils.convertStringToDocument(response);
        doc.getDocumentElement().normalize();
        NodeList nList = doc.getElementsByTagName("NewDataSet");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                response = eElement.getElementsByTagName("Status").item(0).getTextContent();
            }
        }
        if (response.contains("DELIVERED TO DEVICE")) {
            smsStatusDTO.setStatus(SmsStatusDTO.STATUS.SUCCESS);
        } else if (response.contains("TRASHED") || response.contains("REJECTED BY CARRIER")) {
            smsStatusDTO.setStatus(SmsStatusDTO.STATUS.ERROR);
        }

        return smsStatusDTO;
    }

    public static void main(String[] args) {
        String response = "<?xml version=\"1.0\" encoding=\"utf-8\"?><string xmlns=\"http://wavecell.com/\">RECEIVED:1ac599e0-22fe-4017-a9ad-7ab13e0cc23b</string>";
        Document doc = Utils.convertStringToDocument(response);
        doc.getDocumentElement().normalize();
        String reciveReport = doc.getDocumentElement().getTextContent();
        String rsP = reciveReport.substring("RECEIVED:".length(), reciveReport.length());
        System.out.println("" + rsP);
    }
}
