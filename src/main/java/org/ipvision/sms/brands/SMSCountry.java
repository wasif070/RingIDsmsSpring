package org.ipvision.sms.brands;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.ipvision.domain.SMSRoute;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.service.IMessageService;
import org.ipvision.service.ISMSRouteService;
import org.ipvision.sms.routes.SMSSendManager;
import org.ipvision.sms.routes.SmsStatusDTO;
import org.ipvision.utils.Constants;

/**
 *
 * @author sujon
 */
public class SMSCountry extends SMSSendManager {

    ISMSRouteService smsRouteService;

    private static Logger logger = Logger.getLogger("authCommunicationLogger");
    private MessagesDTO messagesDTO = new MessagesDTO();
    String startTime = "";
    String endTime = "";

    public String result = "";

    public SMSCountry(ISMSRouteService smsRouteService, IMessageService messageService) {
        super(messageService);
        this.smsRouteService = smsRouteService;
    }

    @Override
    public MessagesDTO sendSMS(int countryId, String vCode, final int brandId, String mobileNumber, String msg,
            Double rate) {
        try {
            // (1). brandDTO
            brandDTO = smsRouteService.findById(new Long(brandId));
            // (2). Construct data
            String data = prepareData(brandDTO, msg, mobileNumber);

            // (3)-1. Preparing messageDTO
            messagesDTO.setSmsRate(rate);
            messagesDTO.setCountryId(countryId);
            messagesDTO.setvCode(vCode);
            messagesDTO.setVerifiedSend(0);
            messagesDTO.setRoutesId(brandId);
            messagesDTO.setDestination(mobileNumber);

            String sentTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
            logger.debug("sent Date:" + sentTime);

            messagesDTO.setDate(sentTime);
            messagesDTO.setMessage(msg);

            // (4). Send data
            String response = sendData(data, brandDTO);

            // (3)-2. Preparing messageDTO
            messagesDTO.setBrandResponse(response);

            if ((response.startsWith("OK:"))) {
                result = Constants.SUCCESS;
                final String apiMsgID = response.substring(response.indexOf(":") + 1);
                messagesDTO.setResponseId(apiMsgID);

                logger.info("Message submitted successfully and stored in repository");
            } else {
                result = Constants.FAILED;
                messagesDTO.setDlStatus(-1);
                messagesDTO.setVerifiedSend(-1);
                messagesDTO.setResponseId("-1");
                logger.error("Message submission failed [SMSCountry] --> response : " + response);
            }

        } catch (Exception e) {
            result = Constants.FAILED;
            messagesDTO.setDlStatus(-1);
            messagesDTO.setVerifiedSend(-1);
            messagesDTO.setResponseId("-1");
            String errorText = e.toString();
            logger.error("Error SMS " + errorText);
            messagesDTO.setBrandResponse(
                    messagesDTO.getBrandResponse() != null && messagesDTO.getBrandResponse().length() < 1 ? errorText
                    : messagesDTO.getBrandResponse());
        }

        // (6). add to DB
        messagesDTO.setSmsStatus(result);
        messagesDTO.setDbInsert(addToDB(messagesDTO));

        return messagesDTO;
    }

    @Override
    public String prepareData(SMSRoute brandDTO, String msg, String mobileNumber) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("User=").append(brandDTO.getUserName()).append("&passwd=").append(brandDTO.getPassword())
                .append("&message=").append(msg).append("&sid=").append(brandDTO.getSenderId()).append("&mobilenumber=")
                .append(mobileNumber).append("&mtype=N&DR=Y");
        return stringBuilder.toString();
    }

    @Override
    public String prepareDeliveryStatusData(SMSRoute brandDTO, String messageId) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("user=").append(brandDTO.getUserName()).append("&passwd=").append(brandDTO.getPassword())
                .append("&fromdate=").append(startTime).append("&todate=").append(endTime);
        return stringBuilder.toString();
    }

    public SmsStatusDTO getDeliveryStatus(int brandId, String smsid) {
        SmsStatusDTO smsStatusDTO = new SmsStatusDTO();
        brandDTO = smsRouteService.findById(new Long(brandId));
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(
                    "http://api.smscountry.com/smscwebservices_bulk_reports.aspx?").openConnection();
            String data = prepareDeliveryStatusData(brandDTO, smsid);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
            conn.getOutputStream().write(data.getBytes("UTF-8"));
            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            final StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                stringBuilder.append(line);
            }
            rd.close();
            String str = stringBuilder.toString();
            System.err.println("########### --> " + str);
            logger.info("return value : " + str);
            smsStatusDTO = parseResponse(str);
        } catch (Exception e) {
            logger.error("error in getStatus() " + e);
            smsStatusDTO.setStatus(SmsStatusDTO.STATUS.ERROR);
        }
        return smsStatusDTO;
    }

    @Override
    public SmsStatusDTO parseResponse(String response) {
        SmsStatusDTO smsStatusDTO = new SmsStatusDTO();
        System.out.println("response ==> " + response);

        return smsStatusDTO;
    }

    public static void main(String[] args) {
        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/YYYY hh:mm:ss");
        System.out.println("" + ft.format(new Date()));
    }
}
