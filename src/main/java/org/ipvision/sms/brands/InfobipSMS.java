package org.ipvision.sms.brands;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import org.ipvision.domain.SMSRoute;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.service.IMessageService;
import org.ipvision.service.ISMSRouteService;
import org.ipvision.sms.routes.SMSSendManager;
import org.ipvision.sms.routes.SmsStatusDTO;
import org.ipvision.utils.Constants;
import org.ipvision.utils.Utils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class InfobipSMS extends SMSSendManager {

    ISMSRouteService smsRouteService;

    private static Logger logger = Logger.getLogger("authCommunicationLogger");
//    private BrandsDTO brandDTO = new BrandsDTO();
    private MessagesDTO messagesDTO = new MessagesDTO();
    public String result = Constants.SUCCESS;

    public InfobipSMS(ISMSRouteService smsRouteService, IMessageService messageService) {
        super(messageService);
        this.smsRouteService = smsRouteService;
    }

    @Override
    public MessagesDTO sendSMS(int countryId, String vCode, final int brandId, String mobileNumber, String msg, Double rate) {
        try {
            // (1). brandDTO
            brandDTO = smsRouteService.findById(new Long(brandId));
            // (2). Construct data
            String data = prepareData(brandDTO, msg, mobileNumber);

            // (3)-1. Preparing messageDTO
            messagesDTO.setSmsRate(rate);
            messagesDTO.setCountryId(countryId);
            messagesDTO.setvCode(vCode);
            messagesDTO.setVerifiedSend(0);
            messagesDTO.setRoutesId(brandId);
            messagesDTO.setDestination(mobileNumber);

            String sentTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
            logger.debug("sent Date:" + sentTime);

            messagesDTO.setDate(sentTime);
            messagesDTO.setMessage(msg);

            // (4). Send data
            String response = sendData(data, brandDTO);

            // (3)-2. Preparing messageDTO
            messagesDTO.setBrandResponse(response);

//<?xml version="1.0" encoding="UTF-8"?>
//<results>
//<result>
//<status>0</status>
//<messageid>Infobip_MessageId</messageid>
//<destination>38595111111</destination>
//</result>
//</results>
            Document doc = Utils.convertStringToDocument(response);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("results");
//            Double balance = brandDTO.getBalance() - rate;
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String code = eElement.getElementsByTagName("status").item(0).getTextContent();
                    if (code.equalsIgnoreCase("0")) {
                        result = Constants.SUCCESS;
                        final String apiMsgID = eElement.getElementsByTagName("messageid").item(0).getTextContent();
                        messagesDTO.setResponseId(apiMsgID);

                        logger.info("Message submitted successfully and stored in repository");
//                        messagesDTO.setResponseId(eElement.getElementsByTagName("messageid").item(0).getTextContent());  //////////////////////add responseId
                    } else {
                        result = Constants.FAILED;
                        messagesDTO.setDlStatus(-1);
                        messagesDTO.setVerifiedSend(-1);
                        messagesDTO.setResponseId("-1");
                        logger.error("Error --> " + getErrorMessage(Integer.valueOf(code)));
                    }
                }
            }

        } catch (Exception e) {
            result = Constants.FAILED;
            messagesDTO.setDlStatus(-1);
            messagesDTO.setVerifiedSend(-1);
            messagesDTO.setResponseId("-1");
            String errorText = e.toString();
            logger.error("Error SMS " + errorText);
            messagesDTO.setBrandResponse(messagesDTO.getBrandResponse() != null && messagesDTO.getBrandResponse().length() < 1 ? errorText : messagesDTO.getBrandResponse());
        }

        // (6). add to DB
        messagesDTO.setSmsStatus(result);
        messagesDTO.setDbInsert(addToDB(messagesDTO));

        return messagesDTO;
    }

    @Override
    public String prepareData(SMSRoute brandDTO, String msg, String mobileNumber) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("user=").append(brandDTO.getUserName())
                .append("&password=").append(brandDTO.getPassword())
                .append("&SMSText=").append(msg)
                .append("&sender=").append(brandDTO.getSenderId())
                .append("&GSM=").append(mobileNumber);
        return stringBuilder.toString();
    }

    @Override
    public String prepareDeliveryStatusData(SMSRoute brandDTO, String messageId) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("user=").append(brandDTO.getUserName())
                .append("&password=").append(brandDTO.getPassword())
                .append("&messageid=").append(messageId);
        return stringBuilder.toString();
    }

    @Override
    public SmsStatusDTO parseResponse(String response) {

//<DeliveryReport><message id="205091905013000284" sentdate="2015/09/19 07:01:30" donedate="2015/09/19 07:01:46" status="DELIVERED" gsmerror="0" price="2.87" /></DeliveryReport>
        SmsStatusDTO smsStatusDTO = new SmsStatusDTO();
        if (!response.equals("NO_DATA")) {
            Document doc = Utils.convertStringToDocument(response);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("DeliveryReport");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    NamedNodeMap namedNodeMap = eElement.getElementsByTagName("message").item(0).getAttributes();
                    System.out.println("" + namedNodeMap.getNamedItem("sentdate"));
                    String status = namedNodeMap.getNamedItem("status").getTextContent();
                    if (status.equals("DELIVERED")) {
                        smsStatusDTO.setStatus(SmsStatusDTO.STATUS.SUCCESS);
                    } else {
                        smsStatusDTO.setStatus(SmsStatusDTO.STATUS.ERROR);
                        smsStatusDTO.setResponseText(getStatusDetails(status));
                        smsStatusDTO.setResponseCode(status);
                        smsStatusDTO.setResponseDetails(getStatusDetails(status));
                    }
                    System.out.println("smsStatus --> " + smsStatusDTO.toString());
                }
            }
        }
        return smsStatusDTO;
    }

    public SmsStatusDTO getDeliveryStatus(int brandId, String smsid) {
        SmsStatusDTO smsStatusDTO = new SmsStatusDTO();
        brandDTO = smsRouteService.findById(new Long(brandId));
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL("http://api.infobip.com/api/v3/dr/pull?").openConnection();
            String data = prepareDeliveryStatusData(brandDTO, smsid);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
            conn.getOutputStream().write(data.getBytes("UTF-8"));
            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            final StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                stringBuilder.append(line);
            }
            rd.close();
            String str = stringBuilder.toString();
            logger.info("Delivery API response [InfoBip] : " + str);
            smsStatusDTO = parseResponse(str);
        } catch (Exception e) {
            logger.error("error in getStatus() " + e);
            smsStatusDTO.setStatus(SmsStatusDTO.STATUS.ERROR);
        }
        return smsStatusDTO;
    }

    private String getErrorMessage(int responseCode) {
        String errorMessage = "";
        switch (responseCode) {
            case 0:
                errorMessage = "Request was successful (all recipients)";
                break;
            case -1:
                errorMessage = "Error in processing the request";
                break;
            case -2:
                errorMessage = "Not enough credits";
                break;
            case -3:
                errorMessage = "Targeted network is not covered on specific account";
                break;
            case -5:
                errorMessage = "Username or password is invalid";
                break;
            case -6:
                errorMessage = "Destination address is missing in the request";
                break;
            case -10:
                errorMessage = "Username is missing in the request";
                break;
            case -11:
                errorMessage = "Password is missing in the request";
                break;
            case -13:
                errorMessage = "Number is not recognized ";
                break;
            case -22:
                errorMessage = "Incorrect XML format";
                break;
            case -23:
                errorMessage = "General error";
                break;
            case -26:
                errorMessage = "General API error";
                break;
            case -27:
                errorMessage = "Invalid scheduling parametar";
                break;
            case -28:
                errorMessage = "Invalid PushURL in the request";
                break;
            case -30:
                errorMessage = "Invalid APPID in the request";
                break;
            case -33:
                errorMessage = "Duplicated MessageID in the request";
                break;
            case -34:
                errorMessage = "Sender name is not allowed";
                break;
            case -99:
                errorMessage = "Error in processing request, reasons may vary";
                break;
            default:
                errorMessage = "Error occured";
                break;
        }
        return errorMessage;
    }

//    public static void main(String[] args) {
//        InfobipSMS infobipSMS = new InfobipSMS();
//        infobipSMS.parseResponse("<DeliveryReport><message id=\"205091905013000284\" sentdate=\"2015/09/19 07:01:30\" donedate=\"2015/09/19 07:01:46\" status=\"DELIVERED\" gsmerror=\"0\" price=\"2.87\" /></DeliveryReport>");
//    }
    private String getStatusDetails(String status) {
        String response;
        switch (status) {
            case "NOT_SENT":
                response = "The message is queued in the Infobip system but cannot be submitted to SMSC (possible reason: SMSC connection is down)";
                break;
            case "SENT":
                response = "The message was sent over a route that does not support delivery reports";
                break;
            case "NOT_DELIVERED ":
                response = "The message could not be delivered";
                break;
            case "DELIVERED":
                response = "The message was successfully delivered to the recipient";
                break;
            case "NOT_ALLOWED":
                response = "The client has no authorization to send to the specified network (the message will not be charged)";
                break;
            case "INVALID_DESTINATION_ADDRESS":
                response = "Invalid/incorrect GSM recipient";
                break;
            case "INVALID_SOURCE_ADDRESS":
                response = "You have specified incorrect/invalid/not allowed source address (sender name)";
                break;
            case "ROUTE_NOT_AVAILABLE":
                response = "You are trying to use routing that is not available for your account";
                break;
            case "NOT_ENOUGH_CREDITS ":
                response = "There are no available credits on your account to send the message";
                break;
            case "REJECTED":
                response = "Message has been rejected, reasons ma y vary";
                break;
            case "INVALID_MESSAGE_FORMAT":
                response = "Your message has invalid format";
                break;
            default:
                response = "Unknown error occured";
        }
        return response;
    }
}
