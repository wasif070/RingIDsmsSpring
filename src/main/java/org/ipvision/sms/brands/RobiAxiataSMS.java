package org.ipvision.sms.brands;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.ipvision.domain.SMSRoute;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.service.IMessageService;
import org.ipvision.service.ISMSRouteService;
import org.ipvision.sms.routes.SMSSendManager;
import org.ipvision.sms.routes.SmsStatusDTO;
import org.ipvision.utils.Constants;
import org.ipvision.utils.Utils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class RobiAxiataSMS extends SMSSendManager {

    ISMSRouteService smsRouteService;

    private static Logger logger = Logger.getLogger("authCommunicationLogger");
    private MessagesDTO messagesDTO = new MessagesDTO();
    public String result = Constants.SUCCESS;

    public RobiAxiataSMS(ISMSRouteService smsRouteService, IMessageService messageService) {
        super(messageService);
        this.smsRouteService = smsRouteService;
    }

    @Override
    public MessagesDTO sendSMS(int countryId, String vCode, final int brandId, String mobileNumber, String msg,
            Double rate) {
        try {
            // (1). brandDTO
            brandDTO = smsRouteService.findById(new Long(brandId));
            // (2). Construct data
            String data = prepareData(brandDTO, msg, mobileNumber);

            // (3)-1. Preparing messageDTO
            messagesDTO.setSmsRate(rate);
            messagesDTO.setCountryId(countryId);
            messagesDTO.setvCode(vCode);
            messagesDTO.setVerifiedSend(0);
            messagesDTO.setRoutesId(brandId);
            messagesDTO.setDestination(mobileNumber);

            String sentTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
            logger.debug("sent Date:" + sentTime);
            messagesDTO.setDate(sentTime);
            messagesDTO.setMessage(msg);

            // (4). Send data
            String response = sendData(data, brandDTO);

            // (3)-2. Preparing messageDTO
            messagesDTO.setBrandResponse(response);

            Document doc = Utils.convertStringToDocument(response);
            doc.getDocumentElement().normalize();
            // Double balance = brandDTO.getBalance() - rate;
            NodeList nList = doc.getElementsByTagName("ServiceClass");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    messagesDTO.setResponseId(eElement.getElementsByTagName("MessageId").item(0).getTextContent()); ////////////////////// add
                    ////////////////////// responseId
                    logger.debug("MessageId: " + eElement.getElementsByTagName("MessageId").item(0).getTextContent());
                    Integer errorCode = Integer
                            .valueOf(eElement.getElementsByTagName("ErrorCode").item(0).getTextContent());
                    if (errorCode == 0) {
                        result = Constants.SUCCESS;
                        final String apiMsgID = eElement.getElementsByTagName("MessageId").item(0).getTextContent();
                        messagesDTO.setResponseId(apiMsgID);
                        logger.info("Message submitted successfully and stored in repository");
                    } else {
                        result = Constants.FAILED;
                        messagesDTO.setDlStatus(-1);
                        messagesDTO.setVerifiedSend(-1);
                        messagesDTO.setResponseId("-1");
                        logger.error("Message submission failed [Robi] : " + response);
                        logger.error("Message content : " + msg);
                    }
                    // messagesDTO.setSmsStatus(response);
                    logger.debug("ErrorCode : " + errorCode);
                    if (errorCode != 0) {
                        logger.debug(
                                "ErrorText : " + eElement.getElementsByTagName("ErrorText").item(0).getTextContent());
                    }
                }
            }

        } catch (Exception e) {
            result = Constants.FAILED;
            messagesDTO.setDlStatus(-1);
            messagesDTO.setVerifiedSend(-1);
            messagesDTO.setResponseId("-1");
            String errorText = e.toString();
            logger.error("Error SMS " + errorText);
            messagesDTO.setBrandResponse(
                    messagesDTO.getBrandResponse() != null && messagesDTO.getBrandResponse().length() < 1 ? errorText
                    : messagesDTO.getBrandResponse());
        }

        // (6). add to DB
        messagesDTO.setSmsStatus(result);
        messagesDTO.setDbInsert(addToDB(messagesDTO));

        return messagesDTO;
    }

    @Override
    public String prepareData(SMSRoute brandDTO, String msg, String mobileNumber) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Username=").append(brandDTO.getUserName()).append("&Password=")
                .append(brandDTO.getPassword()).append("&Message=").append(msg).append("&From=")
                .append(brandDTO.getSenderId()).append("&To=").append(mobileNumber);
        return stringBuilder.toString();
    }

    @Override
    public String prepareDeliveryStatusData(SMSRoute brandDTO, String messageId) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Username=").append(brandDTO.getUserName()).append("&Password=")
                .append(brandDTO.getPassword()).append("&MessageId=").append(messageId);
        return stringBuilder.toString();
    }

    public SmsStatusDTO getDeliveryStatus(int brandId, String smsid) {
        SmsStatusDTO smsStatusDTO = new SmsStatusDTO();
        brandDTO = smsRouteService.findById(new Long(brandId));
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(
                    "https://bmpws.robi.com.bd/ApacheGearWS/GetMessageStatus").openConnection();
            String data = prepareDeliveryStatusData(brandDTO, smsid);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
            conn.getOutputStream().write(data.getBytes("UTF-8"));
            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            final StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                stringBuilder.append(line);
            }
            rd.close();
            String str = stringBuilder.toString();
            System.err.println("########### --> " + str);
            logger.info("Delivery API response [Robi] : " + str);
            smsStatusDTO = parseResponse(str);
        } catch (Exception e) {
            logger.error("error in getStatus() " + e);
            smsStatusDTO.setStatus(SmsStatusDTO.STATUS.ERROR);
        }
        return smsStatusDTO;
    }

    @Override
    public SmsStatusDTO parseResponse(String response) {
        SmsStatusDTO smsStatusDTO = new SmsStatusDTO();
        System.out.println("response ==> " + response);
        Document doc = Utils.convertStringToDocument(response);
        doc.getDocumentElement().normalize();
        NodeList nList = doc.getElementsByTagName("ServiceClass");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                String status = eElement.getElementsByTagName("Status").item(0).getTextContent();
                if (status.equals("1")) {
                    smsStatusDTO.setStatus(SmsStatusDTO.STATUS.SUCCESS);
                    smsStatusDTO.setResponseCode(status);
                } else if (status.equals("-1")) {
                    smsStatusDTO.setStatus(SmsStatusDTO.STATUS.ERROR);
                    smsStatusDTO.setResponseText(eElement.getElementsByTagName("Status Text").item(0).getTextContent());
                    smsStatusDTO.setResponseCode(status);
                    smsStatusDTO
                            .setResponseDetails(eElement.getElementsByTagName("Status Text").item(0).getTextContent());
                }
                System.out.println("smsStatus --> " + smsStatusDTO.toString());
            }
        }

        return smsStatusDTO;
    }

    public static void main(String[] args) {
        RobiAxiataSMS robi = new RobiAxiataSMS(null, null);
        String response = "<?xml version=\"1.0\" encoding=\"utf-8\"?><ServiceClass xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://cmp.aktel.com/\">  <MessageId>0</MessageId>  <Status>-1</Status>  <StatusText>N/A</StatusText>  <ErrorCode>-3</ErrorCode>  <ErrorText>Insufficient credit</ErrorText>  <SMSCount>1</SMSCount>  <CurrentCredit>0</CurrentCredit></ServiceClass>";

        response = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><ServiceClass xmlns=\"http://cmp.aktel.com/\"><MessageId>54281820</MessageId><Status>0</Status><StatusText>pending</StatusText><ErrorCode>0</ErrorCode><SMSCount>1</SMSCount><CurrentCredit>198339</CurrentCredit></ServiceClass>";
        robi.parseResponse(response);
    }
}
