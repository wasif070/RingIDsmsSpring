package org.ipvision.sms.brands.notUsedAnyMore;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import org.apache.log4j.Logger;
import org.ipvision.domain.SMSRoute;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.service.ISMSRouteService;
import org.ipvision.utils.MyAppError;
import com.plivo.helper.api.client.RestAPI;
import com.plivo.helper.api.response.message.MessageResponse;
import com.plivo.helper.exception.PlivoException;
import org.ipvision.service.IMessageService;
import org.ipvision.sms.routes.SMSSendManager;
import org.ipvision.sms.routes.SmsStatusDTO;

public class PlivoApi extends SMSSendManager {

    ISMSRouteService sMSRouteService;

    IMessageService messageRepository;

    static Logger logger = Logger.getLogger(PlivoApi.class.getName());
    private SMSRoute brandDTO = new SMSRoute();
    private MessagesDTO messagesDTO = new MessagesDTO();
    private ArrayList<MessagesDTO> msgInfoList = new ArrayList<>();
    private final static String authToken = "ZDhjOGQ0YmVmOTZhM2M5ZTljODk4NWEzNTIxYmRl";
    private String message_state;

    public PlivoApi(ISMSRouteService smsRouteService, IMessageService messageService) {
        super(messageService);
        this.sMSRouteService = smsRouteService;
        this.messageRepository = messageService;
    }

    @Override
    public MessagesDTO sendSMS(int countryId, String vCode, int brandId, String mobileNumber, String msg, Double rate) {
        String result = "";
        brandDTO = sMSRouteService.findById(new Long(brandId));
        RestAPI api = new RestAPI(brandDTO.getApiId(), authToken, "v1");
        LinkedHashMap<String, String> parameters = new LinkedHashMap<>();
        parameters.put("src", brandDTO.getSenderId());
        parameters.put("dst", mobileNumber);
        parameters.put("text", msg);
        parameters.put("url", brandDTO.getApiUrl());
        double balance = brandDTO.getBalance() - rate;
        try {
            MessageResponse msgResponse = api.sendMessage(parameters);
            logger.debug(msgResponse.apiId);
            messagesDTO.setResponseId((String) msgResponse.messageUuids.get(0));
            if (msgResponse.serverCode == 202) {
//                this.init(balance, brandId, brandDTO);
//                this.start();
                // String response = getDalReport((String)msgResponse.messageUuids.get(0).toString(),brandId);
                messagesDTO.setBrandResponse(msgResponse.apiId + ":smsId" + msgResponse.messageUuids.get(0));
                result = "success";
            } else {
                result = "failed";
                messagesDTO.setDlStatus(0);
                messagesDTO.setBrandResponse("" + msgResponse.error);
                logger.error(msgResponse.error);
            }
            messagesDTO.setSmsRate(rate);
            messagesDTO.setCountryId(countryId);
            messagesDTO.setvCode(vCode);
            messagesDTO.setVerifiedSend(0);
            messagesDTO.setRoutesId(brandId);
            messagesDTO.setDestination(mobileNumber);
            messagesDTO.setMessage(msg);
            messagesDTO.setSmsStatus(result);
            logger.debug("send Date:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
            messagesDTO.setDate(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
            msgInfoList.add(messagesDTO);
            MyAppError error = messageRepository.addMessage(messagesDTO);
            if (error.getERROR_TYPE() > 0) {
                logger.debug("insert failed");
            }
        } catch (PlivoException e) {
            logger.error(e.getLocalizedMessage());
            result = "failed";
        }
        messagesDTO.setSmsStatus(result);
        return messagesDTO;
    }

    public int getDalReport(int brandId, String messageId) {
        try {
            RestAPI api1 = new RestAPI(sMSRouteService.findById(new Long(brandId)).getApiId(), authToken, "v1");
            LinkedHashMap<String, String> parameter = new LinkedHashMap<>();
            parameter.put("record_id", messageId);
            com.plivo.helper.api.response.message.Message finalResponse = api1.getMessage(parameter);
            Gson gson = new Gson();
            PlivoApi s = gson.fromJson(new Gson().toJson(finalResponse), PlivoApi.class);

            if (s.message_state.contains("delivered") || s.message_state.contains("sent")) {
                return 1;
            } else {
                return -1;
            }
        } catch (PlivoException e) {
            return -2;
        }
    }

    @Override
    public String prepareData(SMSRoute brandDTO, String msg, String mobileNumber) {
        throw new UnsupportedOperationException("Implementation not needed"); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String prepareDeliveryStatusData(SMSRoute brandDTO, String messageId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SmsStatusDTO parseResponse(String response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
