package org.ipvision.sms.brands.notUsedAnyMore;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.apache.log4j.Logger;
import org.ipvision.domain.SMSRoute;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.repository.MessageRepository;
import org.ipvision.service.IMessageService;
import org.ipvision.service.ISMSRouteService;
import org.ipvision.utils.MyAppError;
import org.ipvision.sms.routes.SMSSendManager;
import org.ipvision.sms.routes.SmsStatusDTO;
import org.ipvision.utils.Utils;
import org.ipvision.utils.Constants;

public class ClicknGoBD extends SMSSendManager {

    ISMSRouteService sMSRouteService;
    MessageRepository messageRepository;

    static Logger logger = Logger.getLogger(ClicknGoBD.class.getName());
//    private BrandsDTO brandDTO = new BrandsDTO();
    private MessagesDTO messagesDTO = new MessagesDTO();
    private ArrayList<MessagesDTO> msgInfoList = new ArrayList<>();
    public String result = "";

    public ClicknGoBD(ISMSRouteService smsRouteService, IMessageService messageService, MessageRepository messageRepository) {
        super(messageService);
        this.sMSRouteService = smsRouteService;
        this.messageRepository = messageRepository;
    }

    @Override
    public MessagesDTO sendSMS(int countryId, String vCode, int brandId, String mobileNumber, String msg, Double rate) {
        try {
            brandDTO = sMSRouteService.findById(new Long(brandId));
// Construct data
            String data = prepareData(brandDTO, msg, mobileNumber);
// Send data
            String response = sendData(data, brandDTO);
            try {
                Document doc = Utils.convertStringToDocument(response);
                doc.getDocumentElement().normalize();
                logger.debug("Root element :" + doc.getDocumentElement().getNodeName());
                NodeList nList = doc.getElementsByTagName("results");
                Double balance = brandDTO.getBalance() - rate;
                for (int temp = 0; temp < nList.getLength(); temp++) {
                    Node nNode = nList.item(temp);
                    logger.debug("Current Element :" + nNode.getNodeName());
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        logger.debug(eElement.getElementsByTagName("status").item(0).getTextContent());
                        if (String.valueOf(eElement.getElementsByTagName("status").item(0).getTextContent()).equalsIgnoreCase("0")) {
                            messagesDTO.setSmsStatus("success");
                            result = "success";
                        } else {
                            messagesDTO.setSmsStatus("failed");
                            messagesDTO.setDlStatus(-1);
                            result = "failed";
                        }
                        messagesDTO.setResponseId(eElement.getElementsByTagName("messageid").item(0).getTextContent());  //////////////////////add responseId
                        logger.debug(eElement.getElementsByTagName("messageid").item(0).getTextContent());
                    }
                }
            } catch (Exception e) {
                logger.debug("Error SMS " + e);
                result = Constants.FAILED;
            }
            messagesDTO.setSmsRate(rate);
            messagesDTO.setCountryId(countryId);
            messagesDTO.setvCode(vCode);
            messagesDTO.setVerifiedSend(0);
            messagesDTO.setRoutesId(brandId);
            messagesDTO.setDestination(mobileNumber);
            messagesDTO.setMessage(msg);
            messagesDTO.setBrandResponse(response);
            logger.debug("ClicknGoBD, send Date:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
            messagesDTO.setDate(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
            msgInfoList.add(messagesDTO);
            MyAppError error = messageRepository.addMessage(messagesDTO);
            if (error.getERROR_TYPE() > 0) {
                logger.debug("insert failed");
                result = Constants.FAILED;
            } else {
                result = Constants.SUCCESS;
            }
        } catch (Exception e) {
            logger.debug("Error SMS " + e);
            result = Constants.FAILED;
        }
        messagesDTO.setSmsStatus(result);
        return messagesDTO;
    }

    @Override
    public String prepareData(SMSRoute brandDTO, String msg, String mobileNumber) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("user=").append(brandDTO.getUserName())
                .append("&password=").append(brandDTO.getPassword())
                .append("&GSM=").append(mobileNumber)
                .append("&SMSText=").append(msg)
                .append("&sender=").append(brandDTO.getSenderId());
        return stringBuilder.toString();
    }

    @Override
    public String prepareDeliveryStatusData(SMSRoute brandDTO, String messageId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SmsStatusDTO parseResponse(String response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
