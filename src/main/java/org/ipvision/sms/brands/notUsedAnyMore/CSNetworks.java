package org.ipvision.sms.brands.notUsedAnyMore;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import org.ipvision.domain.SMSRoute;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.service.IMessageService;
import org.ipvision.service.ISMSRouteService;
import org.ipvision.sms.routes.SMSSendManager;
import org.ipvision.sms.routes.SmsStatusDTO;
import org.ipvision.utils.Constants;

public class CSNetworks extends SMSSendManager {

    ISMSRouteService smsRouteService;

    private static Logger logger = Logger.getLogger("authCommunicationLogger");
//    private BrandsDTO brandDTO = new BrandsDTO();

    public String result = Constants.SUCCESS;

    public CSNetworks(ISMSRouteService smsRouteService, IMessageService messageService) {
        super(messageService);
        this.smsRouteService = smsRouteService;
    }

    @Override
    public MessagesDTO sendSMS(int countryId, String vCode, int brandId, String mobileNumber, String msg, Double rate) {
        MessagesDTO messagesDTO = new MessagesDTO();

        try {
            // (1). brandDTO
            brandDTO = smsRouteService.findById(new Long(brandId));
            // (2). Construct data
            String data = prepareData(brandDTO, msg, mobileNumber);

            // (3)-1. Preparing messageDTO
            messagesDTO.setSmsRate(rate);
            messagesDTO.setCountryId(countryId);
            messagesDTO.setvCode(vCode);
            messagesDTO.setVerifiedSend(0);
            messagesDTO.setRoutesId(brandId);
            messagesDTO.setDestination(mobileNumber);
//            double balance = brandDTO.getBalance() - rate;

            String sentTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
            logger.debug("sent Date:" + sentTime);
            messagesDTO.setDate(sentTime);
            messagesDTO.setMessage(msg);

            // (4). Send data
            String response = sendData(data, brandDTO);

            // (3)-2. Preparing messageDTO
            messagesDTO.setBrandResponse(response);

            logger.debug("CSNetworks response : " + response);

            // (5). Response analysis
            if ((response.substring(response.length() - 2, response.length())).equalsIgnoreCase("OK")) {
                result = Constants.SUCCESS;
                final String apiMsgID = response.substring(0, response.length() - 2);
                messagesDTO.setResponseId(apiMsgID);
                logger.info("Message submitted successfully and stored in repository");
            } else {
                result = Constants.FAILED;
                messagesDTO.setDlStatus(-1);
                messagesDTO.setVerifiedSend(-1);
                messagesDTO.setResponseId("-1");
                logger.error("Message submission failed : " + response);
                logger.error("Message content : " + msg);
            }
        } catch (Exception e) {
            result = Constants.FAILED;
            messagesDTO.setDlStatus(-1);
            messagesDTO.setVerifiedSend(-1);
            messagesDTO.setResponseId("-1");
            String errorText = e.toString();
            logger.error("Error SMS " + errorText);
            messagesDTO.setBrandResponse(messagesDTO.getBrandResponse() != null && messagesDTO.getBrandResponse().length() < 1 ? errorText : messagesDTO.getBrandResponse());
        }

        // (6). add to DB
        messagesDTO.setSmsStatus(result);
        messagesDTO.setDbInsert(addToDB(messagesDTO));

        return messagesDTO;
    }

//    logger.error("(CSNetwork) DB insert failed (Exception) --> " + e + "\n" + messagesDTO.toString());
    @Override
    public String prepareData(SMSRoute brandDTO, String msg, String mobileNumber) {
        StringBuilder stringbuilder = new StringBuilder();
        stringbuilder
                .append("USERNAME=").append(brandDTO.getUserName())
                .append("&PASSWORD=").append(brandDTO.getPassword())
                .append("&MESSAGE=").append(msg)
                .append("&SOURCEADDR=").append(brandDTO.getSenderId())
                .append("&DESTADDR=").append(mobileNumber);
        return stringbuilder.toString();
    }

    @Override
    public String prepareDeliveryStatusData(SMSRoute brandDTO, String messageId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SmsStatusDTO parseResponse(String response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
