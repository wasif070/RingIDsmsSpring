package org.ipvision.sms.brands.notUsedAnyMore;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;
import org.ipvision.domain.SMSRoute;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.service.IMessageService;
import org.ipvision.service.ISMSRouteService;
import org.ipvision.sms.routes.SMSSendManager;
import org.ipvision.sms.routes.SmsStatusDTO;
import org.ipvision.utils.Constants;
import org.ipvision.utils.MyAppError;

public class ACL extends SMSSendManager {

    ISMSRouteService smsRouteService;

    IMessageService messageService;

    static Logger logger = Logger.getLogger("authCommunicationLogger");
    private final MessagesDTO messagesDTO = new MessagesDTO();
    private final ArrayList<MessagesDTO> msgInfoList = new ArrayList<>();
    private static final String appid = "ipvision";
    public String result = "";

    public ACL(ISMSRouteService smsRouteService, IMessageService messageService) {
        super(messageService);
        this.smsRouteService = smsRouteService;
    }

    @Override
    public MessagesDTO sendSMS(int countryId, String vCode, int brandId, String mobileNumber, String msg, Double rate) {
        try {
            brandDTO = smsRouteService.findById(new Long(brandId));
            String data = prepareData(brandDTO, msg, mobileNumber);
            String response = sendData(data, brandDTO);
            double balance = brandDTO.getBalance() - rate;
            messagesDTO.setSmsRate(rate);
            messagesDTO.setCountryId(countryId);
            messagesDTO.setvCode(vCode);
            messagesDTO.setResponseId(response);
            messagesDTO.setVerifiedSend(0);
            messagesDTO.setRoutesId(brandId);
            messagesDTO.setDestination(mobileNumber);
            messagesDTO.setMessage(msg);
            if (!response.startsWith(appid)) {
                String errorMessage = getErrorDescription(response);
                logger.error("ACL errorMessage --> " + errorMessage);
                messagesDTO.setBrandResponse(errorMessage);
                result = Constants.FAILED;
                messagesDTO.setDlStatus(-1);
            } else {
                // this.init(balance, brandId, brandDTO);
                // this.start();
                messagesDTO.setBrandResponse(response);
                result = Constants.SUCCESS;
            }
            logger.debug("response no: " + response + "mobile No:" + mobileNumber);
            messagesDTO.setSmsStatus(result);
            logger.debug("ACL, send Date:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
            messagesDTO.setDate(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
            msgInfoList.add(messagesDTO);
            MyAppError error = messageService.addMessage(messagesDTO);
            if (error.getERROR_TYPE() > 0) {
                logger.debug("insert failed");
                result = Constants.FAILED;
            }

        } catch (Exception e) {
            logger.debug("Error SMS " + e);
            result = Constants.FAILED;
        }
        messagesDTO.setSmsStatus(result);
        return messagesDTO;
    }

    @Override
    public String prepareData(SMSRoute brandDTO, String msg, String mobileNumber) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("userId=").append(brandDTO.getUserName()).append("&pass=").append(brandDTO.getPassword())
                .append("&appid=").append(appid).append("&subappid=").append("ipvision").append("&contenttype=")
                .append(1).append("&to=").append(mobileNumber).append("&from=").append(brandDTO.getSenderId())
                .append("&text=").append(msg).append("&selfid=").append(true).append("&alert=").append(1)
                .append("&dlrreq=").append(true).append("&intflag=").append(true);
        return stringBuilder.toString();
    }

    private String getErrorMessage(int responseCode) {
        String errorMessage = "";
        switch (responseCode) {
            case -1:
                errorMessage = "Enterprise Id Missing";
                break;
            case -2:
                errorMessage = "User Id Missing";
                break;
            case -3:
                errorMessage = "Password Missing";
                break;
            case -4:
                errorMessage = "Content type Missing";
                break;
            case -5:
                errorMessage = "Sender Missing";
                break;
            case -6:
                errorMessage = "MSISDN Missing";
                break;
            case -7:
                errorMessage = "Message Text Missing";
                break;
            case -8:
                errorMessage = "Message Id Missing";
                break;
            case -9:
                errorMessage = "WAP Push URL Missing";
                break;
            case -10:
                errorMessage = "Authentication Failed";
                break;
            case -11:
                errorMessage = "Service Blocked for User";
                break;
            case -12:
                errorMessage = "Repeated Message Id Received";
                break;
            case -13:
                errorMessage = "Invalid Content Type Received";
                break;
            case -14:
                errorMessage = "International Messages Not Allowed";
                break;
            case -15:
                errorMessage = "Incomplete or Invalid XML Packet Received";
                break;
            case -16:
                errorMessage = "Invalid DND Flag value";
                break;
            case -17:
                errorMessage = "Direct Pushing Not Allowed";
                break;
            case -18:
                errorMessage = "CLI not registered";
                break;
            case -19:
                errorMessage = "Operator Specific MSISDN Blocked";
                break;
            default:
                errorMessage = "Error occured";
                break;
        }
        return errorMessage;
    }

    private String getErrorDescription(String str) {
        // <status>-8</status><desc>Message Id is missing</desc>
        String startingTagStr = "<desc>";
        String endTagStr = "</desc>";
        if (!str.contains(startingTagStr) || !str.contains(endTagStr)) {
            return "Error Desc Not Found";
        }
        return str.substring(str.indexOf(startingTagStr) + startingTagStr.length(), str.indexOf(endTagStr));
    }

    @Override
    public String prepareDeliveryStatusData(SMSRoute brandDTO, String messageId) {
        throw new UnsupportedOperationException("Not supported yet."); // To
        // change
        // body
        // of
        // generated
        // methods,
        // choose
        // Tools
        // |
        // Templates.
    }

    @Override
    public SmsStatusDTO parseResponse(String response) {
        throw new UnsupportedOperationException("Not supported yet."); // To
        // change
        // body
        // of
        // generated
        // methods,
        // choose
        // Tools
        // |
        // Templates.
    }
}
