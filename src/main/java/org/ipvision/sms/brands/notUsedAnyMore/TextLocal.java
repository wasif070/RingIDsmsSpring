package org.ipvision.sms.brands.notUsedAnyMore;

import org.ipvision.domain.SMSRoute;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.repository.MessageRepository;
import org.ipvision.service.ISMSRouteService;
import org.ipvision.sms.routes.SMSSendManager;
import org.ipvision.utils.Constants;
import org.ipvision.utils.MyAppError;
import org.springframework.beans.factory.annotation.Autowired;
import com.google.gson.Gson;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.apache.log4j.Logger;
import org.ipvision.service.IMessageService;
import org.ipvision.sms.routes.SmsStatusDTO;

public class TextLocal extends SMSSendManager {

    MessageRepository messageRepository;
    ISMSRouteService smsouteISMSRouteService;

    static Logger logger = Logger.getLogger(TextLocal.class.getName());
    private SMSRoute brandDTO = new SMSRoute();
    private MessagesDTO messagesDTO = new MessagesDTO();
    private ArrayList<MessagesDTO> msgInfoList = new ArrayList<>();
    private String batch_id;

    public TextLocal(ISMSRouteService smsRouteService, IMessageService messageService, MessageRepository messageRepository) {
        super(messageService);
        this.smsouteISMSRouteService = smsRouteService;
        this.messageRepository = messageRepository;
    }
    
    @Override
    public MessagesDTO sendSMS(int countryId, String vCode, int brandId, String mobileNumber, String msg, Double rate) {
        String result;
        try {
            brandDTO = smsouteISMSRouteService.findById(new Long(brandId));

            // Construct data
            String data = prepareData(brandDTO, msg, mobileNumber);

            // Send data
            String response = sendData(data, brandDTO);

            Gson gson = new Gson();
            TextLocal thing = gson.fromJson(response, TextLocal.class);
            if (thing.batch_id != null) {
                messagesDTO.setResponseId(thing.batch_id);  //////////////////////add responseId
                logger.debug("" + thing.batch_id);
            }

            double balance = brandDTO.getBalance() - rate;
            messagesDTO.setSmsRate(rate);
            messagesDTO.setCountryId(countryId);
            messagesDTO.setvCode(vCode);
            messagesDTO.setVerifiedSend(0);
            messagesDTO.setRoutesId(brandId);
            messagesDTO.setDestination(mobileNumber);
            messagesDTO.setMessage(msg);
            messagesDTO.setBrandResponse(response);

            if (response != null) {

//                this.init(balance, brandId, brandDTO);
//                this.start();
                messagesDTO.setSmsStatus("success");
            } else {
                messagesDTO.setSmsStatus("failed");
                result = Constants.SUCCESS;
            }
            logger.debug("send Date:" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
            messagesDTO.setDate(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
            msgInfoList.add(messagesDTO);

            MyAppError error = messageRepository.addMessage(messagesDTO);
            if (error.getERROR_TYPE() > 0) {
                logger.debug("insert failed");
                result = Constants.FAILED;
            } else {
                result = Constants.SUCCESS;
            }
        } catch (Exception e) {
            logger.debug("Execption on textLocal SMS Routes ");
            e.printStackTrace();
            result = Constants.FAILED;
        }
        messagesDTO.setSmsStatus(result);
        return messagesDTO;
    }

    @Override
    public String prepareData(SMSRoute brandDTO, String msg, String mobileNumber) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("&message=").append(msg)
                .append("&sender=").append(brandDTO.getSenderId())
                .append("&numbers=").append(mobileNumber)
                .append("username=").append(brandDTO.getUserName())
                .append("&hash=").append(brandDTO.getPassword());
        return stringBuilder.toString();
    }

    @Override
    public String prepareDeliveryStatusData(SMSRoute brandDTO, String messageId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SmsStatusDTO parseResponse(String response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
