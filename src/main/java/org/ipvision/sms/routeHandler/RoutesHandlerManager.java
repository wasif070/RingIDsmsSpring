package org.ipvision.sms.routeHandler;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.service.ICountryService;
import org.ipvision.service.IMessageService;
import org.ipvision.service.IRateplanService;
import org.ipvision.service.ISMSRouteService;
import org.ipvision.sms.routes.RoutesController;
import org.ipvision.sms.routes.SMSDeliveryChecker;
import org.ipvision.utils.Constants;
import org.ipvision.utils.MyAppError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

@Service
@Configurable
public class RoutesHandlerManager {

    @Autowired
    ICountryService countryService;

    @Autowired
    IMessageService messageService;

    @Autowired
    IRateplanService rateplanService;

    @Autowired
    ISMSRouteService smsRouteService;

    private String response;

    static Logger logger = Logger.getLogger("authCommunicationLogger");

    public RoutesHandlerManager() {
    }

    public String sentSMSByRoutesId(String vCode, int brandId, String countryCode, String mobileNo, String message) {
        try {
            String dailNumber = countryCode.trim().concat(mobileNo.trim());
            if (dailNumber.startsWith("+")) {
                dailNumber = dailNumber.substring(1, dailNumber.length());
            }
            int countryId = countryService.findByCode(countryCode).getId().intValue();

            if (countryId > 0) {
                logger.debug("CountryId : " + countryId + " brandId : " + brandId);

                RoutesController routesController = new RoutesController(countryId, vCode, brandId, dailNumber, message, rateplanService, smsRouteService, messageService);
                //routesController.init(countryId, vCode, brandId, dailNumber, message);
                response = routesController.rController().getSmsStatus();
                logger.debug("message sent by " + brandId + "and response " + response);

                //testing purpose [below code]
                if (response.equals("success")) {
                    ArrayList<Integer> list = new ArrayList<>();
//                    list.add(12); 
//                    list.add(10);
//                    list.add(1);
//                    list.add(8);
                    SMSDeliveryChecker sMSDeliveryChecker = new SMSDeliveryChecker(brandId, list, routesController, messageService, smsRouteService);
                    //sMSDeliveryChecker.init(brandId, list, routesController);
                    sMSDeliveryChecker.start();
                }
            } else {
                logger.error("CountryId not found for countryCode --> " + countryCode);
                response = Constants.FAILED;
            }

        } catch (Exception e) {
            logger.debug("execption in sentSMSByRoutesId" + e);
            response = Constants.FAILED;
        }
        return response;
    }

    public String findRoutes(String vCode, String countryCode, String mobileNo, String message) {
        try {
            int countryId = countryService.findByCode(countryCode).getId().intValue();
            if (countryId > 0) {

                List<Integer> brandList = messageService.getBrandId(countryId);
                logger.debug("brandList size: " + brandList.size());

                String destinationNumber = countryCode.trim().concat(mobileNo.trim());
                if (destinationNumber.startsWith("+")) {
                    destinationNumber = destinationNumber.substring(1, destinationNumber.length());
                }

                int ind = 0;
                for (Integer brandID : brandList) {
                    if (brandID > 0) {
                        RoutesController routesController = new RoutesController(countryId, vCode, brandID, destinationNumber, message, rateplanService, smsRouteService, messageService);
                        //routesController.init(countryId, vCode, brandID, destinationNumber, message);
                        response = routesController.rController().getSmsStatus();

                        logger.debug("message sent by " + brandID + "and response " + response);

                        if (response.equals("success")) {
                            ArrayList<Integer> list = new ArrayList<>(brandList.subList(ind, brandList.size()));

                            SMSDeliveryChecker sMSDeliveryChecker = new SMSDeliveryChecker(brandID, list, routesController, messageService, smsRouteService);
                            //sMSDeliveryChecker.init(brandID, list, routesController);
                            sMSDeliveryChecker.start();

                            logger.debug("message sent by (first attempt) " + brandID + " --> options --> " + list.toString());
                            return response;
                        }
                    }
                    ind++;
                }

                if (!response.equals("success")) {
                    RoutesController routesController = new RoutesController(countryId, vCode, 3, destinationNumber, message, rateplanService, smsRouteService, messageService);
                    //routesController.init(countryId, vCode, 3, destinationNumber, message);
                    response = routesController.rController().getSmsStatus();
                    if (response.equals("success")) {
                        logger.debug("message sent succefully by 3");
                        return response;
                    }
                }
            } else {
                logger.error("CountryId not found for countryCode --> " + countryCode);
                response = Constants.FAILED;
            }
        } catch (NullPointerException e) {
            logger.debug("execption on find routes" + e);
            response = "failed";
        }
        return response;
    }

    public int verifiedUpdate(String vCode, String mobileNo, String date) {
        MessagesDTO dto = new MessagesDTO();
        MyAppError error;
        int returnVal;
        logger.debug("varified update " + vCode + "mobile No:" + mobileNo);
        if ((vCode != null && vCode.length() > 0) && (mobileNo != null && mobileNo.length() > 0)) {
            dto.setvCode(vCode);
            dto.setDestination(mobileNo);
            dto.setReceivedDate(date);
            error = messageService.updateSuccessfulMessageVerificationStatus(dto);

            if (error.getERROR_TYPE() > 0) {
                logger.error("error in verifiedUpdate --> " + error.getErrorMessage());
                returnVal = 0;
            } else {
                returnVal = 1;
            }
        } else {
            returnVal = 0;
        }
        return returnVal;
    }

    public String verificationCode(String mobileNo) {
        return messageService.verificationCode(mobileNo);
    }
}
