package org.ipvision.sms.routeHandler;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import org.ipvision.utils.Constants;
import org.ipvision.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@WebServlet(name = "VerificationHandler", urlPatterns = {"/VerificationHandler"})
public class VerificationHandler extends HttpServlet {

    @Autowired
    RoutesHandlerManager routesHandlerManager;

    private String mobileNo = "";
    private String verificationCode = "";
    static Logger logger = Logger.getLogger("authCommunicationLogger");

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            //01. ipaddress detection
            logger.debug("request ip " + Utils.detectIP(request));

            //02. mobileNo & verificationCode
            if (request.getParameter("vCode") != null && request.getParameter("mobileNumber") != null) {
                mobileNo = request.getParameter("mobileNumber");
                verificationCode = request.getParameter("vCode");
            } else {
                FileItemFactory fileFactory = new DiskFileItemFactory();
                ServletFileUpload fileUpload = new ServletFileUpload(fileFactory);
                List uploadParameters;
                try {
                    uploadParameters = fileUpload.parseRequest(request);

                    Iterator uploadParametersIterator = uploadParameters.iterator();
                    org.apache.commons.fileupload.FileItem uploadParameter;
                    while (uploadParametersIterator.hasNext()) {
                        uploadParameter = (org.apache.commons.fileupload.FileItem) uploadParametersIterator.next();
                        if (uploadParameter.isFormField()) {
                            String name = uploadParameter.getFieldName();
                            String value = uploadParameter.getString("UTF-8");
                            if (name.endsWith(Constants.MOBILE_NUMBER)) {
                                mobileNo = value;
                            } else if (name.endsWith(Constants.VERIFICATION_CODE)) {
                                verificationCode = value;
                            }
                        }
                    }
                } catch (FileUploadException e) {
                    logger.error("Exception in VerificationHandler [fileUpload.parseRequest(request)]", e);
                    out.print(0);
                    return;
                }
            }

            //03. log details/params
            String date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
            if (mobileNo.startsWith("+")) {//String MobileNumber = countryCode + mobileNo;
                mobileNo = mobileNo.substring(1, mobileNo.length());
            }
            logger.debug("Receiced Data: verification code : " + verificationCode + " Mobile Numbner: " + mobileNo + "Receive Date:" + date);

            //04. update db
            //RoutesHandlerManager routesHandlerManager = new RoutesHandlerManager();
            int result = routesHandlerManager.verifiedUpdate(verificationCode, mobileNo, date);

            logger.debug("SMS Sending " + (result == 1 ? "successful" : "failed"));
            out.print(result);
        } catch (Exception e) {
            logger.debug("Exception in VerificationHandler ", e);
            out.print(0);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        System.out.println("Test");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
                config.getServletContext());
    }
}
