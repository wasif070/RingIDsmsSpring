package org.ipvision.sms.routeHandler;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import org.ipvision.utils.Constants;
import org.ipvision.utils.MyAppError;
import org.ipvision.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/SMSRoutesHandler")
public class SMSRoutesHandler {

    @Autowired
    RoutesHandlerManager routesHandlerManager;

    static Logger logger = Logger.getLogger("authCommunicationLogger");

    @PostMapping({"", "/"})
    void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        String contextPath = request.getContextPath();

        PrintWriter out = response.getWriter();
        try {
            // 01. ipaddress detection
            logger.debug("request ip " + Utils.detectIP(request));

            String countryCode = "";
            String brandId = "";
            String mobileNo = "";
            String message = "";
            String verificationCode = "";

            // 02. countryCode & brandId & mobileNo & message & verificationCode
            if (request.getParameter("countryCode") != null && request.getParameter("mobileNumber") != null) {
                countryCode = request.getParameter("countryCode");
                mobileNo = request.getParameter("mobileNumber");
                message = request.getParameter("textMessage");
                verificationCode = request.getParameter("vCode");
                brandId = request.getParameter("brandId");
                System.out.println("Mobile No : " + mobileNo);
            } else {
                FileItemFactory fileFactory = new DiskFileItemFactory();
                ServletFileUpload fileUpload = new ServletFileUpload(fileFactory);
                List uploadParameters;
                try {
                    uploadParameters = fileUpload.parseRequest(request);
                    Iterator uploadParametersIterator = uploadParameters.iterator();
                    org.apache.commons.fileupload.FileItem uploadParameter;
                    while (uploadParametersIterator.hasNext()) {
                        uploadParameter = (org.apache.commons.fileupload.FileItem) uploadParametersIterator.next();
                        if (uploadParameter.isFormField()) {
                            String name = uploadParameter.getFieldName();
                            String value = uploadParameter.getString("UTF-8");
                            if (name.endsWith(Constants.COUNTRY_CODE)) {
                                countryCode = value;
                            } else if (name.endsWith(Constants.BRAND_ID)) {
                                mobileNo = value;
                            } else if (name.endsWith(Constants.MOBILE_NUMBER)) {
                                mobileNo = value;
                            } else if (name.endsWith(Constants.SMS_MESSAGE)) {
                                message = value;
                            } else if (name.endsWith(Constants.VERIFICATION_CODE)) {
                                verificationCode = value;
                            }
                        }
                    }
                } catch (FileUploadException e) {
                    logger.error("FileUploadException in SMSRoutesHandler [fileUpload.parseRequest(request)]", e);
                    out.print(0);
                    return;
                } catch (Exception e) {
                    logger.error("Exception in SMSRoutesHandler [fileUpload.parseRequest(request)]", e);
                    out.print(0);
                    return;
                }
            }

            // 03. log details/params
            // String MobileNumber = countryCode + mobileNo;
            String result;
            logger.debug("Receiced Data: countryCOde: " + countryCode + " Mobile Numbner: " + mobileNo + " sms : "
                    + message + " BrandId : " + brandId);

            // 04. update db
            //RoutesHandlerManager routesHandlerManager = new RoutesHandlerManager();
            if (brandId != null && brandId.length() > 0) {
                result = routesHandlerManager.sentSMSByRoutesId(verificationCode, Integer.parseInt(brandId),
                        countryCode, mobileNo, message);
            } else {
                result = routesHandlerManager.findRoutes(verificationCode, countryCode, mobileNo, message);
            }

            logger.debug("SMS Sending Result: " + result);

            out.print(result.equals(Constants.SUCCESS) ? 1 : 0);

            // 05. sending response
            MyAppError myAppError = new MyAppError();
            if (result.equalsIgnoreCase("success")) {
                myAppError.setErrorMessage("Message sent successfully!");
                request.setAttribute("success", myAppError.getErrorMessage());
            } else {
                myAppError.setErrorMessage("Message sending failed!");
                request.setAttribute("success", myAppError.getErrorMessage());
            }

//			request.getRequestDispatcher(contextPath + "/send/sms").forward(request, response);
            // get back to order.jsp page using forward
            response.sendRedirect(contextPath + "/send/sms?success=" + myAppError.getErrorMessage());
            // request.getRequestDispatcher("/i18n/sendsms/send-sms.html").forward(request,
            // response);
        } catch (Exception e) {
            logger.debug("Exception in SMSRoutesHandler ", e);
            response.sendRedirect(contextPath + "/send/sms?success=" + "Message Sending failed!");
//			request.setAttribute("success", "Exception occured : Message Sending failed!");
//			request.setAttribute("attr","Error in sms handler");
//			request.getRequestDispatcher(contextPath + "/send/sms?").forward(request, response);
            //response.sendRedirect(contextPath + "/send/sms");

            // request.getRequestDispatcher("/i18n/sendsms/send-sms.html").forward(request,
            // response);
        }
    }
}
