package org.ipvision.sms.routeHandler;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import org.ipvision.utils.Constants;
import org.ipvision.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/*its use to know the varification code , not varified.*/
@WebServlet(name = "VerifiedCodeHandler", urlPatterns = {"/VerifiedCodeHandler"})
public class VerifiedCodeHandler extends HttpServlet {

    @Autowired
    RoutesHandlerManager routesHandlerManager;

    private String mobileNo = "";
    private String result = "";
    static Logger logger = Logger.getLogger("authCommunicationLogger");

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            //01. ipaddress detection
            logger.debug("request ip " + Utils.detectIP(request));

            //01. mobileNo
            if (request.getParameter("mn") != null && request.getParameter("mn").length() > 3) {
                mobileNo = request.getParameter("mn");
            } else {
                FileItemFactory fileFactory = new DiskFileItemFactory();
                ServletFileUpload fileUpload = new ServletFileUpload(fileFactory);
                List uploadParameters;
                try {
                    uploadParameters = fileUpload.parseRequest(request);

                    Iterator uploadParametersIterator = uploadParameters.iterator();
                    org.apache.commons.fileupload.FileItem uploadParameter;
                    while (uploadParametersIterator.hasNext()) {
                        uploadParameter = (org.apache.commons.fileupload.FileItem) uploadParametersIterator.next();
                        if (uploadParameter.isFormField()) {
                            String name = uploadParameter.getFieldName();
                            String value = uploadParameter.getString("UTF-8");
                            if (name.endsWith(Constants.MOBILE_NUMBER)) {
                                mobileNo = value;
                            }
                        }
                    }
                } catch (FileUploadException e) {
                    logger.error("Exception in VerifiedCodeHandler [fileUpload.parseRequest(request)]", e);
                    out.print(0);
                    return;
                }
            }

            //03. log details/params            
            String date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
            logger.debug("Receiced Data: Mobile Numbner: " + mobileNo + "Receive Date:" + date);
            if (!mobileNo.startsWith("+")) {//String MobileNumber = countryCode + mobileNo;
                mobileNo = "+".trim() + mobileNo.trim();
            }

            //04. update db
            //RoutesHandlerManager routesHandlerManager = new RoutesHandlerManager();
            result = routesHandlerManager.verificationCode(mobileNo);
            if (result.length() < 1) {
                result = "Verification Code not found.";
            }

            logger.debug("SMS Sending Result: " + result);
            out.print(result);
        } catch (Exception e) {
            logger.debug("Exception in VerificationHandler ", e);
            out.print(result);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        System.out.println("Test");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
                config.getServletContext());
    }
}
