package org.ipvision.sms.routes;

import org.apache.log4j.Logger;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.service.IMessageService;
import org.ipvision.service.IRateplanService;
import org.ipvision.service.ISMSRouteService;
import org.ipvision.sms.brands.Cellent;
import org.ipvision.sms.brands.ClickatellApi;
import org.ipvision.sms.brands.GP;
import org.ipvision.sms.brands.InfobipSMS;
import org.ipvision.sms.brands.MobiSMS;
import org.ipvision.sms.brands.Nexmo;
import org.ipvision.sms.brands.RobiAxiataSMS;
import org.ipvision.sms.brands.SMSCountry;
import org.ipvision.sms.brands.SMSRouter;
import org.ipvision.sms.brands.SMSWaalaApi;
import org.ipvision.sms.brands.Wavecell;
import org.ipvision.sms.brands.notUsedAnyMore.ACL;
import org.ipvision.sms.brands.notUsedAnyMore.CSNetworks;
import org.ipvision.utils.SmsServerConstant;

public class RoutesController {

    IRateplanService rateplanService;
    ISMSRouteService smsRouteService;
    IMessageService messageService;

    private int brandID;
    private String mobileNumber;
    private String message;
    private String response;
//    private String result;
    private MessagesDTO messagesDTO;
    private String varificationCode;
    private int countryId;
    private double smsRate;
    private static Logger logger = Logger.getLogger("authCommunicationLogger");

    public RoutesController() {
    }

    public RoutesController(int countryId, String varificationCode, int brandId, String mobileNumber, String message, IRateplanService rateplanService, ISMSRouteService smsRouteService, IMessageService messageService) {
        this.brandID = brandId;
        this.mobileNumber = mobileNumber;
        this.message = message;
        this.varificationCode = varificationCode;
        this.countryId = countryId;
        this.rateplanService = rateplanService;
        this.smsRate = this.rateplanService.getSMSRate(countryId, brandId);
        this.smsRouteService = smsRouteService;
        this.messageService = messageService;
    }

    public MessagesDTO rController() {
        try {
            switch (brandID) {
                case SmsServerConstant.CLICKATELL_API:
                    ClickatellApi clickatell = new ClickatellApi(smsRouteService, messageService);
                    messagesDTO = clickatell.sendSMS(countryId, varificationCode, brandID, mobileNumber, message, smsRate);
                    logger.debug("Response for clickatell: " + messagesDTO.getSmsStatus());
                    break;
                case SmsServerConstant.SMS_ROUTER:
                    SMSRouter sMSRouter = new SMSRouter(smsRouteService, messageService);
                    messagesDTO = sMSRouter.sendSMS(countryId, varificationCode, brandID, mobileNumber, message, smsRate);
                    logger.debug("Response for sMSRouter: " + messagesDTO.getSmsStatus());
                    break;
                case SmsServerConstant.SMS_CELLENT:
                    Cellent sCellent = new Cellent(smsRouteService, messageService);
                    messagesDTO = sCellent.sendSMS(countryId, varificationCode, brandID, mobileNumber, message, smsRate);
                    logger.debug("Response for SMSCellent: " + messagesDTO.getSmsStatus());
                    break;

                case SmsServerConstant.MOBI_SMS:
                    MobiSMS mobileObject = new MobiSMS(smsRouteService, messageService);
                    messagesDTO = mobileObject.sendSMS(countryId, varificationCode, brandID, mobileNumber, message, smsRate);
                    logger.debug("Response for MobiSMS: " + messagesDTO.getSmsStatus());
                    break;
                case SmsServerConstant.CS_NETWORKS:
                    CSNetworks csnNetworks = new CSNetworks(smsRouteService, messageService);
                    messagesDTO = csnNetworks.sendSMS(countryId, varificationCode, brandID, mobileNumber, message, smsRate);
                    logger.debug("Response for CSNetworks: " + messagesDTO.getSmsStatus());
                    break;

                case SmsServerConstant.INFOBIP_SMS:
                    InfobipSMS infobipSMS = new InfobipSMS(smsRouteService, messageService);
                    messagesDTO = infobipSMS.sendSMS(countryId, varificationCode, brandID, mobileNumber, message, smsRate);
                    logger.debug("Response for InfobipSMS: " + messagesDTO.getSmsStatus());
                    break;
                case SmsServerConstant.ROBIAXIATA_SMS:
                    RobiAxiataSMS robiAxiataSMS = new RobiAxiataSMS(smsRouteService, messageService);
                    messagesDTO = robiAxiataSMS.sendSMS(countryId, varificationCode, brandID, mobileNumber, message, smsRate);
                    logger.debug("Response for RobiAxiataSMS: " + messagesDTO.getSmsStatus());
                    break;
                case SmsServerConstant.WAVECELL:
                    Wavecell wavecell = new Wavecell(smsRouteService, messageService);
                    messagesDTO = wavecell.sendSMS(countryId, varificationCode, brandID, mobileNumber, message, smsRate);
                    logger.debug("Response for Wavecell: " + messagesDTO.getSmsStatus());
                    break;
                case SmsServerConstant.SMS_WAALA_API:
                    SMSWaalaApi smswaala = new SMSWaalaApi(smsRouteService, messageService);
                    messagesDTO = smswaala.sendSMS(countryId, varificationCode, brandID, mobileNumber, message, smsRate);
                    break;
                case SmsServerConstant.ACL:
                    ACL acl = new ACL(smsRouteService, messageService);
                    messagesDTO = acl.sendSMS(countryId, varificationCode, brandID, mobileNumber, message, smsRate);
                    break;
                case SmsServerConstant.SMS_COUNTRY:
                    SMSCountry smsCountry = new SMSCountry(smsRouteService, messageService);
                    messagesDTO = smsCountry.sendSMS(countryId, varificationCode, brandID, mobileNumber, message, smsRate);
                    break;
                case SmsServerConstant.NEXMO:
                    Nexmo nexmo = new Nexmo(smsRouteService, messageService);
                    messagesDTO = nexmo.sendSMS(countryId, varificationCode, brandID, mobileNumber, message, smsRate);
                    logger.debug("Response for Nexmo: " + messagesDTO.getSmsStatus());
                    break;
                case SmsServerConstant.GP:
                    GP gp = new GP(smsRouteService, messageService);
                    messagesDTO = gp.sendSMS(countryId, varificationCode, brandID, mobileNumber, message, smsRate);
                    logger.debug("Response for GP: " + messagesDTO.getSmsStatus());
                    break;

// This operator is no longer in use.                
//                case SmsServerConstant.TEXT_LOCAL:
//                    TextLocal textLocal = new TextLocal();
//                    messagesDTO =  textLocal.sendSMS(countryId, varificationCode, brandID, mobileNumber, message,smsRate);
//                    break;
//                    if (response.equals("failed")) {
//                        logger.debug("Response for textLocal:  " + response);
//                        break;
//                    } else {
//                        JSONObject jsonObject = new JSONObject(response);
//                        messagesDTO =  (String) jsonObject.getString("status");
//                        logger.debug("Response for textLocal:  " + response);
//                        break;
//                    }
// This operator is no longer in use.                    
//                case SmsServerConstant.CLICKN_GO_BD:
//                    ClicknGoBD clicknGoBD = new ClicknGoBD();
//                    messagesDTO =  clicknGoBD.sendSMS(countryId, varificationCode, brandID, mobileNumber, message,smsRate);
//                    logger.debug("Response for ClicknGoBD: " + messagesDTO.getSmsStatus());
//                    break;
// This operator is no longer in use.                    
//                case SmsServerConstant.PLIVO_API:
//                    PlivoApi plivo = new PlivoApi();
//                    messagesDTO =  plivo.sendSMS(countryId, varificationCode, brandID, mobileNumber, message, smsRate);
//                    break;
            }
        } catch (Exception e) {
            logger.debug("Exception in RoutesController");
            messagesDTO.setSmsStatus("failed");
            return messagesDTO;
        }
        return messagesDTO;
    }

    public void setBrandID(int brandID) {
        this.brandID = brandID;
    }

    public String getMsgId() {
        String msgId = "";
        if (messagesDTO != null) {
            msgId = messagesDTO.getResponseId();
        }
        return msgId;
    }

    public double getSMSRate() {
        return smsRate;
    }
//    public static void main(String[] args) {
//        RoutesController rc = new RoutesController();
//        rc.rController();
//    }

    public int getCountryId() {
        return countryId;
    }

}
