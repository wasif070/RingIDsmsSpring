package org.ipvision.sms.routes;

import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.ipvision.domain.SMSRoute;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.service.IMessageService;
import org.ipvision.service.ISMSRouteService;
import org.ipvision.sms.brands.ClickatellApi;
import org.ipvision.sms.brands.InfobipSMS;
import org.ipvision.sms.brands.RobiAxiataSMS;
import org.ipvision.sms.brands.SMSWaalaApi;
import org.ipvision.sms.brands.Wavecell;
import org.ipvision.utils.MyAppError;
import org.ipvision.utils.SmsServerConstant;

/**
 *
 * @author reefat
 */
public class SMSDeliveryChecker extends Thread {

    IMessageService messageService;

    ISMSRouteService smsRouteService;

    ArrayList<Integer> brandIdMap;
    String msgId;
    private static Logger logger = Logger.getLogger("authCommunicationLogger");
    RoutesController routesController;
    Integer brandId;

    public SMSDeliveryChecker(Integer brandId, ArrayList<Integer> brandIdMap, RoutesController routesController, IMessageService messageService, ISMSRouteService smsRouteService) {
        this.brandIdMap = brandIdMap;
        this.msgId = routesController.getMsgId();
        this.routesController = routesController;
        this.brandId = brandId;
        this.messageService = messageService;
        this.smsRouteService = smsRouteService;
    }

    @Override
    public void run() {
        if (msgId.length() > 0) {
            boolean delivered;
            logger.info("msgId --> " + msgId);
            try {
                MessagesDTO mDTO = messageService.getMessage(brandId, msgId);
                if (mDTO == null) {
                    Thread.sleep(5000);
                    mDTO = messageService.getMessage(brandId, msgId);
                }
                delivered = getDeliveryStatus(mDTO);
                if (!delivered) {
                    for (int brandIdNew : brandIdMap) {
                        routesController.setBrandID(brandIdNew);
                        MessagesDTO messagesDTO = routesController.rController();
//                        delivered = getDeliveryStatus(MessageScheduler.getInstance().getMessageByMsgId(brandIdNew, messagesDTO.getResponseId()));
                        delivered = getDeliveryStatus(messagesDTO);
                        if (delivered) {
                            break;
                        }
                    }
                    if (!delivered) {
                        logger.error("Message sending failed! All attempts failed!");
                    }
                }
            } catch (InterruptedException ex) {
            }
        } else {
            logger.error("MessageId empty");
        }
    }

    private boolean getDeliveryStatus(MessagesDTO msgDTO) throws InterruptedException {
        if (msgDTO == null) {
            logger.error("msgDTO is null!");
            return false;
        }
        int maxTryCount = 5;
        boolean deliveryResultReceived = false;
        loop:
        while (maxTryCount > 0 && !deliveryResultReceived) {
            Thread.sleep(6000);//6 sec [total 6 * 5 = 30 sec wait]
            int brandIdOfMsg = msgDTO.getRoutesId();
            switch (brandIdOfMsg) {
                case SmsServerConstant.SMS_WAALA_API:
                    SMSWaalaApi sMSWaalaApi = new SMSWaalaApi(msgDTO.getDestination(), messageService);
                    SmsStatusDTO smsStatusDTO = sMSWaalaApi.getDeliveryStatus(brandIdOfMsg, msgDTO.getResponseId());
                    if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.SUCCESS) {
                        msgDTO.setDlStatus(1);
                        deliveryResultReceived = true;
                        //db update
                        updateDB(msgDTO);
                        //balance update                        
                        updateBalance(brandIdOfMsg, routesController.getSMSRate());
                        logger.error("API returned delivery success of brandID : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
                    } else if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.ERROR) {
                        msgDTO.setDlStatus(-1);
                        //db update
                        updateDB(msgDTO);
                        logger.error("Message sending failed using brandID : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
                        break loop;
                    }
                    break;
                case SmsServerConstant.CLICKATELL_API:
                    ClickatellApi clickatell = new ClickatellApi(smsRouteService, messageService);
                    smsStatusDTO = clickatell.getDeliveryStatus(brandIdOfMsg, msgDTO.getResponseId());
                    if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.SUCCESS) {
                        msgDTO.setDlStatus(1);
                        deliveryResultReceived = true;
                        //db update
                        updateDB(msgDTO);
                        //balance update                        
                        updateBalance(brandIdOfMsg, routesController.getSMSRate());
                        logger.error("API returned delivery success of brandID : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
                    } else if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.ERROR) {
                        msgDTO.setDlStatus(-1);
                        //db update
                        updateDB(msgDTO);
                        logger.error("Message sending failed using brandID : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
                        break loop;
                    }
                    break;
                case SmsServerConstant.INFOBIP_SMS:
                    InfobipSMS infobipSMS = new InfobipSMS(smsRouteService, messageService);
                    smsStatusDTO = infobipSMS.getDeliveryStatus(brandIdOfMsg, msgDTO.getResponseId());
                    if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.SUCCESS) {
                        msgDTO.setDlStatus(1);
                        deliveryResultReceived = true;
                        //db update
                        updateDB(msgDTO);
                        //balance update                        
                        updateBalance(brandIdOfMsg, routesController.getSMSRate());
                        logger.error("API returned delivery success of brandID : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
                    } else if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.ERROR) {
                        msgDTO.setDlStatus(-1);
                        //db update
                        updateDB(msgDTO);
                        logger.error("Message sending failed using brandID : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
                        break loop;
                    }
                    break;
                case SmsServerConstant.ROBIAXIATA_SMS:
                    RobiAxiataSMS robiAxiataSMS = new RobiAxiataSMS(smsRouteService, messageService);
                    smsStatusDTO = robiAxiataSMS.getDeliveryStatus(brandIdOfMsg, msgDTO.getResponseId());
                    if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.SUCCESS) {
                        msgDTO.setDlStatus(1);
                        deliveryResultReceived = true;
                        //db update
                        updateDB(msgDTO);
                        //balance update                        
                        updateBalance(brandIdOfMsg, routesController.getSMSRate());
                        logger.error("API returned delivery success of brandID : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
                    } else if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.ERROR) {
                        msgDTO.setDlStatus(-1);
                        //db update
                        updateDB(msgDTO);
                        logger.error("Message sending failed using brandID : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
                        break loop;
                    }
                    break;
                case SmsServerConstant.WAVECELL:
                    Wavecell wavecell = new Wavecell(smsRouteService, messageService);
                    smsStatusDTO = wavecell.getDeliveryStatus(brandIdOfMsg, msgDTO.getResponseId());
                    if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.SUCCESS) {
                        msgDTO.setDlStatus(1);
                        deliveryResultReceived = true;
                        //db update
                        updateDB(msgDTO);
                        //balance update                        
                        updateBalance(brandIdOfMsg, routesController.getSMSRate());
                        logger.error("API returned delivery success of brandID : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
                    } else if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.ERROR) {
                        msgDTO.setDlStatus(-1);
                        //db update
                        updateDB(msgDTO);
                        logger.error("Message sending failed using brandID : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
                        break loop;
                    }
                    break;

                case SmsServerConstant.SMS_ROUTER:
                case SmsServerConstant.SMS_CELLENT:
                case SmsServerConstant.MOBI_SMS:
                case SmsServerConstant.SMS_COUNTRY:
                    smsStatusDTO = getDeliveryStatus(brandIdOfMsg, msgDTO.getResponseId());
                    if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.SUCCESS) {
                        deliveryResultReceived = true;
                        //balance update                        
                        updateBalance(brandIdOfMsg, routesController.getSMSRate());
                        logger.error("CallBack returned delivery success of brandID : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
                    } else if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.ERROR) {
                        logger.error("Message sending failed using brandID : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
                        break loop;
                    }
//                    logger.error("API is not supported for brandId --> " + brandId + ". Please use callBack!");
                    break;
                case SmsServerConstant.CS_NETWORKS:
                case SmsServerConstant.ACL:
                case SmsServerConstant.CLICKN_GO_BD:
                case SmsServerConstant.PLIVO_API:
                case SmsServerConstant.TEXT_LOCAL:
                    deliveryResultReceived = true;
                    logger.error("This operator [brandId] --> " + brandIdOfMsg + " shouldn't be used any more! Please contact admin.");
                    break loop;
            }
            maxTryCount--;
        }

        if (!deliveryResultReceived) {
            logger.error("deliveryResultReceived : false! Message sending failed for  brand : " + msgDTO.getRoutesId());
            msgDTO.setDlStatus(-1);
            //db update
            updateDB(msgDTO);
        }

        return deliveryResultReceived;
    }

    private void updateDB(MessagesDTO msDTO) {
        try {
            MyAppError error=messageService.updateDeliverySMSStatus(msDTO);
            if (error.getERROR_TYPE() > 0) {
                logger.error("Update Delivery SMSStatus Failed.");
            }
        } catch (Exception e) {
            logger.error("Error occured while updating DB --> " + e);
        }
    }

    private void updateBalance(int brandId, double rate) {
        try {
            SMSRoute brandDTO = smsRouteService.findById(new Long(brandId));
            Double balance = brandDTO.getBalance() - rate;

            brandDTO.setBalance(balance);
            brandDTO.setId(new Long(brandId));
            MyAppError error = smsRouteService.updateBrandBalance(brandDTO);
            if (error.getERROR_TYPE() > 0) {
                logger.error("balance Update Failed.");
            }
        } catch (Exception e) {
            logger.error("Error while updating Balance : " + e);
        }

    }

    private SmsStatusDTO getDeliveryStatus(int brandId, String responseId) {
        logger.debug("brandId --> " + brandId + " responseId --> " + responseId);
        MessagesDTO messagesDTO = messageService.getMessage(brandId, responseId);
        if (messagesDTO == null) {
            try {
                Thread.sleep(5000);
                messagesDTO = messageService.getMessage(brandId, msgId);
            } catch (InterruptedException ex) {
            }
        }
        SmsStatusDTO smsStatusDTO = new SmsStatusDTO();
        if (messagesDTO == null) {
            logger.error("getDeliveryStatus in SMSDeliveryChecker --> messagesDTO is null  for : brandId --> " + brandId + " & responseId --> " + responseId);
            smsStatusDTO.setStatus(SmsStatusDTO.STATUS.ERROR);
        } else if (messagesDTO.getDlStatus() > 0) {
            smsStatusDTO.setStatus(SmsStatusDTO.STATUS.SUCCESS);
        } else if (messagesDTO.getDlStatus() < 0) {
            smsStatusDTO.setStatus(SmsStatusDTO.STATUS.ERROR);
        }
        return smsStatusDTO;
    }
}
