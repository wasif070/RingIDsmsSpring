package org.ipvision.sms.routes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.log4j.Logger;
import org.ipvision.domain.SMSRoute;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.service.IMessageService;
import org.ipvision.utils.Constants;
import org.ipvision.utils.MyAppError;

public abstract class SMSSendManager {

    IMessageService messageService;

    private static Logger logger = Logger.getLogger("authCommunicationLogger");

    protected SMSRoute brandDTO;

    public abstract MessagesDTO sendSMS(int countryId, String vCode, int brandId, String mobileNumber, String msg, Double rate);

    public abstract String prepareData(SMSRoute brandDTO, String msg, String mobileNumber);

    public abstract String prepareDeliveryStatusData(SMSRoute brandDTO, String messageId);

    public abstract SmsStatusDTO parseResponse(String response);

    public SMSSendManager(IMessageService messageService) {
        this.messageService = messageService;
    }

//    protected abstract String sendData(String data) throws MalformedURLException, IOException;
    protected final String sendData(String data, SMSRoute brandDTO) throws MalformedURLException, IOException {

        HttpURLConnection conn = (HttpURLConnection) new URL(brandDTO.getApiUrl()).openConnection();
//            String data = user + pass + msisdn + message + sender + messageType;
        conn.setDoOutput(true);
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
        conn.getOutputStream().write(data.getBytes("UTF-8"));
        final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        final StringBuffer stringBuffer = new StringBuffer();
        String line;
        while ((line = rd.readLine()) != null) {
            stringBuffer.append(line);
        }
        rd.close();

        return stringBuffer.toString();

    }

    protected String addToDB(MessagesDTO messagesDTO) {
        String response = Constants.FAILED;
        try {
            MyAppError error = messageService.addMessage(messagesDTO);
            if (error.getERROR_TYPE() > 0) {
                logger.error("DB insert failed --> " + error.getErrorMessage() + "\n" + messagesDTO.toString());
                response = Constants.FAILED;
            } else {
                response = Constants.SUCCESS;
            }
        } catch (Exception e) {

        }
        return response;
    }
}
