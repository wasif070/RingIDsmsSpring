package org.ipvision.sms.routes;

public class SmsStatusDTO {

    public static enum STATUS {

        SUCCESS,
        ERROR,
        PENDING
    }
    private STATUS status = STATUS.PENDING;
    private String responseCode;
    private String responseText;
    private String responseDetails;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public String getResponseDetails() {
        return responseDetails;
    }

    public void setResponseDetails(String responseDetails) {
        this.responseDetails = responseDetails;
    }

    public STATUS getStatus() {
        return status;
    }

    public void setStatus(STATUS status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "SmsStatusDTO{" + "status=" + status + ", responseCode=" + responseCode + ", responseText=" + responseText + ", responseDetails=" + responseDetails + '}';
    }
}
