/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.login;

import org.springframework.security.authentication.BadCredentialsException;

/**
 *
 * @author reefat
 */
public class InvalidLoginException extends BadCredentialsException {

	String errorMessage = "Login Error : ";

	public InvalidLoginException(String errorMessage) {
		super(errorMessage);
		this.errorMessage += errorMessage;
	}

	@Override
	public String getMessage() {
		return errorMessage;
	}

	@Override
	public String toString() {
		return "InvalidLoginException{" + "errorMessage=" + errorMessage + '}';
	}

}
