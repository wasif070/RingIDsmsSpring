package org.ipvision.login;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {

	ROLE_SUPER_ADMIN("SUPER_ADMIN"), ROLE_ADMIN("ADMIN"), ROLE_GUEST("GUEST");

	private String name;

	private Role(String name) {
		this.name = name;
	}

	@Override
	public String getAuthority() {
		// TODO Auto-generated method stub
		return name;
	}

}
