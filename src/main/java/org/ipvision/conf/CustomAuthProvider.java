package org.ipvision.conf;

import org.ipvision.domain.User;
import org.ipvision.login.InvalidLoginException;
import org.ipvision.service.CustomUserDetailsService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthProvider implements AuthenticationProvider {

    static Logger log = Logger.getLogger("vodCustomLogger");

    @Autowired
    CustomUserDetailsService customUserDetailsService;

    /**
     * Responsible for user authentication checking in our system All so check
     * the authorization in Third party RingID server.
     *
     * @param auth
     * @return
     */
    @Override
    public Authentication authenticate(Authentication auth) {

        log.info("User login checking in custom auth provider");

        String username = auth.getName();
        String password = auth.getCredentials().toString().trim();

        if (username == null || username.isEmpty() || password == null || password.isEmpty()) {
            log.error("Username or password can't be empty");
            throw new InvalidLoginException("Username or password can't be empty");
        }

        User user = null;

        user = (User) customUserDetailsService.loadUserByUsername(username);

        if (user == null) {
            throw new InvalidLoginException("Invalid Credentials!");
        }

        if (!user.getPassword().equals(password)) {
            throw new InvalidLoginException("Invalid username or password!");
        }

        System.out.println(user);

        System.out.println("USER ROLES: " + user.getAuthorities());
        return new UsernamePasswordAuthenticationToken(user, user.getPassword(), user.getAuthorities());

    }

    @Override
    public boolean supports(Class<?> authentication) {
        // TODO Auto-generated method stub
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}
