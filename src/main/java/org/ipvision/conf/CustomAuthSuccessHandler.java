package org.ipvision.conf;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthSuccessHandler implements AuthenticationSuccessHandler {

	static Logger log = Logger.getLogger("vodCustomLogger");
	/**
	 * When a user is logged in this system this method will define which
	 * controller user will be redirect
     * @param request
     * @param response
     * @param auth
     * @throws java.io.IOException
     * @throws javax.servlet.ServletException
	 */

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth)
			throws IOException, ServletException {
		log.info("redirect to home after successfully login");
		//response.sendRedirect("employee/info");
		response.sendRedirect("home");
	}

}
