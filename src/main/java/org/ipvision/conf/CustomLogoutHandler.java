package org.ipvision.conf;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.ipvision.domain.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

@Component
public class CustomLogoutHandler implements LogoutHandler {

	static Logger log = Logger.getLogger("vodCustomLogger");

	/**
	 * When user is logout from this system, this method logout the user from
	 * third party RingID server also
     * @param request
     * @param reponse
     * @param auth
	 */

	@Override
	public void logout(HttpServletRequest request, HttpServletResponse reponse, Authentication auth) {
		// TODO Auto-generated method stub
		log.info("Logout success");
		if (auth != null) {
			log.debug("User Logging out : ");
			User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			log.debug("Logout Success : " + user);
		}
		// Remove the user from RindID Server
		// MyAppError error =
		// RingTaskScheduler.getInstance().logout(user.getUsername());
		// log.info("User logout out from RingTask Scheduler " +
		// error.getErrorMessage());
	}

}
