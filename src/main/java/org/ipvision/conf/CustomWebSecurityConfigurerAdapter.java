package org.ipvision.conf;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

@EnableWebSecurity
@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class CustomWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

    static Logger log = Logger.getLogger("vodCustomLogger");

    @Autowired
    AuthenticationProvider authenticationProvider;

    @Autowired
    UserDetailsService customerUserDetailsService;

    @Autowired
    CustomLogoutHandler logoutHandler;

    @Autowired
    CustomAuthSuccessHandler successHandler;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider);
        log.info("Custom Auth Provider Added");
    }

    /**
     * *
     * URL Basis permission
     *
     * @param httpSecurity
     * @throws java.lang.Exception
     */
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        log.info("Web URL security adding");
        httpSecurity.csrf().disable().headers().disable().authorizeRequests()
                .antMatchers("/upload/**", "/home", "/ring/**", "/employee/**", "/smsroute/**", "/account/**",
                        "/country/**", "/rateplan/**", "/send/**", "/message/**", "/report/**", "/send/**")
                .authenticated().antMatchers("/smsroute/**", "/rateplan/**")
                .hasAnyAuthority("ROLE_ADMIN", "ROLE_SUPER_ADMIN").antMatchers("/login", "/", "/index").permitAll()
                .and().formLogin().loginPage("/login").usernameParameter("userName").passwordParameter("password")
                .failureUrl("/login-error").successHandler(successHandler).and().logout()
                .addLogoutHandler(logoutHandler).and().httpBasic();

        httpSecurity.exceptionHandling().accessDeniedPage("/permission-denied");

        log.info("Web URL security added");
    }

}
