/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.apache.log4j.Logger;
import org.ipvision.domain.Country;
import org.ipvision.utils.MyAppError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ipvision
 */
@Repository
public class CountryJdbcRepo {

    static Logger logger = Logger.getLogger(CountryJdbcRepo.class.getName());

    @Autowired
    private NamedParameterJdbcTemplate template;

    public List<Country> findCountriesBySearchKey(String key) {
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();

        namedParameters.addValue("key", "%" + key + "%");

        List<Country> countries;
        countries = this.template.query("SELECT * FROM Country WHERE LOWER(countryName) like :key or LOWER(countryCode) like :key or LOWER(countrySymbol) like :key",
                namedParameters, (ResultSet rs, int rowNum) -> {

                    Country country = new Country();

                    country.setId(rs.getLong("id"));
                    country.setCountryCode(rs.getString("countryName"));
                    country.setCountryCode(rs.getString("countryCode"));
                    country.setCountrySymbol(rs.getString("countrySymbol"));

                    return country;
                });

        return countries;
    }

    public int deleteById(Long id) {

        String query = "DELETE FROM Country WHERE id =:id";

        SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);

        return this.template.update(query, namedParameters);
    }

    public MyAppError save(Country country) {

        String sql;
        Integer nRow;
        MyAppError myAppError = new MyAppError();
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();

        try {
            namedParameters.addValue("id", country.getId());
            namedParameters.addValue("countryCode", country.getCountryCode());
            namedParameters.addValue("countryName", country.getCountryName());
            namedParameters.addValue("countrySymbol", country.getCountrySymbol());

            if (country.getId() == null || country.getId() == 0) {

                sql = "SELECT COUNT(c) FROM Country c WHERE c.countrySymbol = :countrySymbol or c.countryName = :countryName or c.countryCode = :countryCode";
                nRow = this.template.queryForObject(sql, namedParameters, Integer.class);
                if (nRow > 0) {
                    myAppError.setERROR_TYPE(MyAppError.OTHERERROR);
                    myAppError.setErrorMessage("Country Name, Code or Symbol exists");
                    return myAppError;
                }

                sql = "INSERT INTO Country VALUES ( :id, :countryCode, :countryName, :countrySymbol )";
                nRow = this.template.update(sql, namedParameters);

                if (nRow > 0) {
                    myAppError.setERROR_TYPE(MyAppError.NOERROR);
                    myAppError.setErrorMessage("Country " + country.getCountryName() + " Saved Successfully");
                    return myAppError;
                }
            } else {

                sql = "SELECT COUNT(c) FROM Country c WHERE (c.countrySymbol = :countrySymbol or c.countryName = :countryName or c.countryCode = :countryCode) and c.id != :id";
                nRow = this.template.queryForObject(sql, namedParameters, Integer.class);
                if (nRow > 0) {
                    myAppError.setERROR_TYPE(MyAppError.OTHERERROR);
                    myAppError.setErrorMessage("Country Name, Code or Symbol exists");
                    return myAppError;
                }

                sql = "UPDATE Country SET  countryCode = :countryCode, countryName = :countryName, countrySymbol = :countrySymbol WHERE id = :id";
                nRow = this.template.update(sql, namedParameters);

                if (nRow > 0) {
                    myAppError.setERROR_TYPE(MyAppError.NOERROR);
                    myAppError.setErrorMessage("Country " + country.getCountryName() + " Updated Successfully");
                    return myAppError;
                }

            }
        } catch (DataAccessException e) {
            myAppError.setERROR_TYPE(myAppError.DBERROR);
            myAppError.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("addCountriesInfo[1] --> " + e);
        }
        return myAppError;
    }
}
