package org.ipvision.repository;

import java.sql.ResultSet;
import java.text.ParseException;

import org.ipvision.domain.Country;
import org.ipvision.domain.Message;
import org.ipvision.domain.SMSRoute;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.dto.SearchDTO;
import org.ipvision.dto.SummaryDTO;
import org.ipvision.utils.Constants;
import org.ipvision.utils.MyAppError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.dao.DataAccessException;

@Repository
public class MessageRepository {

    Logger logger = Logger.getLogger("vodCustomLogger");

    @Autowired
    private NamedParameterJdbcTemplate template;

    @Autowired
    private SMSRouteRepository smsRouteRepo;

    @Autowired
    private CountryRepository countryRep;

    public Message findOne(Long id) {
        // TODO Auto-generated method stub
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("id", id);

        Message message = (Message) this.template.queryForObject("select * from messages where id = :id",
                namedParameters, (ResultSet rs, int rowNum) -> {
                    Message objMessage = new Message();
                    objMessage.setId(rs.getInt("id"));
                    objMessage.setRoutesId(rs.getInt("brandId"));
                    objMessage.setMobileNo(rs.getString("mobileNo"));
                    objMessage.setMessage(rs.getString("message"));
                    objMessage.setResponseId(rs.getString("responseId"));
                    objMessage.setBrandResponse(rs.getString("brandResponse"));
                    objMessage.setSmsStatus(rs.getString("smsStatus"));
                    objMessage.setDate(rs.getDate("sendDate").toString());
                    objMessage.setvCode(rs.getString("vCode"));
                    objMessage.setColumn(rs.getInt("countryId"));
                    objMessage.setReceivedDate(rs.getDate("responseDate").toString());
                    objMessage.setDlStatus(rs.getInt("delivaryStatus"));
                    return objMessage;
                });

        return message;
    }

    private String getClause(String optionalSql) {
        return (optionalSql.length() < 1 ? " WHERE " : " AND ");
    }

    private String prepareDateLimitation(String optionalSql, SearchDTO searchDTO) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        Date maxToDate = cal.getTime();
        String maxToDateStr = sdf.format(maxToDate);
        cal.add(Calendar.DATE, -60);
        Date minFromDate = cal.getTime();
        String minFromDateStr = sdf.format(minFromDate);

        String fromDate, toDate;

        if (!searchDTO.isSerachByFromDate() || sdf.parse(searchDTO.getFromDate()).before(minFromDate)) {
            fromDate = minFromDateStr;
        } else {
            fromDate = searchDTO.getFromDate();
        }
        if (!searchDTO.isSerachByToDate()) {
            toDate = maxToDateStr;
        } else {
            toDate = searchDTO.getToDate();
        }

        // if (!searchDTO.isSerachByFromDate() && !searchDTO.isSerachByToDate())
        // {
        // optionalSql += getClause(optionalSql) + " DATE(sendDate) BETWEEN '" +
        // minFromDateStr + "' AND '" + maxToDateStr + "'";
        // } else if (searchDTO.isSerachByFromDate() &&
        // searchDTO.isSerachByToDate()) {
        // optionalSql += getClause(optionalSql) + " DATE(sendDate) BETWEEN '" +
        // fromDate + "' AND '" + toDate + "'";
        // } else if (searchDTO.isSerachByFromDate()) {
        // optionalSql += getClause(optionalSql) + " DATE(sendDate) >= '" +
        // fromDate + "' ";
        // } else if (searchDTO.isSerachByToDate()) {
        // optionalSql += getClause(optionalSql) + " DATE(sendDate) <= '" +
        // toDate + "'";
        // }
        minFromDateStr = minFromDateStr + " 00:00:00";
        maxToDateStr = maxToDateStr + " 23:59:59";
        fromDate = fromDate + " 00:00:00";
        toDate = toDate + " 23:59:59";

        if (!searchDTO.isSerachByFromDate() && !searchDTO.isSerachByToDate()) {
            optionalSql += getClause(optionalSql) + " sendDate BETWEEN '" + minFromDateStr + "' AND '" + maxToDateStr
                    + "'";
        } else if (searchDTO.isSerachByFromDate() && searchDTO.isSerachByToDate()) {
            optionalSql += getClause(optionalSql) + " sendDate BETWEEN '" + fromDate + "' AND '" + toDate + "'";
        } else if (searchDTO.isSerachByFromDate()) {
            optionalSql += getClause(optionalSql) + " sendDate >= '" + fromDate + "' ";
        } else if (searchDTO.isSerachByToDate()) {
            optionalSql += getClause(optionalSql) + " sendDate <= '" + toDate + "'";
        }
        return optionalSql;
    }

    private String prepareDateLimitation(SearchDTO searchDTO) throws ParseException {
        String dateSpecificOptionalSql = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        Date maxToDate = cal.getTime();
        String maxToDateStr = sdf.format(maxToDate);
        cal.add(Calendar.DATE, -60);
        Date minFromDate = cal.getTime();
        String minFromDateStr = sdf.format(minFromDate);

        String fromDate, toDate;

        if (!searchDTO.isSerachByFromDate() || sdf.parse(searchDTO.getFromDate()).before(minFromDate)) {
            fromDate = minFromDateStr;
        } else {
            fromDate = searchDTO.getFromDate();
        }
        if (!searchDTO.isSerachByToDate()) {
            toDate = maxToDateStr;
        } else {
            toDate = searchDTO.getToDate();
        }

        // if (!searchDTO.isSerachByFromDate() && !searchDTO.isSerachByToDate())
        // {
        // dateSpecificOptionalSql = " and DATE(sendDate) BETWEEN '" +
        // minFromDateStr + "' AND '" + maxToDateStr + "'";
        // } else if (searchDTO.isSerachByFromDate() &&
        // searchDTO.isSerachByToDate()) {
        // dateSpecificOptionalSql = " and DATE(sendDate) BETWEEN '" + fromDate
        // + "' AND '" + toDate + "'";
        // } else if (searchDTO.isSerachByFromDate()) {
        // dateSpecificOptionalSql = " and DATE(sendDate) >= '" + fromDate + "'
        // ";
        // } else if (searchDTO.isSerachByToDate()) {
        // dateSpecificOptionalSql = " and DATE(sendDate) <= '" + toDate + "'";
        // }
        minFromDateStr = minFromDateStr + " 00:00:00";
        maxToDateStr = maxToDateStr + " 23:59:59";
        fromDate = fromDate + " 00:00:00";
        toDate = toDate + " 23:59:59";
        if (!searchDTO.isSerachByFromDate() && !searchDTO.isSerachByToDate()) {
            dateSpecificOptionalSql = " and sendDate BETWEEN '" + minFromDateStr + "' AND '" + maxToDateStr + "'";
        } else if (searchDTO.isSerachByFromDate() && searchDTO.isSerachByToDate()) {
            dateSpecificOptionalSql = " and sendDate BETWEEN '" + fromDate + "' AND '" + toDate + "'";
        } else if (searchDTO.isSerachByFromDate()) {
            dateSpecificOptionalSql = " and sendDate >= '" + fromDate + "' ";
        } else if (searchDTO.isSerachByToDate()) {
            dateSpecificOptionalSql = " and sendDate <= '" + toDate + "'";
        }

        return dateSpecificOptionalSql;
    }

    public Integer getMessagesCount(SearchDTO searchDTO) {
        Integer total = 0;
        // try {
        String sql;
        String firstPartOfSql = "SELECT count(1) as total  FROM messages ";
        String optionalSql = "";

        if (searchDTO.isSearchByRoutes()) {
            optionalSql += getClause(optionalSql) + " brandId = " + searchDTO.getRoutesId();
        }

        if (searchDTO.isSearchByStatus()) {
            optionalSql += getClause(optionalSql) + " smsStatus = " + searchDTO.getSmsStatus();
        }

        if (searchDTO.isSearchByCountryId()) {
            optionalSql += getClause(optionalSql) + " countryId = " + searchDTO.getCountryId();
        }

        if (searchDTO.isFailed()) {
            optionalSql += getClause(optionalSql) + " delivaryStatus < 1 AND verifiedSend < 1 ";
        }

        try {
            optionalSql = prepareDateLimitation(optionalSql, searchDTO);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (searchDTO.isSuccess()) {
            firstPartOfSql = "SELECT id  FROM messages ";
            // sql = "SELECT count(1) as total FROM ((" + firstPartOfSql +
            // optionalSql + getClause(optionalSql) + " delivaryStatus = 1 )
            // UNION "
            // + "(" + firstPartOfSql + optionalSql + getClause(optionalSql) + "
            // verifiedSend = 1 )) t ";

            switch (searchDTO.getType()) {
                case Constants.VERIFIED:
                    sql = "SELECT count(1) as total FROM messages " + optionalSql + getClause(optionalSql)
                            + " verifiedSend = 1";
                    break;
                case Constants.DELIVERED:
                    sql = "SELECT count(1) as total FROM messages " + optionalSql + getClause(optionalSql)
                            + " delivaryStatus = 1";
                    break;
                case Constants.BOTH:
                    sql = "SELECT count(1) as total FROM ((" + firstPartOfSql + optionalSql + getClause(optionalSql)
                            + " delivaryStatus = 1 ) UNION " + "(" + firstPartOfSql + optionalSql + getClause(optionalSql)
                            + " verifiedSend = 1 )) t ";
                    break;
                default:
                    sql = "SELECT count(1) as total FROM ((" + firstPartOfSql + optionalSql + getClause(optionalSql)
                            + " delivaryStatus = 1 ) UNION " + "(" + firstPartOfSql + optionalSql + getClause(optionalSql)
                            + " verifiedSend = 1 )) t ";
                    break;
            }
        } else {
            sql = firstPartOfSql + optionalSql;
        }

        logger.debug("getMessagesCount : SQL [SUCS: " + searchDTO.isSuccess() + " --> FAILED: " + searchDTO.isFailed()
                + "] --> " + sql);

        SqlParameterSource namedParameters = new MapSqlParameterSource();
        total = this.template.queryForObject(sql, namedParameters, Integer.class);

        // db = databaseconnector.DBConnector.getInstance().makeConnection();
        // stmt = db.connection.createStatement();
        // rs = stmt.executeQuery(sql);
        // if (searchDTO.isSuccess()) {
        // while (rs.next()) {
        // total += rs.getInt("total");
        // }
        // } else {
        // if (rs.next()) {
        // total = rs.getInt("total");
        // }
        // }
        // } catch (Exception e) {
        // logger.error("execption in MessageDAO of getMessageList " + e);
        // return null;
        // } finally {
        // close();
        // }
        return total;
    }

    public List<Message> findAll(PageRequest pageRequest) {
        // TODO Auto-generated method stub
        return null;
    }

    public Long count() {
        // TODO Auto-generated method stub
        return null;
    }

    public ArrayList<MessagesDTO> getMessagesList(long start, int limit, SearchDTO searchDTO) {
        HashMap<Integer, MessagesDTO> messageListByDateMap = new HashMap<>();

        String sql;
        String firstPartOfSql = "SELECT id,brandId,mobileNo,message,responseId,brandResponse,smsStatus,sendDate,verifiedSend,delivaryStatus,countryId  FROM messages ";
        String optionalSql = "";
        String lastPartOfSql = " order by id desc limit " + start + "," + limit;

        if (searchDTO.isSearchByRoutes()) {
            optionalSql += getClause(optionalSql) + " brandId = " + searchDTO.getRoutesId();
        }

        if (searchDTO.isSearchByStatus()) {
            optionalSql += getClause(optionalSql) + " smsStatus = " + searchDTO.getSmsStatus();
        }

        if (searchDTO.isSearchByCountryId()) {
            optionalSql += getClause(optionalSql) + " countryId = " + searchDTO.getCountryId();
        }

        /*
		 * if (searchDTO.isSuccess()) { optionalSql += getClause(optionalSql) +
		 * " delivaryStatus = 1 OR verifiedSend = 1 "; } else
         */
        if (searchDTO.isFailed()) {
            optionalSql += getClause(optionalSql) + " delivaryStatus < 1 AND verifiedSend < 1 ";
        }

        try {
            optionalSql = prepareDateLimitation(optionalSql, searchDTO);
        } catch (ParseException e) {
            // TODO Auto-generated catch block

        }
        if (searchDTO.isSuccess()) {
            // sql = "SELECT * FROM ((" + firstPartOfSql + optionalSql +
            // getClause(optionalSql) + " delivaryStatus = 1 ) UNION "
            // + "(" + firstPartOfSql + optionalSql + getClause(optionalSql) + "
            // verifiedSend = 1 )) t " + lastPartOfSql;

            switch (searchDTO.getType()) {
                case Constants.VERIFIED:
                    sql = firstPartOfSql + optionalSql + getClause(optionalSql) + " verifiedSend = 1  " + lastPartOfSql;
                    break;
                case Constants.DELIVERED:
                    sql = firstPartOfSql + optionalSql + getClause(optionalSql) + " delivaryStatus = 1  " + lastPartOfSql;
                    break;
                case Constants.BOTH:
                    sql = "SELECT * FROM ((" + firstPartOfSql + optionalSql + getClause(optionalSql)
                            + " delivaryStatus = 1 ) UNION " + "(" + firstPartOfSql + optionalSql + getClause(optionalSql)
                            + " verifiedSend = 1 )) t " + lastPartOfSql;
                    break;
                default:
                    sql = "SELECT * FROM ((" + firstPartOfSql + optionalSql + getClause(optionalSql)
                            + " delivaryStatus = 1 ) UNION " + "(" + firstPartOfSql + optionalSql + getClause(optionalSql)
                            + " verifiedSend = 1 )) t " + lastPartOfSql;
                    break;
            }
        } else {
            sql = firstPartOfSql + optionalSql + lastPartOfSql;
        }
        logger.debug("getMessagesList : SQL [SUCS: " + searchDTO.isSuccess() + " --> FAILED: " + searchDTO.isFailed()
                + "] --> " + sql);

        try {
            MapSqlParameterSource namedParameters = new MapSqlParameterSource();
            this.template.query(sql, namedParameters, (ResultSet rs, int rowNum) -> {
                MessagesDTO messageDTO = new MessagesDTO();
                messageDTO.setId(rs.getInt("id"));
                messageDTO.setRoutesId(rs.getInt("brandId"));
                messageDTO.setRoutesName(smsRouteRepo.findOne(rs.getLong("brandId")).getBrandName());
                messageDTO.setDestination(rs.getString("mobileNo"));
                messageDTO.setSms(rs.getString("message"));
                messageDTO.setBrandResponse(rs.getString("brandResponse"));
                messageDTO.setMsgId(rs.getString("responseId"));
                messageDTO.setSmsStatus(rs.getString("smsStatus"));
                messageDTO.setDate(rs.getString("sendDate"));
                messageDTO.setVerifiedSend(rs.getInt("verifiedSend"));
                messageDTO.setDlStatus(rs.getInt("delivaryStatus"));
                messageDTO.setCountryId(rs.getInt("countryId"));
                messageListByDateMap.put(messageDTO.getId(), messageDTO);

                return messageDTO;
            });
        } catch (Exception ex) {
            logger.debug("getMessagesList : exception --> " + ex.getMessage());
        }
        return new ArrayList<>(messageListByDateMap.values());
    }

    public List<SummaryDTO> getSummaryList(SearchDTO searchDTO) {

        ArrayList<SummaryDTO> list = new ArrayList<>();
        HashMap<String, HashMap<String, SummaryDTO>> countryMapping = new HashMap();

        // try {
        // db = databaseconnector.DBConnector.getInstance().makeConnection();
        // stmt = db.connection.createStatement();
        String countryOptionalSql = "";
        String routeOptionalSql = "";

        if (searchDTO.isSearchByCountryId()) {
            countryOptionalSql = " and countryId = " + searchDTO.getCountryId();
        }

        if (searchDTO.isSearchByRoutes()) {
            routeOptionalSql = " and brandId = " + searchDTO.getRoutesId();
        }

        String dateSpecificOptionalSql = null;
        try {
            dateSpecificOptionalSql = prepareDateLimitation(searchDTO);
        } catch (ParseException e) {
            // TODO Auto-generated catch block

        }

        // ==== part - 01 =======
        String sql = "select countryId, brandId,count(1) as total_verified from messages where verifiedSend = 1 "
                + countryOptionalSql + routeOptionalSql + dateSpecificOptionalSql + " group by countryId, brandId;";

        logger.debug("part - 01 --> " + sql);

        this.template.query(sql, (ResultSet rs, int rowNum) -> {
            Country country = countryRep.findOne(rs.getLong("countryId"));
            SMSRoute smsRoute = smsRouteRepo.findOne(rs.getLong("brandId"));

            if (countryMapping.containsKey(country.getCountryName())) {

                HashMap<String, SummaryDTO> brandMappingLocal = countryMapping.get(country.getCountryName());

                if (brandMappingLocal == null) {

                    SummaryDTO summaryDTO = new SummaryDTO();
                    summaryDTO.setCountryName(country.getCountryName());
                    summaryDTO.setRoutesName(smsRoute.getBrandName());
                    summaryDTO.setTotalVerified(rs.getInt("total_verified"));
                    brandMappingLocal = new HashMap<>();
                    brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
                    countryMapping.put(country.getCountryName(), brandMappingLocal);
                } else {
                    if (brandMappingLocal.containsKey(smsRoute.getBrandName())) {
                        brandMappingLocal.get(smsRoute.getBrandName())
                                .setTotalVerified(rs.getInt("total_verified"));
                    } else {
                        SummaryDTO summaryDTO = new SummaryDTO();
                        summaryDTO.setCountryName(country.getCountryName());
                        summaryDTO.setRoutesName(smsRoute.getBrandName());
                        summaryDTO.setTotalVerified(rs.getInt("total_verified"));
                        brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
                    }
                }
            } else {
                SummaryDTO summaryDTO = new SummaryDTO();
                summaryDTO.setCountryName(country.getCountryName());
                summaryDTO.setRoutesName(smsRoute.getBrandName());
                summaryDTO.setTotalVerified(rs.getInt("total_verified"));

                HashMap<String, SummaryDTO> brandMappingLocal = new HashMap<>();
                brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
                countryMapping.put(country.getCountryName(), brandMappingLocal);
            }
            return null;
        });

        // ===== part - 02 ======
        sql = "select countryId, brandId,count(1) as total_confirmed_by_operator from messages where delivaryStatus = 1"
                + countryOptionalSql + routeOptionalSql + dateSpecificOptionalSql + " group by countryId, brandId;";
        logger.debug("part - 02 --> " + sql);

        this.template.query(sql, (ResultSet rs, int rowNum) -> {
            Country country = countryRep.findOne(rs.getLong("countryId"));
            SMSRoute smsRoute = smsRouteRepo.findOne(rs.getLong("brandId"));

            if (countryMapping.containsKey(country.getCountryName())) {
                HashMap<String, SummaryDTO> brandMappingLocal = countryMapping.get(country.getCountryName());
                if (brandMappingLocal == null) {
                    SummaryDTO summaryDTO = new SummaryDTO();
                    summaryDTO.setCountryName(country.getCountryName());
                    summaryDTO.setRoutesName(smsRoute.getBrandName());
                    summaryDTO.setTotalConfirmedByOperator(rs.getInt("total_confirmed_by_operator"));
                    brandMappingLocal = new HashMap<>();
                    brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
                    countryMapping.put(country.getCountryName(), brandMappingLocal);
                } else {
                    if (brandMappingLocal.containsKey(smsRoute.getBrandName())) {
                        brandMappingLocal.get(smsRoute.getBrandName())
                                .setTotalConfirmedByOperator(rs.getInt("total_confirmed_by_operator"));
                    } else {
                        SummaryDTO summaryDTO = new SummaryDTO();
                        summaryDTO.setCountryName(country.getCountryName());
                        summaryDTO.setRoutesName(smsRoute.getBrandName());
                        summaryDTO.setTotalVerified(rs.getInt("total_confirmed_by_operator"));
                        brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
                    }
                }
            } else {
                SummaryDTO summaryDTO = new SummaryDTO();
                summaryDTO.setCountryName(country.getCountryName());
                summaryDTO.setRoutesName(smsRoute.getBrandName());
                summaryDTO.setTotalConfirmedByOperator(rs.getInt("total_confirmed_by_operator"));

                HashMap<String, SummaryDTO> brandMappingLocal = new HashMap<>();
                brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
                countryMapping.put(country.getCountryName(), brandMappingLocal);
            }

            return null;
        });

        // ===== part - 03 ======
        sql = "select countryId, brandId,count(1) as total_failed_by_operator from messages where delivaryStatus < 1 "
                + countryOptionalSql + routeOptionalSql + dateSpecificOptionalSql + " group by countryId, brandId;";
        logger.debug("part - 03 --> " + sql);

        this.template.query(sql, (ResultSet rs, int rowNum) -> {
            Country country = countryRep.findOne(rs.getLong("countryId"));
            SMSRoute smsRoute = smsRouteRepo.findOne(rs.getLong("brandId"));

            if (countryMapping.containsKey(country.getCountryName())) {
                HashMap<String, SummaryDTO> brandMappingLocal = countryMapping.get(country.getCountryName());
                if (brandMappingLocal == null) {
                    SummaryDTO summaryDTO = new SummaryDTO();
                    summaryDTO.setCountryName(country.getCountryName());
                    summaryDTO.setRoutesName(smsRoute.getBrandName());
                    summaryDTO.setTotalFailedByOperator(rs.getInt("total_failed_by_operator"));
                    brandMappingLocal = new HashMap<>();
                    brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
                    countryMapping.put(country.getCountryName(), brandMappingLocal);
                } else {
                    if (brandMappingLocal.containsKey(smsRoute.getBrandName())) {
                        brandMappingLocal.get(smsRoute.getBrandName())
                                .setTotalFailedByOperator(rs.getInt("total_failed_by_operator"));
                    } else {
                        SummaryDTO summaryDTO = new SummaryDTO();
                        summaryDTO.setCountryName(country.getCountryName());
                        summaryDTO.setRoutesName(smsRoute.getBrandName());
                        summaryDTO.setTotalVerified(rs.getInt("total_failed_by_operator"));
                        brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
                    }
                }
            } else {
                SummaryDTO summaryDTO = new SummaryDTO();
                summaryDTO.setCountryName(country.getCountryName());
                summaryDTO.setRoutesName(smsRoute.getBrandName());
                summaryDTO.setTotalFailedByOperator(rs.getInt("total_failed_by_operator"));

                HashMap<String, SummaryDTO> brandMappingLocal = new HashMap<>();
                brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
                countryMapping.put(country.getCountryName(), brandMappingLocal);
            }

            return null;
        });

        // ===== part - 04 ======
        sql = "select countryId, brandId,count(1) as total_sms from messages where id > -1  " + countryOptionalSql
                + routeOptionalSql + dateSpecificOptionalSql + " group by countryId, brandId;";
        logger.debug("part - 04 --> " + sql);

        this.template.query(sql, (ResultSet rs, int rowNum) -> {
            Country country = countryRep.findOne(rs.getLong("countryId"));
            SMSRoute smsRoute = smsRouteRepo.findOne(rs.getLong("brandId"));

            if (countryMapping.containsKey(country.getCountryName())) {
                HashMap<String, SummaryDTO> brandMappingLocal = countryMapping.get(country.getCountryName());
                if (brandMappingLocal == null) {
                    SummaryDTO summaryDTO = new SummaryDTO();
                    summaryDTO.setCountryName(country.getCountryName());
                    summaryDTO.setRoutesName(smsRoute.getBrandName());
                    summaryDTO.setTotalSms(rs.getInt("total_sms"));
                    brandMappingLocal = new HashMap<>();
                    brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
                    countryMapping.put(country.getCountryName(), brandMappingLocal);
                } else {
                    if (brandMappingLocal.containsKey(smsRoute.getBrandName())) {
                        brandMappingLocal.get(smsRoute.getBrandName()).setTotalSms(rs.getInt("total_sms"));
                    } else {
                        SummaryDTO summaryDTO = new SummaryDTO();
                        summaryDTO.setCountryName(country.getCountryName());
                        summaryDTO.setRoutesName(smsRoute.getBrandName());
                        summaryDTO.setTotalVerified(rs.getInt("total_failed_by_operator"));
                        brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
                    }
                }
            } else {
                SummaryDTO summaryDTO = new SummaryDTO();
                summaryDTO.setCountryName(country.getCountryName());
                summaryDTO.setRoutesName(smsRoute.getBrandName());
                summaryDTO.setTotalSms(rs.getInt("total_sms"));

                HashMap<String, SummaryDTO> brandMappingLocal = new HashMap<>();
                brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
                countryMapping.put(country.getCountryName(), brandMappingLocal);
            }

            return null;
        });

        Set set = countryMapping.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            HashMap<String, SummaryDTO> map = (HashMap<String, SummaryDTO>) me.getValue();

            Set internalSet = map.entrySet();
            Iterator internalI = internalSet.iterator();
            while (internalI.hasNext()) {
                Map.Entry internalMe = (Map.Entry) internalI.next();
                SummaryDTO summaryDTO = (SummaryDTO) internalMe.getValue();
                list.add(summaryDTO);
            }

        }
        return list;

    }

    public MyAppError addMessage(MessagesDTO messagesDTO) {
        // TODO Auto-generated method stub
        Integer total = 0;
        MyAppError error = new MyAppError();
        try {

            String query = "select COUNT(*) from messages where sendDate='" + messagesDTO.getDate() + "' and responseId='"
                    + messagesDTO.getResponseId() + "' AND brandId=" + messagesDTO.getRoutesId();

            MapSqlParameterSource namedParameters = new MapSqlParameterSource();

            total = this.template.queryForObject(query, namedParameters, Integer.class);

            if (total > 0) {
                error.setERROR_TYPE(MyAppError.OTHERERROR);
                error.setErrorMessage("Duplicate content!");
                return error;
            }

            namedParameters.addValue("brandId", messagesDTO.getRoutesId());
            namedParameters.addValue("mobileNo", messagesDTO.getDestination());
            namedParameters.addValue("message", messagesDTO.getMessage());
            namedParameters.addValue("brandResponse", messagesDTO.getBrandResponse());
            namedParameters.addValue("smsStatus", messagesDTO.getSmsStatus());
            namedParameters.addValue("sendDate", messagesDTO.getDate());
            namedParameters.addValue("vCode", messagesDTO.getvCode());
            namedParameters.addValue("verifiedSend", messagesDTO.getVerifiedSend());
            namedParameters.addValue("responseId", messagesDTO.getResponseId());
            namedParameters.addValue("countryId", messagesDTO.getCountryId());
            namedParameters.addValue("delivaryStatus", messagesDTO.getDlStatus());

            this.template.update(
                    "INSERT INTO messages(brandId,mobileNo,message,brandResponse,smsStatus,sendDate,vCode,verifiedSend,responseId,countryId,delivaryStatus) VALUES ( :brandId, :mobileNo, :message, :brandResponse, :smsStatus, :sendDate, :vCode, :verifiedSend, :responseId, :countryId, :delivaryStatus)",
                    namedParameters);
        } catch (DataAccessException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.error("error in MessageRepository of addMessage ---> " + e);
        }
        return error;
    }

    public MessagesDTO getMessage(Integer brandId, String msgId) {
        // TODO Auto-generated method stub
        //MessagesDTO messageDTO = null;
        List<MessagesDTO> messageDTO = new ArrayList<>();
        try {

            MapSqlParameterSource namedParameters = new MapSqlParameterSource();

            String sql = "SELECT * FROM messages WHERE responseId='" + msgId + "' and brandId = " + brandId
                    + " ORDER BY id DESC LIMIT 1;";

            messageDTO = this.template.query(sql, namedParameters, (ResultSet rs, int rowNum) -> {
                MessagesDTO messageDTO1 = new MessagesDTO();
                messageDTO1.setId(rs.getInt("id"));
                messageDTO1.setRoutesId(rs.getInt("brandId"));
                messageDTO1.setRoutesName(smsRouteRepo.findOne(new Long(rs.getInt("brandId"))).getBrandName());
                messageDTO1.setDestination(rs.getString("mobileNo"));
                messageDTO1.setSms(rs.getString("message"));
                messageDTO1.setBrandResponse(rs.getString("brandResponse"));
                messageDTO1.setResponseId(rs.getString("responseId"));
                messageDTO1.setSmsStatus(rs.getString("smsStatus"));
                messageDTO1.setDate(rs.getString("sendDate"));
                messageDTO1.setVerifiedSend(rs.getInt("verifiedSend"));
                messageDTO1.setDlStatus(rs.getInt("delivaryStatus"));
                return messageDTO1;
            });
        } catch (DataAccessException e) {
            logger.error("execption in MessageRepository of getMessage " + e);
        }
        return messageDTO.get(0);
    }

    public MyAppError updateDeliverySMSStatus(MessagesDTO msDTO) {
        // TODO Auto-generated method stub
        MyAppError error = new MyAppError();
        try {

            if (msDTO.getReceivedDate() == null || msDTO.getReceivedDate().isEmpty()) {
                String responseTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
                logger.debug("response Date:" + responseTime);
                msDTO.setReceivedDate(responseTime);
            }
            MapSqlParameterSource namedParameters = new MapSqlParameterSource();
            namedParameters.addValue("delivaryStatus", msDTO.getDlStatus());
            namedParameters.addValue("responseDate", msDTO.getReceivedDate());
            namedParameters.addValue("responseId", msDTO.getResponseId());
            namedParameters.addValue("brandId", msDTO.getRoutesId());

            int num = this.template.update(
                    "UPDATE messages SET delivaryStatus = :delivaryStatus, responseDate = :responseDate WHERE responseId = :responseId  AND brandId = :brandId",
                    namedParameters);
            logger.debug("updateDeliverySMSStatus [success] --> " + num);
        } catch (DataAccessException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("varified update failed --> " + e);
        }
        return error;
    }

    public MyAppError updateSuccessfulMessageVerificationStatus(MessagesDTO dto) {
        // TODO Auto-generated method stub
        MyAppError error = new MyAppError();
        try {

            MapSqlParameterSource namedParameters = new MapSqlParameterSource();

            namedParameters.addValue("responseDate", dto.getReceivedDate());
            namedParameters.addValue("mobileNo", dto.getDestination());
            namedParameters.addValue("vCode", dto.getvCode());

            int num = this.template.update(
                    "UPDATE messages SET verifiedSend = 1,responseDate= :responseDate WHERE mobileNo = :mobileNo AND vCode = :vCode AND verifiedSend=0",
                    namedParameters);

            logger.debug("verifiedSend field updated [succes] --> " + num);
        } catch (DataAccessException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("verified update [failed] --> " + e);
        }

        return error;
    }

    public String verificationCode(String mobileNo) {
        // TODO Auto-generated method stub
        List<String> vCode = new ArrayList<>();
        try {

            String sql = "SELECT vCode FROM messages WHERE mobileNo='" + mobileNo + "' ORDER BY sendDate DESC LIMIT 1;";

            MapSqlParameterSource namedParameters = new MapSqlParameterSource();

            vCode = this.template.query(sql, namedParameters, (ResultSet rs, int rowNum) -> rs.getString("vCode"));
        } catch (DataAccessException e) {
            logger.error("execption in MessageRepository of verificationCode " + e);
        }
        return vCode.get(0);
    }

    public List<Integer> getBrandId(int countryId) {
        // TODO Auto-generated method stub
        List<Integer> brandIdMap = new ArrayList<>();

        String sql = "SELECT smsbrands.balence-smsroutes.smsrates as remainingBalance, smsroutes.brandId as brandId,smsroutes.smsrates,smsroutes.priority, smsbrands.brandName, smsbrands.balence, smsbrands.commercial "
                + "FROM smsroutes " + "INNER JOIN smsbrands "
                + "ON smsbrands.id=smsroutes.brandId AND smsroutes.countryId =:countryId AND smsbrands.commercial AND smsbrands.balence-smsroutes.smsrates > 0 "
                + "ORDER BY smsroutes.priority DESC,smsroutes.smsrates ASC;";

        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("countryId", countryId);

        brandIdMap = this.template.query(sql, namedParameters, (ResultSet rs, int rowNum) -> rs.getInt("brandId"));

        return brandIdMap;
    }

}
