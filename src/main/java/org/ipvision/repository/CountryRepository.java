package org.ipvision.repository;

import java.util.List;

import org.ipvision.domain.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CountryRepository extends JpaRepository<Country, Long> {

//	Country findByCountrycode(String countryCode);
    @Query("FROM Country e WHERE LOWER(e.countryName) like CONCAT('%',:key,'%') or LOWER(e.countryCode) like CONCAT('%',:key,'%') or LOWER(e.countrySymbol) like CONCAT('%',:key,'%')")
    public List<Country> findCountriesBySearchKey(@Param("key") String key);

    @Query("SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END FROM Country c WHERE c.countrySymbol = :countrySymbol or c.countryName = :countryName or c.countryCode = :countryCode")
    boolean existsCountry(@Param("countrySymbol") String countrySymbol, @Param("countryName") String countryName, @Param("countryCode") String countryCode);

    @Query("SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END FROM Country c WHERE (c.countrySymbol = :countrySymbol or c.countryName = :countryName or c.countryCode = :countryCode) and id != :id")
    public Boolean existsCountry(@Param("id") Long id, @Param("countrySymbol") String countrySymbol, @Param("countryName") String countryName, @Param("countryCode") String countryCode);

    @Query("FROM Country c WHERE c.countryCode = :countryCode")
    public Country findByCode(@Param("countryCode") String countryCode);
}
