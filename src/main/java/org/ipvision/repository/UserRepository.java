package org.ipvision.repository;

import java.util.List;

import org.ipvision.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	User findByUsername(String username);

	@Query("FROM User u WHERE LOWER(u.username) like CONCAT('%',:key,'%')")
	List<User> findUsersBySearchKey(@Param("key") String key);

//	@Query("FROM User u WHERE LOWER(u.username) like CONCAT('%',:key,'%') or LOWER(u.userRoles) like CONCAT('%',:key,'%')")
}
