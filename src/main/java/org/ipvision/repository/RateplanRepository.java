package org.ipvision.repository;

import java.sql.ResultSet;
import java.util.List;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import org.ipvision.domain.Country;
import org.ipvision.domain.Rateplan;
import org.ipvision.domain.SMSRoute;
import org.ipvision.dto.SMSRouteDTO;
import org.ipvision.utils.MyAppError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class RateplanRepository {

    @Autowired
    private NamedParameterJdbcTemplate template;

    @Autowired
    private CountryRepository countryRep;

    @Autowired
    private SMSRouteRepository smsRouteRepository;

    public List<Rateplan> findAllRateplans(String key) {

        MapSqlParameterSource namedParameters = new MapSqlParameterSource();

        namedParameters.addValue("key", "%" + key + "%");
        namedParameters.addValue("priority", "");
        if (key.equals("high")) {
            namedParameters.addValue("priority", "%" + 3 + "%");
        } else if (key.equals("mid")) {
            namedParameters.addValue("priority", "%" + 2 + "%");
        } else if (key.equals("low")) {
            namedParameters.addValue("priority", "%" + 1 + "%");
        }

        List<Rateplan> rateplans = this.template.query(
                "select c.countryName, b.brandName, s.id, s.countryId, s.brandId, s.operatorId, s.smsrates, s.priority from smsroutes s "
                + "inner join country c on s.countryId=c.id " + "inner join smsbrands b on s.brandId=b.id "
                + "where LOWER(c.countryName) like :key or LOWER(b.brandName) like :key or s.priority like :priority or s.smsrates like :key",
                namedParameters, new RowMapper<Rateplan>() {
            public Rateplan mapRow(ResultSet rs, int rowNum) throws SQLException {
                Rateplan rateplan = new Rateplan();

                rateplan.setId(rs.getLong("id"));

                Long countryId = rs.getLong("countryId");
                Long brandId = rs.getLong("brandId");

                Country country = countryRep.findOne(countryId);
                SMSRoute smsRoute = smsRouteRepository.findOne(brandId);

                rateplan.setCountry(country);
                rateplan.setSmsRoute(smsRoute);
                rateplan.setPriority(rs.getInt("priority"));
                rateplan.setSmsRates(rs.getDouble("smsrates"));

                return rateplan;
            }
        });

        return rateplans;
    }

    public List<Rateplan> findAllRateplans(int page, int size) {

        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        if (page <= 0) {
            page = 1;
        }
        namedParameters.addValue("page", page * size);
        namedParameters.addValue("size", size);

        List<Rateplan> rateplans = this.template.query("select * from smsroutes limit :page, :size", namedParameters,
                new RowMapper<Rateplan>() {
            public Rateplan mapRow(ResultSet rs, int rowNum) throws SQLException {
                Rateplan rateplan = new Rateplan();

                rateplan.setId(rs.getLong("id"));

                Long countryId = rs.getLong("countryId");
                Long brandId = rs.getLong("brandId");

                Country country = countryRep.findOne(countryId);
                SMSRoute smsRoute = smsRouteRepository.findOne(brandId);

                rateplan.setCountry(country);
                rateplan.setSmsRoute(smsRoute);
                rateplan.setPriority(rs.getInt("priority"));
                rateplan.setSmsRates(rs.getDouble("smsrates"));

                return rateplan;
            }
        });

        return rateplans;
    }

    public int deleteById(Long id) {

        String query = "delete from smsroutes WHERE id =:id";

        SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);

        return template.update(query, namedParameters);
    }

    public Long count() {
        // TODO Auto-generated method stub
        SqlParameterSource namedParameters = new MapSqlParameterSource();
        return this.template.queryForObject("select count(*) from smsroutes", namedParameters,
                Long.class);
    }

    public Rateplan findOne(Long id) {
        // TODO Auto-generated method stub
        List<Rateplan> rateplans = new ArrayList<>();
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("id", id);

        try {
            rateplans = this.template.query("select * from smsroutes where id = :id",
                    namedParameters, (ResultSet rs, int rowNum) -> {
                        Rateplan rateplan1 = new Rateplan();
                        rateplan1.setId(rs.getLong("id"));
                        Long countryId = rs.getLong("countryId");
                        Long brandId = rs.getLong("brandId");
                        Country country = countryRep.findOne(countryId);
                        SMSRoute smsRoute = smsRouteRepository.findOne(brandId);
                        rateplan1.setCountry(country);
                        rateplan1.setSmsRoute(smsRoute);
                        rateplan1.setPriority(rs.getInt("priority"));
                        rateplan1.setSmsRates(rs.getDouble("smsrates"));
                        return rateplan1;
                    });
        } catch (DataAccessException ex) {

        }
        return rateplans.get(0);
    }

    public Rateplan save(Rateplan rateplan) {
        // TODO Auto-generated method stub

        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("id", rateplan.getId());
        namedParameters.addValue("countryid", rateplan.getCountry().getId());
        namedParameters.addValue("operatorid", rateplan.getOperatorId());
        namedParameters.addValue("brandid", rateplan.getSmsRoute().getId());
        namedParameters.addValue("smsrates", rateplan.getSmsRates());
        namedParameters.addValue("priority", rateplan.getPriority());

        if (rateplan.getId() == null || rateplan.getId() == 0) {

            this.template.update(
                    "INSERT INTO smsroutes VALUES ( :id, :countryid, :operatorid, :brandid, :smsrates, :priority)",
                    namedParameters);
        } else {
            this.template.update(
                    "UPDATE smsroutes SET  countryId =:countryid, operatorId =:operatorid, brandId =:brandid, smsrates =:smsrates, priority =:priority WHERE id =:id",
                    namedParameters);
        }
        return rateplan;
    }

    public double getSMSRate(int countryId, int brandId) {
        // TODO Auto-generated method stub
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();

        namedParameters.addValue("countryId", countryId);
        namedParameters.addValue("brandId", brandId);

        return this.template.queryForObject(
                "select smsrates from smsroutes where countryId =:countryId and brandId =:brandId", namedParameters,
                Double.class);
    }

    public MyAppError addSMSRoutesInfo(Rateplan rateplan) {
        // TODO Auto-generated method stub
        MyAppError error = new MyAppError();
        try {
            // db =
            // databaseconnector.DBConnector.getInstance().makeConnection();
            // stmt = db.connection.createStatement();
            if (rateplan.getId() == null || rateplan.getId() == 0) {
                String sql = "";
                sql = "SELECT count(id) as id FROM smsroutes WHERE countryId = "
                        + rateplan.getCountry().getId().intValue() + " AND operatorId = " + rateplan.getOperatorId()
                        + " AND brandId = " + rateplan.getSmsRoute().getId() + "";

                System.out.println("Exception : " + sql);

                MapSqlParameterSource namedParameters = new MapSqlParameterSource();

                Integer nId = this.template.queryForObject(sql, namedParameters, Integer.class);

                if (nId != null && nId > 0) {
                    System.out.println("Rateplan Repository : Duplicate Contents");
                    error.setERROR_TYPE(MyAppError.OTHERERROR);
                    error.setErrorMessage("Duplicate Contents");
                    return error;
                }

                String query = "INSERT INTO smsroutes(countryId,operatorId,brandId,smsrates,priority) VALUES(?,?,?,?,?);";
                namedParameters.addValue("countryId", rateplan.getCountry().getId().intValue());
                namedParameters.addValue("operatorId", rateplan.getOperatorId());
                namedParameters.addValue("brandId", rateplan.getSmsRoute().getId().intValue());
                namedParameters.addValue("smsrates", rateplan.getSmsRates());
                namedParameters.addValue("priority", rateplan.getPriority());

                nId = this.template.update(
                        "INSERT INTO smsroutes(countryId,operatorId,brandId,smsrates,priority) VALUES(:countryId, :operatorId, :brandId, :smsrates, :priority)",
                        namedParameters);
                if (nId > 0) {
                    System.out.println("Rateplan Repository : SMSRoutes Saved");
                    error.setERROR_TYPE(MyAppError.NOERROR);
                    error.setErrorMessage("SMS Routes Saved Successfully");
                    return error;
                }
            } else {
                MapSqlParameterSource namedParameters = new MapSqlParameterSource();

                String sql = "";
                sql = "SELECT count(id) as id FROM smsroutes WHERE countryId = "
                        + rateplan.getCountry().getId().intValue() + " AND operatorId = " + rateplan.getOperatorId()
                        + " AND brandId = " + rateplan.getSmsRoute().getId() + " AND id != " + rateplan.getId().intValue();

                Integer nId = this.template.queryForObject(sql, namedParameters, Integer.class);

                if (nId != null && nId > 0) {
                    System.out.println("Rateplan Repository : Duplicate Contents");
                    error.setERROR_TYPE(MyAppError.OTHERERROR);
                    error.setErrorMessage("Duplicate Contents");
                    return error;
                }

                namedParameters.addValue("id", rateplan.getId());
                namedParameters.addValue("countryId", rateplan.getCountry().getId().intValue());
                namedParameters.addValue("operatorId", rateplan.getOperatorId());
                namedParameters.addValue("brandId", rateplan.getSmsRoute().getId().intValue());
                namedParameters.addValue("smsrates", rateplan.getSmsRates());
                namedParameters.addValue("priority", rateplan.getPriority());
                nId = this.template.update(
                        "UPDATE smsroutes SET  countryId =:countryId, operatorId =:operatorId, brandId =:brandId, smsrates =:smsrates, priority =:priority WHERE id =:id",
                        namedParameters);
                if (nId > 0) {
                    System.out.println("Rateplan Repository : SMSRoutes Updated");
                    error.setERROR_TYPE(MyAppError.NOERROR);
                    error.setErrorMessage("SMS Routes Updated Successfully");
                    return error;
                }
            }
        } catch (DataAccessException e) {
            System.out.println("Exception : " + e.getMessage());
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
        }
        // logger.error("" + e);
        return error;
    }

    public ArrayList<Map.Entry<Integer, SMSRouteDTO>> getSMSRoutesList() {

        Map<Integer, SMSRouteDTO> brandInfMap = new HashMap<>();
        ArrayList<Map.Entry<Integer, SMSRouteDTO>> entryList = new ArrayList<>();
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();

        try {

            String sql = "SELECT smsroutes.id AS id,country.countryName AS con,country.id AS conId,operators.operatorName AS op,smsbrands.brandName AS brand,smsroutes.brandId AS brandID,smsroutes.smsrates AS rate,smsroutes.priority AS priority\n"
                    + "    FROM smsroutes LEFT JOIN country\n"
                    + "	ON smsroutes.countryId = country.id \n"
                    + "		 LEFT JOIN operators\n"
                    + "			ON smsroutes.operatorId = operators.id \n"
                    + "				LEFT JOIN  smsbrands \n"
                    + "					ON smsroutes.brandId= smsbrands.id ORDER BY smsroutes.smsrates ASC;";

            this.template.query(sql, namedParameters, (ResultSet rs, int rowNum) -> {
                SMSRouteDTO brandDTO = new SMSRouteDTO();
                brandDTO.setId(rs.getInt("id"));
                brandDTO.setCountryName(rs.getString("con"));
                brandDTO.setCountryId(rs.getInt("conId"));
                brandDTO.setOperatorName(rs.getString("op"));
                brandDTO.setBrandName(rs.getString("brand"));
                brandDTO.setBrandId(rs.getInt("brandID"));
                brandDTO.setSmsRates(rs.getDouble("rate"));
                brandDTO.setPriority(rs.getInt("priority"));
                brandInfMap.put(brandDTO.getId(), brandDTO);

                return brandDTO;
            });

            entryList = new ArrayList<>(brandInfMap.entrySet());

            Collections.sort(entryList, (Map.Entry<Integer, SMSRouteDTO> integerEmployeeEntry, Map.Entry<Integer, SMSRouteDTO> integerEmployeeEntry2) -> new Double(integerEmployeeEntry.getValue().getSmsRates())
                    .compareTo(integerEmployeeEntry2.getValue().getSmsRates()));
        } catch (DataAccessException e) {

        }
        return entryList;
    }

}
