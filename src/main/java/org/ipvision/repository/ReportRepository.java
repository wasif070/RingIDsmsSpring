package org.ipvision.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.ipvision.domain.RateReport;
import org.ipvision.dto.SummaryDTO;
import org.ipvision.domain.Country;
import org.ipvision.domain.SMSRoute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class ReportRepository {

	@Autowired
	private NamedParameterJdbcTemplate template;

	@Autowired
	private CountryRepository countryRep;

	@Autowired
	private SMSRouteRepository smsRouteRepository;

	@Autowired
	private RateplanRepository rateplanRepository;

	public List<RateReport> findAllRate() {

		List<RateReport> rateReports = this.template.query(
				"SELECT smsroutes.id AS id,country.countryName AS con,country.id AS conId,operators.operatorName AS op, "
						+ "smsbrands.brandName AS brand,smsroutes.brandId AS brandID,smsroutes.smsrates AS rate,smsroutes.priority AS priority "
						+ "FROM smsroutes LEFT JOIN country ON smsroutes.countryId = country.id LEFT JOIN operators "
						+ "ON smsroutes.operatorId = operators.id LEFT JOIN  smsbrands "
						+ "ON smsroutes.brandId= smsbrands.id ORDER BY smsroutes.smsrates ASC",
				new RowMapper<RateReport>() {
					public RateReport mapRow(ResultSet rs, int rowNum) throws SQLException {
						RateReport rateReport = new RateReport();

						rateReport.setCountryId(rs.getInt("conId"));
						rateReport.setBrandId(rs.getInt("brandID"));
						rateReport.setCountryName(rs.getString("con"));
						rateReport.setBrandName(rs.getString("brand"));
						rateReport.setSmsRates(rs.getDouble("rate"));
						return rateReport;
					}
				});

		return rateReports;
	}

	public List<Map.Entry<Integer, RateReport>> getSMSRoutesList() {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		Map<Integer, RateReport> brandInfMap = new HashMap<>();
		List<Map.Entry<Integer, RateReport>> entryList;
		String sql = "SELECT smsroutes.id AS id,country.countryName AS con,country.id AS conId,operators.operatorName AS op,smsbrands.brandName AS brand,smsroutes.brandId AS brandID,smsroutes.smsrates AS rate,smsroutes.priority AS priority\n"
				+ "    FROM smsroutes LEFT JOIN country\n" + "    ON smsroutes.countryId = country.id \n"
				+ "         LEFT JOIN operators\n" + "            ON smsroutes.operatorId = operators.id \n"
				+ "                LEFT JOIN  smsbrands \n"
				+ "                    ON smsroutes.brandId= smsbrands.id ORDER BY smsroutes.smsrates ASC;";
		List<RateReport> rateReports = this.template.query(sql, new RowMapper<RateReport>() {
			public RateReport mapRow(ResultSet rs, int rowNum) throws SQLException {

				RateReport brandDTO = new RateReport();
				brandDTO.setId(rs.getInt("id"));
				brandDTO.setCountryName(rs.getString("con"));
				brandDTO.setCountryId(rs.getInt("conId"));
				brandDTO.setOperatorName(rs.getString("op"));
				brandDTO.setBrandName(rs.getString("brand"));
				brandDTO.setBrandId(rs.getInt("brandID"));
				brandDTO.setSmsRates(rs.getDouble("rate"));
				brandDTO.setPriority(rs.getInt("priority"));
				// brandInfMap.put(brandDTO.getId(), brandDTO);
				return brandDTO;
			}
		});
		for (RateReport rateReport : rateReports) {
			brandInfMap.put(rateReport.getId(), rateReport);
		}
		try {
			entryList = new ArrayList<>(brandInfMap.entrySet());
			Collections.sort(entryList, new Comparator<Map.Entry<Integer, RateReport>>() {
				@Override
				public int compare(Map.Entry<Integer, RateReport> integerEmployeeEntry,
						Map.Entry<Integer, RateReport> integerEmployeeEntry2) {
					return new Double(integerEmployeeEntry.getValue().getSmsRates())
							.compareTo(integerEmployeeEntry2.getValue().getSmsRates());
				}
			});
		} catch (Exception e) {
			return null;
		} finally {

		}
		return entryList;
	}

	public List<RateReport> getFailSmsList() {
		// TODO Auto-generated method stub

		List<RateReport> rateReports = this.template.query(
				"select mobileNo, message, responseId, delivaryStatus, brandName from messages m left join smsbrands b on m.brandId=b.id where delivaryStatus < 1 AND verifiedSend < 1",
				new RowMapper<RateReport>() {
					public RateReport mapRow(ResultSet rs, int rowNum) throws SQLException {
						RateReport rateReport = new RateReport();

						rateReport.setBrandName(rs.getString("brandName"));
						rateReport.setMobileNo(rs.getString("mobileNo"));
						rateReport.setMessage(rs.getString("message"));
						rateReport.setResponseId(rs.getString("responseId"));
						rateReport.setDeliveryStatus(rs.getInt("delivaryStatus"));
						return rateReport;
					}
				});

		return rateReports;
	}

	public List<RateReport> getSuccessSmsList() {
		// TODO Auto-generated method stub
		List<RateReport> rateReports = this.template.query(
				"select mobileNo, message, responseId, delivaryStatus, brandName, sendDate, responseDate from messages m left join smsbrands b on m.brandId=b.id where delivaryStatus = 1 OR verifiedSend = 1",
				new RowMapper<RateReport>() {
					public RateReport mapRow(ResultSet rs, int rowNum) throws SQLException {
						RateReport rateReport = new RateReport();

						rateReport.setBrandName(rs.getString("brandName"));
						rateReport.setMobileNo(rs.getString("mobileNo"));
						rateReport.setMessage(rs.getString("message"));
						rateReport.setResponseId(rs.getString("responseId"));
						rateReport.setDeliveryStatus(rs.getInt("delivaryStatus"));
						rateReport.setSendDate(rs.getDate("sendDate"));
						rateReport.setResponseDate(rs.getDate("responseDate"));
						return rateReport;
					}
				});

		return rateReports;
	}

	public List<RateReport> getAllSmsList() {
		// TODO Auto-generated method stub
		List<RateReport> rateReports = this.template.query(
				"select mobileNo, message, sendDate, responseId, delivaryStatus, brandName from messages m left join smsbrands b on m.brandId=b.id",
				new RowMapper<RateReport>() {
					public RateReport mapRow(ResultSet rs, int rowNum) throws SQLException {
						RateReport rateReport = new RateReport();

						rateReport.setBrandName(rs.getString("brandName"));
						rateReport.setMobileNo(rs.getString("mobileNo"));
						rateReport.setMessage(rs.getString("message"));
						rateReport.setResponseId(rs.getString("responseId"));
						rateReport.setDeliveryStatus(rs.getInt("delivaryStatus"));
						rateReport.setSendDate(rs.getDate("sendDate"));
						return rateReport;
					}
				});

		return rateReports;
	}

	public List<SummaryDTO> getSummarySms() {
		// TODO Auto-generated method stub
		List<SummaryDTO> list = new ArrayList<>();
		HashMap<String, HashMap<String, SummaryDTO>> countryMapping = new HashMap();

		// ==== part - 01 =======
		List<SummaryDTO> smsVerifiedSummaries = this.template.query(
				"select countryId, brandId,count(1) as total_verified from messages where verifiedSend = 1 group by countryId, brandId",
				new RowMapper<SummaryDTO>() {

					public SummaryDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

						Country country = countryRep.findOne(rs.getLong("countryId"));
						SMSRoute smsRoute = smsRouteRepository.findOne(rs.getLong("brandId"));

						if (countryMapping.containsKey(country.getCountryName())) {

							HashMap<String, SummaryDTO> brandMappingLocal = countryMapping
									.get(country.getCountryName());

							if (brandMappingLocal == null) {

								SummaryDTO summaryDTO = new SummaryDTO();
								summaryDTO.setCountryName(country.getCountryName());
								summaryDTO.setRoutesName(smsRoute.getBrandName());
								summaryDTO.setTotalVerified(rs.getInt("total_verified"));
								brandMappingLocal = new HashMap<>();
								brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
								countryMapping.put(country.getCountryName(), brandMappingLocal);
							} else {
								if (brandMappingLocal.containsKey(smsRoute.getBrandName())) {
									brandMappingLocal.get(smsRoute.getBrandName())
											.setTotalVerified(rs.getInt("total_verified"));
								} else {
									SummaryDTO summaryDTO = new SummaryDTO();
									summaryDTO.setCountryName(country.getCountryName());
									summaryDTO.setRoutesName(smsRoute.getBrandName());
									summaryDTO.setTotalVerified(rs.getInt("total_verified"));
									brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
								}
							}
						} else {
							SummaryDTO summaryDTO = new SummaryDTO();
							summaryDTO.setCountryName(country.getCountryName());
							summaryDTO.setRoutesName(smsRoute.getBrandName());
							summaryDTO.setTotalVerified(rs.getInt("total_verified"));

							HashMap<String, SummaryDTO> brandMappingLocal = new HashMap<>();
							brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
							countryMapping.put(country.getCountryName(), brandMappingLocal);
						}
						return null;
					}
				});

		// ===== part - 02 ======
		List<SummaryDTO> smsDelivariesSummary = this.template.query(
				"select countryId, brandId,count(1) as total_confirmed_by_operator from messages where delivaryStatus = 1 group by countryId, brandId",
				new RowMapper<SummaryDTO>() {

					public SummaryDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

						Country country = countryRep.findOne(rs.getLong("countryId"));
						SMSRoute smsRoute = smsRouteRepository.findOne(rs.getLong("brandId"));

						if (countryMapping.containsKey(country.getCountryName())) {
							HashMap<String, SummaryDTO> brandMappingLocal = countryMapping
									.get(country.getCountryName());
							if (brandMappingLocal == null) {
								SummaryDTO summaryDTO = new SummaryDTO();
								summaryDTO.setCountryName(country.getCountryName());
								summaryDTO.setRoutesName(smsRoute.getBrandName());
								summaryDTO.setTotalConfirmedByOperator(rs.getInt("total_confirmed_by_operator"));
								brandMappingLocal = new HashMap<>();
								brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
								countryMapping.put(country.getCountryName(), brandMappingLocal);
							} else {
								if (brandMappingLocal.containsKey(smsRoute.getBrandName())) {
									brandMappingLocal.get(smsRoute.getBrandName())
											.setTotalConfirmedByOperator(rs.getInt("total_confirmed_by_operator"));
								} else {
									SummaryDTO summaryDTO = new SummaryDTO();
									summaryDTO.setCountryName(country.getCountryName());
									summaryDTO.setRoutesName(smsRoute.getBrandName());
									summaryDTO.setTotalVerified(rs.getInt("total_confirmed_by_operator"));
									brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
								}
							}
						} else {
							SummaryDTO summaryDTO = new SummaryDTO();
							summaryDTO.setCountryName(country.getCountryName());
							summaryDTO.setRoutesName(smsRoute.getBrandName());
							summaryDTO.setTotalConfirmedByOperator(rs.getInt("total_confirmed_by_operator"));

							HashMap<String, SummaryDTO> brandMappingLocal = new HashMap<>();
							brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
							countryMapping.put(country.getCountryName(), brandMappingLocal);
						}

						return null;
					}
				});

		// ===== part - 03 ======
		List<SummaryDTO> smsFailedSummaries = this.template.query(
				"select countryId, brandId,count(1) as total_failed_by_operator from messages where delivaryStatus < 1 group by countryId, brandId",
				new RowMapper<SummaryDTO>() {

					public SummaryDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

						Country country = countryRep.findOne(rs.getLong("countryId"));
						SMSRoute smsRoute = smsRouteRepository.findOne(rs.getLong("brandId"));

						if (countryMapping.containsKey(country.getCountryName())) {
							HashMap<String, SummaryDTO> brandMappingLocal = countryMapping
									.get(country.getCountryName());
							if (brandMappingLocal == null) {
								SummaryDTO summaryDTO = new SummaryDTO();
								summaryDTO.setCountryName(country.getCountryName());
								summaryDTO.setRoutesName(smsRoute.getBrandName());
								summaryDTO.setTotalFailedByOperator(rs.getInt("total_failed_by_operator"));
								brandMappingLocal = new HashMap<>();
								brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
								countryMapping.put(country.getCountryName(), brandMappingLocal);
							} else {
								if (brandMappingLocal.containsKey(smsRoute.getBrandName())) {
									brandMappingLocal.get(smsRoute.getBrandName())
											.setTotalFailedByOperator(rs.getInt("total_failed_by_operator"));
								} else {
									SummaryDTO summaryDTO = new SummaryDTO();
									summaryDTO.setCountryName(country.getCountryName());
									summaryDTO.setRoutesName(smsRoute.getBrandName());
									summaryDTO.setTotalVerified(rs.getInt("total_failed_by_operator"));
									brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
								}
							}
						} else {
							SummaryDTO summaryDTO = new SummaryDTO();
							summaryDTO.setCountryName(country.getCountryName());
							summaryDTO.setRoutesName(smsRoute.getBrandName());
							summaryDTO.setTotalFailedByOperator(rs.getInt("total_failed_by_operator"));

							HashMap<String, SummaryDTO> brandMappingLocal = new HashMap<>();
							brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
							countryMapping.put(country.getCountryName(), brandMappingLocal);
						}

						return null;
					}
				});

		// ===== part - 04 ======
		List<SummaryDTO> smsSummaries = this.template.query(
				"select countryId, brandId,count(1) as total_sms from messages where id > -1 group by countryId, brandId",
				new RowMapper<SummaryDTO>() {

					public SummaryDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

						Country country = countryRep.findOne(rs.getLong("countryId"));
						SMSRoute smsRoute = smsRouteRepository.findOne(rs.getLong("brandId"));

						if (countryMapping.containsKey(country.getCountryName())) {
							HashMap<String, SummaryDTO> brandMappingLocal = countryMapping
									.get(country.getCountryName());
							if (brandMappingLocal == null) {
								SummaryDTO summaryDTO = new SummaryDTO();
								summaryDTO.setCountryName(country.getCountryName());
								summaryDTO.setRoutesName(smsRoute.getBrandName());
								summaryDTO.setTotalSms(rs.getInt("total_sms"));
								brandMappingLocal = new HashMap<>();
								brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
								countryMapping.put(country.getCountryName(), brandMappingLocal);
							} else {
								if (brandMappingLocal.containsKey(smsRoute.getBrandName())) {
									brandMappingLocal.get(smsRoute.getBrandName()).setTotalSms(rs.getInt("total_sms"));
								} else {
									SummaryDTO summaryDTO = new SummaryDTO();
									summaryDTO.setCountryName(country.getCountryName());
									summaryDTO.setRoutesName(smsRoute.getBrandName());
									summaryDTO.setTotalVerified(rs.getInt("total_failed_by_operator"));
									brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
								}
							}
						} else {
							SummaryDTO summaryDTO = new SummaryDTO();
							summaryDTO.setCountryName(country.getCountryName());
							summaryDTO.setRoutesName(smsRoute.getBrandName());
							summaryDTO.setTotalSms(rs.getInt("total_sms"));

							HashMap<String, SummaryDTO> brandMappingLocal = new HashMap<>();
							brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
							countryMapping.put(country.getCountryName(), brandMappingLocal);
						}

						return null;
					}
				});

		Set set = countryMapping.entrySet();
		Iterator i = set.iterator();
		while (i.hasNext()) {
			Map.Entry me = (Map.Entry) i.next();
			HashMap<String, SummaryDTO> map = (HashMap<String, SummaryDTO>) me.getValue();

			Set internalSet = map.entrySet();
			Iterator internalI = internalSet.iterator();
			while (internalI.hasNext()) {
				Map.Entry internalMe = (Map.Entry) internalI.next();
				SummaryDTO summaryDTO = (SummaryDTO) internalMe.getValue();
				list.add(summaryDTO);
			}
		}
		return list;
	}
}
