package org.ipvision.repository;

import java.util.List;

import org.ipvision.domain.SMSRoute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface SMSRouteRepository extends JpaRepository<SMSRoute, Long> {

    @Query("FROM SMSRoute e WHERE LOWER(e.brandName) like CONCAT('%',:key,'%') or LOWER(e.userName) like CONCAT('%',:key,'%') or LOWER(e.contactEmail) like CONCAT('%',:key,'%') or LOWER(e.balance) like CONCAT('%',:key,'%') or LOWER(e.commercial) like CONCAT('%',:key,'%')")
    public List<SMSRoute> findSMSRoutesBySearchKey(@Param("key") String key);

    @Query("SELECT CASE WHEN COUNT(s) > 0 THEN true ELSE false END FROM SMSRoute s WHERE s.brandName = :brandName and s.id != :id")
    public Boolean existsSmsRoute(@Param("id") Long id, @Param("brandName") String brandName);

    @Query("SELECT CASE WHEN COUNT(s) > 0 THEN true ELSE false END FROM SMSRoute s WHERE s.brandName = :brandName")
    public Boolean existsSmsRoute(@Param("brandName") String brandName);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("UPDATE SMSRoute s SET s.balance =:#{#brandDTO.balance} WHERE s.id = :#{#brandDTO.id}")
    public int updateBrandBalance(@Param("brandDTO") SMSRoute brandDTO);

    @Query("SELECT id FROM SMSRoute s WHERE s.brandName = :brandName")
    public int getBrandIdByName(@Param("brandName") String brandName);

}
