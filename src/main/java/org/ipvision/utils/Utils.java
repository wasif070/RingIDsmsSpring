package org.ipvision.utils;

import java.io.StringReader;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class Utils {

    public Utils() {
    }

    public static String removeBrackets(String s) {
        try {
            if (s.length() > 1) {
                StringBuilder sb = new StringBuilder(s);
                sb.deleteCharAt(0);
                sb.deleteCharAt(sb.length() - 1);
                return sb.toString();
            }
        } catch (Exception e) {
        }
        return s;
    }

//    public static String traduceUtf8(String _normalString) {
//        if (_normalString != null) {
//            try {
//                _normalString = new String(_normalString.getBytes("ISO-8859-1"), "UTF-8");
//            } catch (java.io.UnsupportedEncodingException e) {
//                System.err.println(e);
//            }
//        }
//        return _normalString;
//    }
    public static String base64_to_image(String arg) {
        String user_image = arg;
        user_image = user_image.replaceAll("-", "+");
        user_image = user_image.replaceAll("_", "/");
        switch (user_image.length() % 4) {
            case 0:
                break;
            case 2:
                user_image += "==";
                break;
            case 3:
                user_image += "=";
                break;
        }
        return user_image;
    }

    public static String getRandomPacketId(String userIdentity) {
        SecureRandom random = new SecureRandom();
        char[] chars = new char[8];
        for (int i = 0; i < chars.length; i++) {
            int v = random.nextInt(10 + 26 + 26);
            char c;
            if (v < 10) {
                c = (char) ('0' + v);
            } else if (v < 36) {
                c = (char) ('a' - 10 + v);
            } else {
                c = (char) ('A' - 36 + v);
            }
            chars[i] = c;
        }
        String key = new String(chars);
        if (userIdentity != null) {
            key = key + userIdentity;
        }
        return key;
    }

    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY");
        Date date = new Date();

        return dateFormat.format(date);
    }

    public static Document convertStringToDocument(String xmlStr) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(xmlStr)));
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String traduceUtf8(String _normal) {
        if (_normal != null) {
            try {
                _normal = new String(_normal.getBytes(), "UTF-8");
            } catch (java.io.UnsupportedEncodingException e) {
                System.err.println(e);
            }
        }
        return _normal;
    }

    public static String detectIP(HttpServletRequest request) {
        String ipAddress = request.getHeader("x-forwarded-for");
        if (ipAddress == null) {
            ipAddress = request.getHeader("X_FORWARDED_FOR");
            if (ipAddress == null) {
                ipAddress = request.getRemoteAddr();
            }
        }
        return ipAddress;
    }
}
