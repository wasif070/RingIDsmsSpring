package org.ipvision.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.util.HashMap;

public class Constants {

    public static HashMap<String, String> ALLOWED_USERS_LIST;
    // for heartBeatSender
    BufferedReader br = null;

    static {
        try {
            File file = new File("config.txt");
            BufferedReader br = null;
            try {
                String sCurrentLine;
                br = new BufferedReader(new FileReader(file));
                while ((sCurrentLine = br.readLine()) != null) {
                    if (br.readLine().contains("HEARTBEAT_SERVER_IP")) {
                    }
                    if (br.readLine().contains("HEARTBEAT_SERVER_PORT")) {
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (br != null) {
                        br.close();
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            HEARTBEAT_SERVER_IP = InetAddress.getByName("192.168.1.45");
            HEARTBEAT_SERVER_PORT = 1001;
        } catch (Exception e) {
        }
    }

    public static String UPLOAD_URL = "official/desktop/";

    public static String DOMAIN_NAME_UPLOAD = null;//"http://images.ringid.com";// [PRODUCTION]
    public static String DOMAIN_NAME_VIEW = null;//"http://stickerres.ringid.com";
    public static String DOMAIN_NAME_IMAGE_VIEW = null;//"http://stickerres.ringid.com";
    public static String IMAGE_UPLOAD_URL = "ringmarket/OfficialImageUploadHandler";
    public static String CONSTANTS_URL = "http://auth.ringid.com/cnstnts?";
    public static final String COMMUNICATION_PORT_SUFFIX = "comports";

    public final static String LOGIN_DTO = "loginDTO";
    public final static String COMMUNICATOR = "COMMUNICATOR";
    public final static String LOGIN_ID = "loginId";
    public final static long LOGIN_EXPIRE_TIME = 15 * 60 * 1000;

    public static Integer UNCOMMITED = 0; //no-use 
    public static Integer COMMITED = 1;

    public final static String MESSAGE = "message";
    public final static String COMMITED_STR = "commited";

    public static final String FAILED = "failed";
    public static final String SUCCESS = "success";

    //pagination
    public final static String DATA_ROWS = "DATA_ROWS";
    public final static String TOTAL_RECORDS = "totalRecords";
    public final static String TOTAL_PAGES = "totalPages";
    public final static String CURRENT_PAGE_NO = "currentPageNo";
    public final static String STARTING_RECORD_NO = "startingRecordNo";
    public final static String ENDING_RECORD_NO = "endingRecordNo";
    public final static String RECORD_PER_PAGE = "RECORD_PER_PAGE";
    public static final int VERIFIED = 1;
    public static final int DELIVERED = 2;
    public static final int BOTH = 3;
    public static final String[] DELIVERY_STATUS_TEXT = {"FAILED", "PENDING", "DELIVERED"};

////FOR MESSAGE 
    public static final String SERVER_URL = "";
    public static final String COUNTRY_CODE = "countryCode";
    public static final String MOBILE_NUMBER = "mobileNumber";
    public static final String SMS_MESSAGE = "textMessage";
    public static final String RESPONSE_MESSAGE_ID = "rMessageId";
    public static final String VERIFIED_SEND = "verifiedSend";
    public static final String VERIFICATION_CODE = "vCode";
    public static final String BRAND_ID = "brandId";
    public static final int IMG_TYPE_COVER = 1;
    public static final int IMG_TYPE_PROFILE = 2;
    public static final int IMG_TYPE_ALBUM = 3;

    public static final long SLEEP_TIME_100MS = 100L;
    public static final long SLEEP_TIME_1SEC = 1000L;
    public static final int MAX_RESEND_COUNTER = 10;
    public static final int DEFAULT_PACKET_SIZE = 1024;
    // -----  ACTION TYPES------------
    public static final int TYPE_SESSION_VALIDATION = 76;
    public static final int TYPE_ALBUM_IMAGE_UPLOAD = 102;
    public static final int TYPE_ADD_COVER_IMAGE = 103;
    public static final int TYPE_UPDATE_IMAGE_FROM_WEB = 63;//”update_image_from_web” 
    public static final int IMAGE_TYPE_ALBUM = 1;
    public static final int IMAGE_TYPE_PROFILE = 2;
    public static final int IMAGE_TYPE_COVER = 3;
    //   ===============IMAGE UPLOAD VARIABLES ================
    public static String CLIENTS_IMAGE_FOLDER = "clients_image";
    public static String IMAGE_CONTEXT_PATH_ONE = "imageServer";
    public static String IMAGE_CONTEXT_PATH_TWO = "imageServer/";
    public static String DEFAULT_IMAGE = "default.png";
    public static String WEB_IMAGE_EXTENSION = "web";
    public static int IMG_WIDTH = 100;
    public static int IMG_HEIGHT = 100;
    public static int IMG_WIDTH_WEB = 400;
    public static int IMG_HEIGHT_WEB = 400;
    public static final int BUFFER_SIZE = 1024;

    public static final String GNM = "gNm";

    public static String SERVER_IP;//own server ip
    public static int SERVER_PORT = 0;
    public static InetAddress HEARTBEAT_SERVER_IP = null;
    public static int HEARTBEAT_SERVER_PORT = 1001;
    public static long HEARTBEAT_SEND_INTERVAL = 5000L;
    public static final int SERVER_TYPE_IMAGE = 3;

    //Action
    public final static int RECORD_PER_PAGE_CHANGE = 1;
    public final static int PAGE_NAVIGATION = 2;
    public final static int ADD = 3;
    public final static int EDIT = 4;
    public final static int UPDATE = 5;
    public final static int DELETE = 6;
    public final static int RESET = 13;
    /* Sorting style */

    public final static int ASC_SORT = 0;
    public final static int DESC_SORT = 1;
    public final static String COLLECTION_LIST = "collectionList";

    /*------Sorting Column-----*/
    public final static int COLUMN_ONE = 1;
    public final static int COLUMN_TWO = 2;
    public final static int COLUMN_THREE = 3;
    public final static int COLUMN_FOUR = 4;
    public final static int COLUMN_FIVE = 5;
    public final static int COLUMN_SIX = 6;
    public final static int C = 6;

    ///////////////sticker market
    public static final int STICKER_CTG_TYPE_FREE = 1;
    public static final int STICKER_CTG_TYPE_TOP = 2;
    public static final int STICKER_CTG_TYPE_NEW = 3;

    public static final long STICKER_CTG_NEW_TIME_LIMIT = 1000 * 60 * 60 * 24 * 30;
    public static final String ALBUM_IMAGE_UPLOAD_URL = "http://localhost:8084/UploadSticker/";

    public static final int NOT_VERIFIED = 1;
    public static final int UN_DELIVERED = 2;

    public static final int SUPER_ADMIN = 1;
    public static final int ADMIN = 2;
    public static final int GUEST = 3;

}
