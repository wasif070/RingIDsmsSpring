package org.ipvision.controller;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

	@GetMapping({ "/login", ""})
	public String showLogin(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			// return "redirect:/admin/info";
			System.out.println("Login Controller Called and Redirect to home");
			return "redirect:home";
		}
		System.out.println("Login Controller Called and Redirect to Login");
		model.addAttribute("loginError", false);
		return "i18n/login";
	}

	@GetMapping({ "/login-error" })
	public String loginError(Model model) {
		model.addAttribute("loginError", true);
		System.out.println("Login Controller Called with login-error and Redirect to Login");
		return "i18n/login";
		// return "pages/login";
	}
}
