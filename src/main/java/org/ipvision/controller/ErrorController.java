package org.ipvision.controller;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ErrorController {
	
	@GetMapping("/error-content")
	public String accesssDenied404() {
		return "i18n/error/error404";

	}
	
	@GetMapping("/permission-denied")
	public String accesssDenied403(Principal user) {
		return "error";

	}
}
