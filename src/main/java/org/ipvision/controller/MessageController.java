package org.ipvision.controller;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.ipvision.constant.DTableOperationType;
import org.ipvision.domain.Country;
import org.ipvision.domain.Message;
import org.ipvision.domain.SMSRoute;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.dto.RateReportDTO;

import org.ipvision.dto.SummaryDTO;
import org.ipvision.service.ICountryService;
import org.ipvision.service.IMessageService;
import org.ipvision.service.IRateReportService;

import org.ipvision.service.ISMSRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/message")
public class MessageController {
	private static Logger log = Logger.getLogger("vodCustomLogger");

	private IMessageService messageService;

	@Autowired
	private ICountryService countryService;

	@Autowired
	private ISMSRouteService smsRouteService;
        
        @Autowired
        IRateReportService reportService;

	@Autowired
	public void setMessageService(IMessageService messageService) {
		this.messageService = messageService;
	}

	@GetMapping("/info/{id}")
	public String showMessages(@PathVariable("id") Long id) {

		Message message = messageService.findById(id);
		System.out.println(message);
		log.info(message);
		
		return "i18n/home";
	}

	@GetMapping("/allsmslist")
	public String allSmsList(Model model, Message message) {

		List<MessagesDTO> pageMessageDTO = messageService.getAllSmsList(message);

		List<Country> countries = countryService.findAll();
		List<SMSRoute> brands = smsRouteService.findAll();

		model.addAttribute("pageMessageDTO", pageMessageDTO);
		model.addAttribute("countries", countries);
		model.addAttribute("brands", brands);
		model.addAttribute("message", message);

		return "i18n/sms/allsms-dtable";
	}

	@PostMapping("/allsmslist")
	public String allSmsList(Model model, Message message,
			@RequestParam(name = "operation", required = false) DTableOperationType operation) {


		if (message.getGoPageNo() != null && !message.getGoPageNo().equals("")
				&& Integer.parseInt(message.getGoPageNo()) > 0) {
			message.setPageNo(Integer.parseInt(message.getGoPageNo()));
		}
		if (operation==null || operation == DTableOperationType.SELECT) {
			message.setPageNo(1);
		}
		else if(operation == DTableOperationType.NEXT){
			message.setPageNo(message.getPageNo()+1);
		}
		else if(operation == DTableOperationType.PREV){
			message.setPageNo(message.getPageNo()-1);
		}
		List<MessagesDTO> pageMessageDTO = messageService.getAllSmsList(message);

		if (pageMessageDTO.size() > 0) {
			System.out.println(pageMessageDTO.get(0));
		}

		List<Country> countries = countryService.findAll();
		List<SMSRoute> brands = smsRouteService.findAll();

		model.addAttribute("pageMessageDTO", pageMessageDTO);
		model.addAttribute("countries", countries);
		model.addAttribute("brands", brands);
		model.addAttribute("message", message);

		return "i18n/sms/allsms-dtable";
	}
	
	@GetMapping("/rateRoutelist")
	public String getSmsRouteRates(Model model) {

		System.out.println("rateRoutelist");

		return "i18n/sms/rate-dtable";
	}
	
	@PostMapping("/GetRates")
	public void GetRates(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html;charset=UTF-8");
        
        try (PrintWriter out = response.getWriter()) {

        	RateReportDTO rateReportDTO=reportService.getSMSRoutesList();
            Gson g = new Gson();
            response.getWriter().println(g.toJson(rateReportDTO));
        } finally {
            response.getWriter().close();
        }
	}
	
	@GetMapping("/sucsmslist")
	public String successSmsList(Model model, Message message) {

		List<MessagesDTO> pageMessageDTO = messageService.getSuccessSmsList(message);

		List<SMSRoute> brands = smsRouteService.findAll();

		model.addAttribute("pageMessageDTO", pageMessageDTO);
		model.addAttribute("brands", brands);
		model.addAttribute("message", message);

		return "i18n/sms/sucsmslist-dtable";
	}

	@PostMapping("/sucsmslist")
	public String successSmsList(Model model, Message message,
			@RequestParam(name = "operation", required = false) DTableOperationType operation) {

		if (message.getGoPageNo() != null && !message.getGoPageNo().equals("")
				&& Integer.parseInt(message.getGoPageNo()) > 0) {
			message.setPageNo(Integer.parseInt(message.getGoPageNo()));
		}
		if (operation==null || operation == DTableOperationType.SELECT) {
			message.setPageNo(1);
		}
		else if(operation == DTableOperationType.NEXT){
			message.setPageNo(message.getPageNo()+1);
		}
		else if(operation == DTableOperationType.PREV){
			message.setPageNo(message.getPageNo()-1);
		}
		List<MessagesDTO> pageMessageDTO = messageService.getSuccessSmsList(message);

		if (pageMessageDTO.size() > 0) {
			System.out.println(pageMessageDTO.get(0));
		}
		
		List<SMSRoute> brands = smsRouteService.findAll();

		model.addAttribute("pageMessageDTO", pageMessageDTO);
		model.addAttribute("brands", brands);
		model.addAttribute("message", message);

		return "i18n/sms/sucsmslist-dtable";
	}
	
	
	
	@GetMapping("/failsmslist")
	public String failSmsList(Model model, Message message) {

		List<MessagesDTO> pageMessageDTO = messageService.getFailSmsList(message);

		List<SMSRoute> brands = smsRouteService.findAll();

		model.addAttribute("pageMessageDTO", pageMessageDTO);
		model.addAttribute("brands", brands);
		model.addAttribute("message", message);

		return "i18n/sms/failsmslist-dtable";
	}

	@PostMapping("/failsmslist")
	public String failSmsList(Model model, Message message,
			@RequestParam(name = "operation", required = false) DTableOperationType operation) {

		if (message.getGoPageNo() != null && !message.getGoPageNo().equals("")
				&& Integer.parseInt(message.getGoPageNo()) > 0) {
			message.setPageNo(Integer.parseInt(message.getGoPageNo()));
		}
		if (operation==null || operation == DTableOperationType.SELECT) {
			message.setPageNo(1);
		}
		else if(operation == DTableOperationType.NEXT){
			message.setPageNo(message.getPageNo()+1);
		}
		else if(operation == DTableOperationType.PREV){
			message.setPageNo(message.getPageNo()-1);
		}
		List<MessagesDTO> pageMessageDTO = messageService.getFailSmsList(message);

		if (pageMessageDTO.size() > 0) {
			System.out.println(pageMessageDTO.get(0));
		}
		
		List<SMSRoute> brands = smsRouteService.findAll();

		model.addAttribute("pageMessageDTO", pageMessageDTO);
		model.addAttribute("brands", brands);
		model.addAttribute("message", message);

		return "i18n/sms/failsmslist-dtable";
	}
	
	
	@GetMapping("/summarysms")
	public String summarySmsList(Model model, Message message) {

		List<SummaryDTO> pageSummaryDTO = messageService.getSummarySmsList(message);

		List<SMSRoute> brands = smsRouteService.findAll();
                List<Country> countries = countryService.findAll();

		model.addAttribute("pageSummaryDTO", pageSummaryDTO);
		model.addAttribute("brands", brands);
		model.addAttribute("message", message);
                model.addAttribute("countries", countries);

		return "i18n/sms/summarysms-dtable";
	}

	@PostMapping("/summarysms")
	public String summarySmsList(Model model, Message message,
			@RequestParam(name = "operation", required = false) DTableOperationType operation) {

		if (message.getGoPageNo() != null && !message.getGoPageNo().equals("")
				&& Integer.parseInt(message.getGoPageNo()) > 0) {
			message.setPageNo(Integer.parseInt(message.getGoPageNo()));
		}
		if (operation==null || operation == DTableOperationType.SELECT) {
			message.setPageNo(1);
		}
		else if(operation == DTableOperationType.NEXT){
			message.setPageNo(message.getPageNo()+1);
		}
		else if(operation == DTableOperationType.PREV){
			message.setPageNo(message.getPageNo()-1);
		}
		List<SummaryDTO> pageSummaryDTO = messageService.getSummarySmsList(message);

		if (pageSummaryDTO.size() > 0) {
			System.out.println(pageSummaryDTO.get(0));
		}
		
		List<SMSRoute> brands = smsRouteService.findAll();
                List<Country> countries = countryService.findAll();

		model.addAttribute("pageSummaryDTO", pageSummaryDTO);
		model.addAttribute("brands", brands);
		model.addAttribute("message", message);
                model.addAttribute("countries", countries);

		return "i18n/sms/summarysms-dtable";
	}
}
