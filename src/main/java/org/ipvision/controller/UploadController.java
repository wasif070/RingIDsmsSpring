package org.ipvision.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/upload")
public class UploadController {

	@GetMapping({ "", "/" })
	public String upload() {
		return "i18n/file-upload";
	}
	
	@GetMapping({ "/csvfile" })
	public String uploadCsvFile() {
		
		return "i18n/country/csvfile-upload";
	}


	@PostMapping("/file")
	public String uploadFile(HttpServletRequest req, HttpServletResponse res, HttpSession session) {
		// Uploader uploader = new Uploader();
		// uploader.doProcess(req, res);
		return "redirect:/upload/success";
	}

}
