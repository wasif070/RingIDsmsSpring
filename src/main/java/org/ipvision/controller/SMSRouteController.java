package org.ipvision.controller;

import javax.validation.Valid;

import org.ipvision.constant.DTableOperationType;
import org.ipvision.domain.SMSRoute;
import org.ipvision.dto.SMSRouteDTO;
import org.ipvision.service.ISMSRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/smsroute")
public class SMSRouteController {
	private ISMSRouteService smsRouteService;

	@Autowired
	public void setSmsRouteService(ISMSRouteService smsRouteService) {
		Assert.notNull(smsRouteService);
		this.smsRouteService = smsRouteService;
	}

	@GetMapping("/info")
	public String showAllSMSRoutes(Model model,
			@RequestParam(name = "viewableRows", required = false) Long viewableRows,
			@RequestParam(name = "currentViewablePage", required = false) Long currentViewablePage,
			@RequestParam(name = "searchKey", required = false) String searchKey,
			@RequestParam(name = "operation", required = false) DTableOperationType operation,
			@RequestParam(name = "goPageNo", required = false) Long goPageNo) {
		SMSRouteDTO pageSMSRouteDTO = smsRouteService.findPageableDTO(viewableRows, currentViewablePage, searchKey,
				operation, goPageNo);
		model.addAttribute("pageSMSRouteDTO", pageSMSRouteDTO);
		return "i18n/smsroute/smsroute-dtable";
	}

	@GetMapping("/add")
	public String addSMSRoute(Model model, @RequestParam(name = "id", required = false) Long id) {
		SMSRoute smsRoute = new SMSRoute();
		if (id != null) {
			smsRoute = smsRouteService.findById(id);
		}
		model.addAttribute("smsroute", smsRoute);
		return "i18n/smsroute/add-smsroute";
	}

	@PostMapping("/add")
	public String addSMSRoute(@Valid @ModelAttribute("smsroute") SMSRoute smsRoute, BindingResult errors, @RequestParam(name = "id", required = false) Long id) {

		if (errors.hasErrors()) {
			return "i18n/smsroute/add-smsroute";
		}

		if (id != null) {
            if (!smsRouteService.existsSmsRoute(smsRoute.getId(), smsRoute.getBrandName())) {
                smsRouteService.save(smsRoute);
            } else {
                errors.rejectValue("brandName", "error.smsroute", "Brand name exist");
                return "i18n/smsroute/add-smsroute";
            }
        } else if (!smsRouteService.existsSmsRoute(smsRoute.getBrandName())) {
            smsRouteService.save(smsRoute);
        } else {
            errors.rejectValue("brandName", "error.smsroute", "Brand name exist");
            return "i18n/smsroute/add-smsroute";
        }

        return "redirect:/smsroute/info";
	}

	@GetMapping("/edit/{id}")
	public String editSMSRoute(@PathVariable("id") Long id, RedirectAttributes redirAttr) {
		// System.out.println("EDIT ID: " + id);
		redirAttr.addAttribute("id", id);
		return "redirect:/smsroute/add";
	}

	@GetMapping("/delete/{id}")
	public String deleteCountry(@PathVariable("id") Long id) {
		smsRouteService.delete(id);
		return "redirect:/smsroute/info";
	}
}
