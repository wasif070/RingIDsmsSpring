package org.ipvision.controller;

import java.io.OutputStream;
import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.ipvision.constant.DTableOperationType;
import org.ipvision.domain.Country;
import org.ipvision.domain.User;
import org.ipvision.dto.CountryDTO;
import org.ipvision.service.ICountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/country")
public class CountryController {

	private static Logger log = Logger.getLogger("vodCustomLogger");

        @Autowired
	private ICountryService countryService;
        
	@GetMapping("/info")
	public String showAllCountries(Model model,
			@RequestParam(name = "viewableRows", required = false) Long viewableRows,
			@RequestParam(name = "currentViewablePage", required = false) Long currentViewablePage,
			@RequestParam(name = "searchKey", required = false) String searchKey,
			@RequestParam(name = "operation", required = false) DTableOperationType operation,
			@RequestParam(name = "goPageNo", required = false) Long goPageNo, Principal principal) {
		CountryDTO pageCountryDTO = countryService.findPageableDTO(viewableRows, currentViewablePage, searchKey,
				operation, goPageNo);
		model.addAttribute("pageCountryDTO", pageCountryDTO);
		
		return "i18n/country/country-dtable";
	}

	@GetMapping("/add")
	@Secured({ "ROLE_ADMIN", "ROLE_SUPER_ADMIN" })
	public String addCountry(Model model, @RequestParam(name = "id", required = false) Long id) {

		Country country = new Country();
		if (id != null) {
			country = countryService.findById(id);
		}
		model.addAttribute("country", country);
		return "i18n/country/add-country";
	}

	@PostMapping("/add")
	@Secured({ "ROLE_ADMIN", "ROLE_SUPER_ADMIN" })
	public String addCountry(@Valid Country country, BindingResult errors,
			@RequestParam(name = "id", required = false) Long id) {

		if (errors.hasErrors()) {

			return "i18n/country/add-country";
		}

		if (id != null) {
			if (!countryService.existsCountry(country.getId(), country.getCountrySymbol(), country.getCountryName(),
					country.getCountryCode())) {
				countryService.save(country);
			} else {
				errors.rejectValue("countryCode", "error.country", "country symbol, name or code exist");
				return "i18n/country/add-country";
			}
		} else if (!countryService.existsCountry(country.getCountrySymbol(), country.getCountryName(),
				country.getCountryCode())) {
			countryService.save(country);
		} else {
			errors.rejectValue("countryCode", "error.country", "country symbol, name or code exist");
			return "i18n/country/add-country";
		}

		return "redirect:/country/info";
	}

	@GetMapping("/edit/{id}")
	@Secured({ "ROLE_ADMIN", "ROLE_SUPER_ADMIN" })
	public String editCountry(@PathVariable("id") Long id, RedirectAttributes redirAttr) {

		redirAttr.addAttribute("id", id);

		return "redirect:/country/add";
	}

	@GetMapping("/delete/{id}")
	@Secured({ "ROLE_ADMIN", "ROLE_SUPER_ADMIN" })
	public String deleteCountry(@PathVariable("id") Long id) {

		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (user.getUserRoles() == 3)
			return "redirect:/country/info";

		countryService.delete(id);
		return "redirect:/country/info";
	}

	@GetMapping("/downloadCsv")
	@Secured({ "ROLE_ADMIN", "ROLE_SUPER_ADMIN" })
	public void downloadCsvFile(HttpServletRequest request, HttpServletResponse response, HttpSession session) {

		response.setContentType("text/csv");
		response.setHeader("Content-Disposition", "attachment; filename=\"countries.csv\"");

		try {

			OutputStream outputStream = response.getOutputStream();
			List<Country> countries = countryService.findAll();

			outputStream.write("SL.".getBytes());
			outputStream.write(",".getBytes());
			outputStream.write("Country Symbol".getBytes());
			outputStream.write(",".getBytes());
			outputStream.write("Country Name".getBytes());
			outputStream.write(",".getBytes());
			outputStream.write("Country Code".getBytes());
			outputStream.write("\n".getBytes());
			int index = 1;
			for (Country country : countries) {

				outputStream.write(String.valueOf(index).getBytes());
				outputStream.write(",".getBytes());

				outputStream.write(country.getCountrySymbol().getBytes());
				outputStream.write(",".getBytes());

				outputStream.write(country.getCountryName().getBytes());
				outputStream.write(",".getBytes());

				outputStream.write(country.getCountryCode().getBytes());
				outputStream.write("\n".getBytes());

				index++;
			}
			outputStream.flush();
			outputStream.close();

		} catch (Exception e) {
			System.out.println("Exception : " + e.getMessage());
		}
	}

	@PostMapping("/csvfile")
	@Secured({ "ROLE_ADMIN", "ROLE_SUPER_ADMIN" })
	public String saveCountriesFromCsvFile(@RequestParam(name="file", required = false) MultipartFile file, Model model) {

		if (file==null || file.isEmpty()) {
			System.out.println("File Null");
			model.addAttribute("nofile", "Please select a file");
			return "i18n/country/csvfile-upload";
		}

		System.out.println("File Name --> :" + file.getName());

		List<Country> countries = countryService.csvToCountryList(file);

		for (Country country : countries) {

			if (!countryService.existsCountry(country.getCountrySymbol(), country.getCountryName(),
					country.getCountryCode())) {
				countryService.save(country);
			} else {
				log.info("Country Exist");
				System.out.println("Country Exist!");
			}
		}

		// return "i18n/country/csvfile-upload";
		return "redirect:/country/info";
	}

	@GetMapping("/csvfile")
	@Secured({ "ROLE_ADMIN", "ROLE_SUPER_ADMIN" })
	public String uploadCountriesFromCsvFile() {

		// return "i18n/country/csvfile-upload";
		return "i18n/country/csvfile-upload";
	}
}
