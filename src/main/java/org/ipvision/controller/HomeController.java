package org.ipvision.controller;

import org.apache.log4j.Logger;
import org.ipvision.domain.User;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/home")
public class HomeController {
    
    static Logger log = Logger.getLogger("vodCustomLogger");
    
    @GetMapping({"", "/", "/**"})
    public String showHome() {
        log.info("--------  Home Controller  ----------");
        return "i18n/home";
    }
}
