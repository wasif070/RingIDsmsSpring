package org.ipvision.controller;

import java.util.List;

import javax.validation.Valid;

import org.ipvision.constant.DTableOperationType;
import org.ipvision.domain.Country;
import org.ipvision.domain.Rateplan;
import org.ipvision.domain.SMSRoute;
import org.ipvision.dto.RateplanDTO;
import org.ipvision.service.ICountryService;
import org.ipvision.service.IRateplanService;
import org.ipvision.service.ISMSRouteService;
import org.ipvision.service.RateplanService;
import org.ipvision.utils.MyAppError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/rateplan")
public class RateplanController {
	
	private IRateplanService rateplanService;
	
	@Autowired
	private ICountryService countryService;

	@Autowired
	private ISMSRouteService smsRouteService;

	@Autowired
	public void setRateplanService(IRateplanService rateplanService) {
		Assert.notNull(rateplanService);
		this.rateplanService = rateplanService;
	}

	@GetMapping("/info")
	public String showAllCountries(Model model,
			@RequestParam(name = "viewableRows", required = false) Long viewableRows,
			@RequestParam(name = "currentViewablePage", required = false) Long currentViewablePage,
			@RequestParam(name = "searchKey", required = false) String searchKey,
			@RequestParam(name = "operation", required = false) DTableOperationType operation,
			@RequestParam(name = "goPageNo", required = false) Long goPageNo) {
		// System.out.println("Rateplan Controller before");
		RateplanDTO pageRateplanDTO = rateplanService.findPageableDTO(viewableRows, currentViewablePage, searchKey,
				operation, goPageNo);
		model.addAttribute("pageRateplanDTO", pageRateplanDTO);
		// System.out.println("Rateplan Controller after");
		return "i18n/rateplan/rateplan-dtable";
	}

	@GetMapping("/add")
	public String addRateplan(Model model, @RequestParam(name = "id", required = false) Long id) {
		Rateplan rateplan = new Rateplan();
		if (id != null) {
			rateplan = rateplanService.findById(id);
			System.out.println(rateplan);
		}

		List<Country> countries = countryService.findAll();
		List<SMSRoute> brands = smsRouteService.findAll();

		model.addAttribute("rateplan", rateplan);
		model.addAttribute("countries", countries);
		model.addAttribute("brands", brands);

		return "i18n/rateplan/add-rateplan";
	}
	
	@PostMapping("/add")
    public String addRateplan(@Valid @ModelAttribute("rateplan") Rateplan rateplan, BindingResult errors, Model model) {

        boolean flag = false;
        // System.out.println("Rateplan : " + rateplan);
        if (errors.hasErrors()) {
            flag = true;
            
            // return "i18n/rateplan/add-rateplan";
        }

        if (rateplan.getCountry().getId() == 0) {
            errors.rejectValue("country", "error.rateplan", "Please select a country");
            flag = true;
            // return "i18n/rateplan/add-rateplan";
        }
        if (rateplan.getSmsRoute().getId() == 0) {
            errors.rejectValue("smsRoute", "error.rateplan", "Please select a Brand");
            flag = true;
            // return "i18n/rateplan/add-rateplan";
        }
        if (flag) {
           
            List<Country> countries = countryService.findAll();
            List<SMSRoute> brands = smsRouteService.findAll();
            model.addAttribute("countries", countries);
            model.addAttribute("brands", brands);
            return "i18n/rateplan/add-rateplan";
        }

        MyAppError error = rateplanService.addSMSRoutesInfo(rateplan);
        if (error.getERROR_TYPE() > 0) {
        	
        	List<Country> countries = countryService.findAll();
            List<SMSRoute> brands = smsRouteService.findAll();
            model.addAttribute("countries", countries);
            model.addAttribute("brands", brands);
            model.addAttribute("duplicate", error.getErrorMessage());
            return "i18n/rateplan/add-rateplan";
        } 
        //rateplanService.save(rateplan);
        return "redirect:/rateplan/info";
    }

	@GetMapping("/edit/{id}")
	public String editRateplan(@PathVariable("id") Long id, RedirectAttributes redirAttr) {
		// System.out.println("EDIT ID: " + id);
		redirAttr.addAttribute("id", id);
		return "redirect:/rateplan/add";
	}

	@GetMapping("/delete/{id}")
	public String deleteRateplan(@PathVariable("id") Long id) {
		rateplanService.delete(id);
		return "redirect:/rateplan/info";
	}
}
