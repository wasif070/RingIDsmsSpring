package org.ipvision.controller;

import java.io.IOException;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.ipvision.domain.Country;
import org.ipvision.domain.SMSRoute;
import org.ipvision.domain.SmsUI;
import org.ipvision.service.ICountryService;
import org.ipvision.service.ISMSRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/send")
public class SMSController {

	@Autowired
	private ICountryService countryService;

	@Autowired
	private ISMSRouteService smsRouteService;

	@GetMapping({ "", "/" })
	public String sendSms(Model model, @RequestParam(name = "success", required = false) String success) {
		model.addAttribute("success", success);
		System.out.println("Success MSG : " + success);
		return "i18n/sendsms/send-sms";
	}

	@GetMapping("/sms")
	public String sendSMS(Model model, HttpSession session, HttpServletRequest req, HttpServletResponse res, @RequestParam(name="success",required=false) String attr) {

		SmsUI smsUI = new SmsUI();

		List<Country> countries = countryService.findAll();
		List<SMSRoute> brands = smsRouteService.findAll();
		model.addAttribute("smsui", smsUI);

		model.addAttribute("success", attr);
		System.out.println("Success MSG : " + attr);
		//System.out.println("Response Status : " + res.getStatus() + " Response MSG : " + res);

		session.setAttribute("countries", countries);
		session.setAttribute("brands", brands);
		return "i18n/sendsms/send-sms";
	}

	@PostMapping("/sms")
	public String sendSMS(@Valid @ModelAttribute("smsui") SmsUI smsUI, BindingResult errors, HttpServletRequest req,
			HttpServletResponse res, Model model) {
		boolean flag = false;

		if (errors.hasErrors()) {
			flag = true;
			// return "i18n/rateplan/add-rateplan";
		}

		if (smsUI.getCountryCode().equals("0")) {
			errors.rejectValue("countryCode", "error.smsui", "Please select a country");
			flag = true;
			// return "i18n/rateplan/add-rateplan";
		}
		if (smsUI.getBrandId() == 0) {
			errors.rejectValue("brandId", "error.smsui", "Please select a Brand");
			flag = true;
			// return "i18n/rateplan/add-rateplan";
		}
		if (flag) {
			return "i18n/sendsms/send-sms";
		}
		
		try {
			req.getRequestDispatcher("/SMSRoutesHandler").forward(req, res);
		} catch (ServletException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("send SMS PostHandler");

		return "i18n/sendsms/send-sms";
	}
}
