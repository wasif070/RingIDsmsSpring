package org.ipvision.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.ipvision.login.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.sun.javafx.image.IntToBytePixelConverter;

@Entity
@Table(name="users")
public class User implements UserDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="user_id")
	private Long id;

	@NotNull(message = "Username can't be empty")
	//@Size(min = 4, max = 40, message = "Full name must be between 4 and 40 characters")
	@Column(name="user_name")
	private String username;

	@NotNull
	@Size(min = 4, max = 32, message = "Password must be between 4 and 32 characters")
	//@Pattern(regexp = "^[a-zA-Z0-9]*$", message = "Must be contains alpha numeric characters")
	@Column(name="user_password")
	private String password;

	@NotNull(message = "User role can't be empty")
	@Column(name="permission_level")
	private int userRoles;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

//	public String getFullName() {
//		return fullName;
//	}
//
//	public void setFullName(String fullName) {
//		this.fullName = fullName;
//	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(int userRoles) {
		this.userRoles = userRoles;
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		//String[] multipleAuthorities = this.userRoles.split(",");
		List<GrantedAuthority> allAuthorities = new ArrayList<>();
		//for (String authority : multipleAuthorities) {
			//allAuthorities.add(new SimpleGrantedAuthority(authority));
		//}
		if(this.userRoles==1)
		{
			
			allAuthorities.add(new SimpleGrantedAuthority("ROLE_SUPER_ADMIN"));
		}
		else if(this.userRoles==2)
		{
	
			allAuthorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		}
		else
		{
			
			allAuthorities.add(new SimpleGrantedAuthority("ROLE_GUEST"));
		}
		return allAuthorities;
	}

//	@Override
//	public Collection<? extends GrantedAuthority> getAuthorities() {
//		 TODO Auto-generated method stub
//		String[] multipleAuthorities = this.userRoles.split(",");
//		List<GrantedAuthority> allAuthorities = new ArrayList<>();
//		for (String authority : multipleAuthorities) {
//			allAuthorities.add(new SimpleGrantedAuthority(authority));
//		}
//		List<GrantedAuthority> allAuthorities = new ArrayList<>();
//		allAuthorities.add(new SimpleGrantedAuthority(userRoles));
//		return allAuthorities;
//	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password=" + password + ", userRoles=" + userRoles
				+ ", getAuthorities()=" + getAuthorities() + "]";
	}

	

}
