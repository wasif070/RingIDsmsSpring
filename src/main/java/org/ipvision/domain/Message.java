package org.ipvision.domain;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.ipvision.utils.Constants;

public class Message {

    private boolean sortByID;
    private int recordPerPage;
    private int pageNo;
    private int column;
    private int sort;
    private int[] selectedIDs;
    private int action;
    private int id;
    private String routesName;
    private int routesId;
    private String destination;
    private String sms;
    private String response;
    private String smsStatus;
    private String message;
    private String fromDate;
    private String toDate;
    private String vCode;
    private int nSuccess = 0;
    private int nFailed = 0;
    private String countryName;
    private int countryId;
    private String receivedDate;
    private String responseId;
    private int dlStatus;
    private int type;
    private String[] checkboxValue;
    private String mobileNo;
    private String brandResponse;
    private int totalPages;
    private int totalEntries;
    private String deliveryStatusText;
    private String date;
    private String goPageNo;

    public Message() {
        recordPerPage = 100;
        action = 0;
        totalPages = 1;
        totalEntries = 0;
        column = 0;
        sort = 0;
        sortByID = false;
        pageNo = 1;
        dlStatus = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        fromDate = sdf.format(new Date());
        toDate = sdf.format(new Date());
    }

    public String getGoPageNo() {
        return goPageNo;
    }

    public void setGoPageNo(String goPageNo) {
        this.goPageNo = goPageNo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDeliveryStatusText() {
        return deliveryStatusText;
    }

    public void setDeliveryStatusText(String deliveryStatusText) {
        this.deliveryStatusText = deliveryStatusText;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getTotalEntries() {
        return totalEntries;
    }

    public void setTotalEntries(int totalEntries) {
        this.totalEntries = totalEntries;
    }

    public String getBrandResponse() {
        return brandResponse;
    }

    public void setBrandResponse(String brandResponse) {
        this.brandResponse = brandResponse;
    }

    public boolean isSortByID() {
        return sortByID;
    }

    public void setSortByID(boolean sortByID) {
        this.sortByID = sortByID;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public int[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(int[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRoutesName() {
        return routesName;
    }

    public void setRoutesName(String routesName) {
        this.routesName = routesName;
    }

    public int getRoutesId() {
        return routesId;
    }

    public void setRoutesId(int routesId) {
        this.routesId = routesId;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getSms() {
        return sms;
    }

    public void setSms(String sms) {
        this.sms = sms;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getSmsStatus() {
        return smsStatus;
    }

    public void setSmsStatus(String smsStatus) {
        this.smsStatus = smsStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getvCode() {
        return vCode;
    }

    public void setvCode(String vCode) {
        this.vCode = vCode;
    }

    public int getnSuccess() {
        return nSuccess;
    }

    public void setnSuccess(int nSuccess) {
        this.nSuccess = nSuccess;
    }

    public int getnFailed() {
        return nFailed;
    }

    public void setnFailed(int nFailed) {
        this.nFailed = nFailed;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(String receivedDate) {
        this.receivedDate = receivedDate;
    }

    public String getResponseId() {
        return responseId;
    }

    public void setResponseId(String responseId) {
        this.responseId = responseId;
    }

    public int getDlStatus() {
        return dlStatus;
    }

    public void setDlStatus(int dlStatus) {
        setDeliveryStatusText(Constants.DELIVERY_STATUS_TEXT[dlStatus + 1]);
        this.dlStatus = dlStatus;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String[] getCheckboxValue() {
        return checkboxValue;
    }

    public void setCheckboxValue(String[] checkboxValue) {
        this.checkboxValue = checkboxValue;
    }

    @Override
    public String toString() {
        return "Message [sortByID=" + sortByID + ", recordPerPage=" + recordPerPage + ", pageNo=" + pageNo + ", column="
                + column + ", sort=" + sort + ", selectedIDs=" + Arrays.toString(selectedIDs) + ", action=" + action
                + ", id=" + id + ", routesName=" + routesName + ", routesId=" + routesId + ", destination="
                + destination + ", sms=" + sms + ", response=" + response + ", smsStatus=" + smsStatus + ", message="
                + message + ", fromDate=" + fromDate + ", toDate=" + toDate + ", vCode=" + vCode + ", nSuccess="
                + nSuccess + ", nFailed=" + nFailed + ", countryName=" + countryName + ", countryId=" + countryId
                + ", receivedDate=" + receivedDate + ", responseId=" + responseId + ", dlStatus=" + dlStatus + ", type="
                + type + ", checkboxValue=" + Arrays.toString(checkboxValue) + ", mobileNo=" + mobileNo
                + ", brandResponse=" + brandResponse + ", totalPages=" + totalPages + ", totalEntries=" + totalEntries
                + ", deliveryStatusText=" + deliveryStatusText + ", date=" + date + ", goPageNo=" + goPageNo + "]";
    }

}
