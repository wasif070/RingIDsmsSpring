package org.ipvision.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "messages")
public class SendSMS implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

//	@Column(name = "brandid")
//	private int brandId;

	@NotEmpty(message = "Missing Mobile Number")
	@Column(name = "mobileno")
	private String mobileNo;

	@Column(name = "message")
	private String message;
	
	@Column(name = "brandresponse")
	private String brandResponse;

	@Column(name = "smsstatus")
	private String smsStatus;
	
	@Column(name = "senddate")
	private Date sendDate;

	@Column(name = "vcode")
	private String vCode;
	
	@Column(name = "verifiedsend")
	private int verifiedSend;

	@Column(name = "responseid")
	private String responseId;

	@Column(name = "responsedate")
	private Date responseDate;
	
	@Column(name = "delivarystatus")
	private int delivaryStatus;
	
	private Country country;

	private SMSRoute smsRoute;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getBrandResponse() {
		return brandResponse;
	}

	public void setBrandResponse(String brandResponse) {
		this.brandResponse = brandResponse;
	}

	public String getSmsStatus() {
		return smsStatus;
	}

	public void setSmsStatus(String smsStatus) {
		this.smsStatus = smsStatus;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public String getvCode() {
		return vCode;
	}

	public void setvCode(String vCode) {
		this.vCode = vCode;
	}

	public int getVerifiedSend() {
		return verifiedSend;
	}

	public void setVerifiedSend(int verifiedSend) {
		this.verifiedSend = verifiedSend;
	}

	public String getResponseId() {
		return responseId;
	}

	public void setResponseId(String responseId) {
		this.responseId = responseId;
	}

	public Date getResponseDate() {
		return responseDate;
	}

	public void setResponseDate(Date responseDate) {
		this.responseDate = responseDate;
	}

	public int getDelivaryStatus() {
		return delivaryStatus;
	}

	public void setDelivaryStatus(int delivaryStatus) {
		this.delivaryStatus = delivaryStatus;
	}

	@Id
	@ManyToOne
	@JoinColumn(name = "countryid",referencedColumnName="id")
	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@Id
	@ManyToOne
	@JoinColumn(name = "brandid", referencedColumnName="id")
	public SMSRoute getSmsRoute() {
		return smsRoute;
	}

	public void setSmsRoute(SMSRoute smsRoute) {
		this.smsRoute = smsRoute;
	}

	@Override
	public String toString() {
		return "SendSMS [id=" + id + ", mobileNo=" + mobileNo + ", message=" + message + ", brandResponse="
				+ brandResponse + ", smsStatus=" + smsStatus + ", sendDate=" + sendDate + ", vCode=" + vCode
				+ ", verifiedSend=" + verifiedSend + ", responseId=" + responseId + ", responseDate=" + responseDate
				+ ", delivaryStatus=" + delivaryStatus + ", country=" + country + ", smsRoute=" + smsRoute + "]";
	}

}
