package org.ipvision.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "smsbrands")
public class SMSRoute implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotEmpty(message = "Missing Brand Name")
	@Column(name = "brandname")
	private String brandName;

	@NotEmpty(message = "Missing User Name")
	@Column(name = "username")
	private String userName;

	@NotEmpty(message = "Missing Password")
	@Column(name = "password")
	private String password;

	@NotEmpty(message = "Missing API Id")
	@Column(name = "apiid")
	private String apiId;

	@NotEmpty(message = "Missing API URL")
	@Column(name = "apiurl")
	private String apiUrl;

	@NotEmpty(message = "Missing Contact Email")
	@Column(name = "contactemail")
	private String contactEmail;

	@NotEmpty(message = "Missing SenderId")
	@Column(name = "senderid")
	private String senderId;

	@NotNull(message = "Missing Balance")
	@Column(name = "balence")
	private Double balance;

	@NotNull(message = "Missing Commercial")
	@Column(name = "commercial")
	private Boolean commercial;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getApiUrl() {
		return apiUrl;
	}

	public void setApiUrl(String apiUrl) {
		this.apiUrl = apiUrl;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public Boolean getCommercial() {
		return commercial;
	}

	public void setCommercial(Boolean commercial) {
		this.commercial = commercial;
	}

	public String getApiId() {
		return apiId;
	}

	public void setApiId(String apiId) {
		this.apiId = apiId;
	}

	@Override
	public String toString() {
		return "SMSRoute [id=" + id + ", brandName=" + brandName + ", userName=" + userName + ", password=" + password
				+ ", apiId=" + apiId + ", apiUrl=" + apiUrl + ", contactEmail=" + contactEmail + ", senderId="
				+ senderId + ", balance=" + balance + ", commercial=" + commercial + "]";
	}

}
