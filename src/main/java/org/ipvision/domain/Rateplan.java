package org.ipvision.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "smsroutes")
public class Rateplan implements Serializable {

	@Column(name="id")
	private Long id;

	@Column(name = "operatorid")
	private int operatorId;

	@NotNull
	@Column(name = "smsrates")
	private double smsRates;

	@Column(name = "priority")
	private int priority;

	private Country country;

	private SMSRoute smsRoute;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(int operatorId) {
		this.operatorId = operatorId;
	}

	public double getSmsRates() {
		return smsRates;
	}

	public void setSmsRates(double smsRates) {
		this.smsRates = smsRates;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	@Id
	@ManyToOne
	@JoinColumn(name = "countryid",referencedColumnName="id")
	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@Id
	@ManyToOne
	@JoinColumn(name = "brandid", referencedColumnName="id")
	public SMSRoute getSmsRoute() {
		return smsRoute;
	}

	public void setSmsRoute(SMSRoute smsRoute) {
		this.smsRoute = smsRoute;
	}

	@Override
	public String toString() {
		return "Rateplan [id=" + id + ", operatorId=" + operatorId + ", smsRates=" + smsRates + ", priority=" + priority
				+ ", country=" + country + ", smsRoute=" + smsRoute + "]";
	}

}
