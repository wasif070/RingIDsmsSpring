package org.ipvision.domain;

import java.util.Date;

public class RateReport {

	private int id;
    private int countryId;
    private String countryName;
    private String countryCode;
    private String numberPattern;
    private int brandId;
    private String brandName;
    private int operatorId;
    private String operatorName;
    private double smsRates;
    private int priority;
    private String mobileNo;
    private String message;
    private String responseId;
    private int deliveryStatus;
    private Date sendDate;
    private Date responseDate;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getNumberPattern() {
        return numberPattern;
    }

    public void setNumberPattern(String numberPattern) {
        this.numberPattern = numberPattern;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public int getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(int operatorId) {
        this.operatorId = operatorId;
    }

    public double getSmsRates() {
        return smsRates;
    }

    public void setSmsRates(double smsRates) {
        this.smsRates = smsRates;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
    
    

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getResponseId() {
		return responseId;
	}

	public void setResponseId(String responseId) {
		this.responseId = responseId;
	}

	public int getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(int deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public Date getResponseDate() {
		return responseDate;
	}

	public void setResponseDate(Date responseDate) {
		this.responseDate = responseDate;
	}

	@Override
	public String toString() {
		return "RateReport [id=" + id + ", countryId=" + countryId + ", countryName=" + countryName + ", countryCode="
				+ countryCode + ", numberPattern=" + numberPattern + ", brandId=" + brandId + ", brandName=" + brandName
				+ ", operatorId=" + operatorId + ", operatorName=" + operatorName + ", smsRates=" + smsRates
				+ ", priority=" + priority + ", mobileNo=" + mobileNo + ", message=" + message + ", responseId="
				+ responseId + ", deliveryStatus=" + deliveryStatus + "]";
	}
	
}
