package org.ipvision;

import org.apache.log4j.Logger;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

    private static Logger log = Logger.getLogger("vodCustomLogger");

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        log.info("Servlet Initialize .... : " + ServletInitializer.class);
        return application.sources(SMSApplication.class);
    }
}
