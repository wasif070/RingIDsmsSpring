package org.ipvision.dto;

import org.ipvision.domain.Country;

public class CountryDTO extends PageableDTO<Country> {

    public CountryDTO() {
    }

    public CountryDTO(Long viewableRows) {
        super(viewableRows);
    }

    public CountryDTO(Long viewableRows, Long viewablePage) {
        super(viewableRows, viewablePage);
    }
}
