package org.ipvision.dto;

import org.ipvision.domain.User;

public class UserDTO extends PageableDTO<User> {

	public UserDTO() {
	}

	public UserDTO(Long viewableRows) {
		super(viewableRows);
	}

	public UserDTO(Long viewableRows, Long viewablePage) {
		super(viewableRows, viewablePage);
	}
}
