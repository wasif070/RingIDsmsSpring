package org.ipvision.dto;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class TestClass {
	private String countryName;
    private Map<Integer, Double> brands = new HashMap<>();

    public Map<Integer, Double> getBrands() {
        return brands;
    }

    public void setBrands(Map<Integer, Double> brands) {
        this.brands = brands;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public static class countryNameComparator implements Comparator<TestClass> {

        public static int sort = 0;

        @Override
        public int compare(TestClass arg1, TestClass arg2) {
            int returnVal;
            switch (sort) {
                case 0:
                    returnVal = arg1.getCountryName().compareTo(arg2.getCountryName());
                    break;
                case 1:
                    returnVal = arg2.getCountryName().compareTo(arg1.getCountryName());
                    break;
                default:
                    returnVal = arg1.getCountryName().compareTo(arg2.getCountryName());
                    break;
            }
            return returnVal;
        }
    }
}
