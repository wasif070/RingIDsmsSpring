package org.ipvision.dto;

public class SearchDTO extends BaseDTO {
	private Integer start=0;
    private Integer limit=100;
    private boolean searchByNumber = false;
    private boolean searchByCountryId = false;
    private boolean searchByRoutesId = false;
    private boolean searchByStatus = false;
    private boolean serachByFromDate = false;
    private boolean serachByToDate = false;
    private boolean success = false;
    private boolean failed = false;
    private int type;
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	public boolean isSearchByNumber() {
		return searchByNumber;
	}
	public void setSearchByNumber(boolean searchByNumber) {
		this.searchByNumber = searchByNumber;
	}
	public boolean isSearchByCountryId() {
		return searchByCountryId;
	}
	public void setSearchByCountryId(boolean searchByCountryId) {
		this.searchByCountryId = searchByCountryId;
	}
	public boolean isSearchByRoutesId() {
		return searchByRoutesId;
	}
	public void setSearchByRoutesId(boolean searchByRoutesId) {
		this.searchByRoutesId = searchByRoutesId;
	}
	public boolean isSearchByStatus() {
		return searchByStatus;
	}
	public void setSearchByStatus(boolean searchByStatus) {
		this.searchByStatus = searchByStatus;
	}
	public boolean isSerachByFromDate() {
		return serachByFromDate;
	}
	public void setSerachByFromDate(boolean serachByFromDate) {
		this.serachByFromDate = serachByFromDate;
	}
	public boolean isSerachByToDate() {
		return serachByToDate;
	}
	public void setSerachByToDate(boolean serachByToDate) {
		this.serachByToDate = serachByToDate;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public boolean isFailed() {
		return failed;
	}
	public void setFailed(boolean failed) {
		this.failed = failed;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "SearchDTO [start=" + start + ", limit=" + limit + ", searchByNumber=" + searchByNumber
				+ ", searchByCountryId=" + searchByCountryId + ", searchByRoutesId=" + searchByRoutesId
				+ ", searchByStatus=" + searchByStatus + ", serachByFromDate=" + serachByFromDate + ", serachByToDate="
				+ serachByToDate + ", success=" + success + ", failed=" + failed + ", type=" + type + "]";
	}
	public boolean isSearchByRoutes() {
		// TODO Auto-generated method stub
		return searchByRoutesId;
	}
    
}
