package org.ipvision.dto;

import java.util.ArrayList;

public class SmsUIDTO {
	private String countryCode;
    private String mobileNumber;
    private String textMessage;
    private String vCode;
    private int brandId;
    private ArrayList<SmsUIDTO> list = new ArrayList<>();

    public ArrayList<SmsUIDTO> getList() {
        return list;
    }

    public void setList(ArrayList<SmsUIDTO> list) {
        this.list = list;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public String getvCode() {
        return vCode;
    }

    public void setvCode(String vCode) {
        this.vCode = vCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getTextMessage() {
        return textMessage;
    }

    public void setTextMessage(String textMessage) {
        this.textMessage = textMessage;
    }
}
