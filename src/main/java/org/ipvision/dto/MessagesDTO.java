package org.ipvision.dto;

import java.util.ArrayList;

import org.ipvision.domain.Message;
import org.ipvision.utils.Constants;

public class MessagesDTO extends PageableDTO<Message> {

	private int id;
    private String routesName;
    private String destination;
    private String message;
    private String sms;
    private String brandResponse;
    private ArrayList<MessagesDTO> responseList = new ArrayList<>();
    private String date;
    private String receivedDate;
    private int nSuccess;
    private int nFailed;
    private String vCode;
    private String responseId;
    private int verifiedSend;
    private String countryName;
    private int dlStatus = 0;
    private String deliveryStatusText;
    private String sendResult;
    private String msgId;
    private String dbInsert;
    private double smsRate;
    private int countryId;
    private int routesId;
    private String smsStatus;
    private String fromDate;
    private String toDate;
    
	public MessagesDTO() {
		this.dlStatus = 0;
	}

	public MessagesDTO(Long viewableRows) {
		super(viewableRows);
	}

	public MessagesDTO(Long viewableRows, Long viewablePage) {
		super(viewableRows, viewablePage);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRoutesName() {
		return routesName;
	}

	public void setRoutesName(String routesName) {
		this.routesName = routesName;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSms() {
		return sms;
	}

	public void setSms(String sms) {
		this.sms = sms;
	}

	public String getBrandResponse() {
		return brandResponse;
	}

	public void setBrandResponse(String brandResponse) {
		this.brandResponse = brandResponse;
	}

	public ArrayList<MessagesDTO> getResponseList() {
		return responseList;
	}

	public void setResponseList(ArrayList<MessagesDTO> responseList) {
		this.responseList = responseList;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(String receivedDate) {
		this.receivedDate = receivedDate;
	}

	public int getnSuccess() {
		return nSuccess;
	}

	public void setnSuccess(int nSuccess) {
		this.nSuccess = nSuccess;
	}

	public int getnFailed() {
		return nFailed;
	}

	public void setnFailed(int nFailed) {
		this.nFailed = nFailed;
	}

	public String getvCode() {
		return vCode;
	}

	public void setvCode(String vCode) {
		this.vCode = vCode;
	}

	public String getResponseId() {
		return responseId;
	}

	public void setResponseId(String responseId) {
		this.responseId = responseId;
	}

	public int getVerifiedSend() {
		return verifiedSend;
	}

	public void setVerifiedSend(int verifiedSend) {
		this.verifiedSend = verifiedSend;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public int getDlStatus() {
		return dlStatus;
	}

	public void setDlStatus(int dlStatus) {
		setDeliveryStatusText(Constants.DELIVERY_STATUS_TEXT[dlStatus + 1]);
		this.dlStatus = dlStatus;
	}

	public String getDeliveryStatusText() {
		return deliveryStatusText;
	}

	public void setDeliveryStatusText(String deliveryStatusText) {
		this.deliveryStatusText = deliveryStatusText;
	}

	public String getSendResult() {
		return sendResult;
	}

	public void setSendResult(String sendResult) {
		this.sendResult = sendResult;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getDbInsert() {
		return dbInsert;
	}

	public void setDbInsert(String dbInsert) {
		this.dbInsert = dbInsert;
	}

	public double getSmsRate() {
		return smsRate;
	}

	public void setSmsRate(double smsRate) {
		this.smsRate = smsRate;
	}

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public int getRoutesId() {
		return routesId;
	}

	public void setRoutesId(int routesId) {
		this.routesId = routesId;
	}

	public String getSmsStatus() {
		return smsStatus;
	}

	public void setSmsStatus(String smsStatus) {
		this.smsStatus = smsStatus;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	@Override
	public String toString() {
		return "MessagesDTO [id=" + id + ", routesName=" + routesName + ", destination=" + destination + ", message="
				+ message + ", sms=" + sms + ", brandResponse=" + brandResponse + ", responseList=" + responseList
				+ ", date=" + date + ", receivedDate=" + receivedDate + ", nSuccess=" + nSuccess + ", nFailed="
				+ nFailed + ", vCode=" + vCode + ", responseId=" + responseId + ", verifiedSend=" + verifiedSend
				+ ", countryName=" + countryName + ", dlStatus=" + dlStatus + ", deliveryStatusText="
				+ deliveryStatusText + ", sendResult=" + sendResult + ", msgId=" + msgId + ", dbInsert=" + dbInsert
				+ ", smsRate=" + smsRate + ", countryId=" + countryId + ", routesId=" + routesId + ", smsStatus="
				+ smsStatus + ", fromDate=" + fromDate + ", toDate=" + toDate + "]";
	}
	
}
