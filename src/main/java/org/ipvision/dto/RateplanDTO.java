package org.ipvision.dto;

import java.util.List;

import org.ipvision.domain.Country;
import org.ipvision.domain.Rateplan;
import org.ipvision.domain.SMSRoute;

public class RateplanDTO extends PageableDTO<Rateplan>{
	

	
	public RateplanDTO() {
	}

	public RateplanDTO(Long viewableRows) {
		super(viewableRows);
	}

	public RateplanDTO(Long viewableRows, Long viewablePage) {
		super(viewableRows, viewablePage);
	}
}
