package org.ipvision.dto;

import java.util.HashMap;
import java.util.Map;

public class BrandRate {

	private String countryName;
	private Map<Integer, Double> brands = new HashMap<>();

	public Map<Integer, Double> getBrands() {
		return brands;
	}

	public void setBrands(Map<Integer, Double> brands) {
		this.brands = brands;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	@Override
	public String toString() {
		return "BrandRate [countryName=" + countryName + ", brands=" + brands + "]";
	}

}
