package org.ipvision.dto;

import java.util.List;
import java.util.Map;

import org.ipvision.domain.RateReport;

public class RateReportDTO extends PageableDTO<RateReport> {

	public RateReportDTO() {
	}

	public RateReportDTO(Long viewableRows) {
		super(viewableRows);
	}

	public RateReportDTO(Long viewableRows, Long viewablePage) {
		super(viewableRows, viewablePage);
	}

}
