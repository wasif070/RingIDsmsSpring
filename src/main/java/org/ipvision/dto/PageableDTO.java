package org.ipvision.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.ipvision.constant.DTableOperationType;

public abstract class PageableDTO<T> {

    Long totalRows = 0L;
    Long totalPages = 1L;
    Long viewableRows = 25L;
    Long viewablePage = 1L;
    List<T> listItems = new ArrayList<T>();
    DTableOperationType operation = DTableOperationType.SELECT;
    String searchKey = "";

    private List<T> dataRows;
    Map<Integer, String> vendors;

    public PageableDTO() {
        // TODO Auto-generated constructor stub
    }

    public PageableDTO(Long viewableRows) {
        this.viewableRows = viewableRows;
    }

    public PageableDTO(Long viewableRows, Long viewablePage) {
        this.viewableRows = viewableRows;
        this.viewablePage = viewablePage;
    }

    public Long getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(Long totalRows) {
        this.totalRows = totalRows;
    }

    public Long getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Long totalPages) {
        this.totalPages = totalPages;
    }

    public Long getViewableRows() {
        return viewableRows;
    }

    public void setViewableRows(Long viewableRows) {
        this.viewableRows = viewableRows;
    }

    public Long getViewablePage() {
        return viewablePage;
    }

    public void setViewablePage(Long viewablePage) {
        this.viewablePage = viewablePage;
    }

    public DTableOperationType getOperation() {
        return operation;
    }

    public void setOperation(DTableOperationType operation) {
        this.operation = operation;
    }

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public List<T> getListItems() {
        return listItems;
    }

    public void setListItems(List<T> listItems) {
        this.listItems = listItems;
    }

    public List<T> getDataRows() {
        return dataRows;
    }

    public void setDataRows(List<T> dataRows) {
        this.dataRows = dataRows;
    }

    public Map<Integer, String> getVendors() {
        return vendors;
    }

    public void setVendors(Map<Integer, String> vendors) {
        this.vendors = vendors;
    }

}
