package org.ipvision.dto;

public class BaseDTO {
	private int countryId;
    private int routesId;
    private String smsStatus;
    private String fromDate;
    private String toDate;
	public int getCountryId() {
		return countryId;
	}
	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}
	public int getRoutesId() {
		return routesId;
	}
	public void setRoutesId(int routesId) {
		this.routesId = routesId;
	}
	public String getSmsStatus() {
		return smsStatus;
	}
	public void setSmsStatus(String smsStatus) {
		this.smsStatus = smsStatus;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	@Override
	public String toString() {
		return "BaseDTO [countryId=" + countryId + ", routesId=" + routesId + ", smsStatus=" + smsStatus + ", fromDate="
				+ fromDate + ", toDate=" + toDate + "]";
	}
    
}
