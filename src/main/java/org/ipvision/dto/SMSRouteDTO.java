package org.ipvision.dto;

import java.util.Comparator;

import org.ipvision.domain.SMSRoute;

public class SMSRouteDTO extends PageableDTO<SMSRoute> {

    private Integer id;
    private String brandName;
    private String userName;
    private String password;
    private String apiUrl;
    private String apiId;
    private String contactEmail;
    private String senderId;
    private Integer brandId;
    private Double balance = 0.0;
    private Boolean commercial = false;
    private int priority;

    private int countryId;
    private String countryName;
    private String countryCode;
    private String numberPattern;

    private int operatorId;
    private String operatorName;
    private double smsRates;

    private boolean searchByCountryId = false;
    private boolean searchByOperatorId = false;
    private boolean searchByBrandId = false;
    private boolean searchByAll = false;

    public SMSRouteDTO() {
    }

    public SMSRouteDTO(Long viewableRows) {
        super(viewableRows);
    }

    public SMSRouteDTO(Long viewableRows, Long viewablePage) {
        super(viewableRows, viewablePage);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Boolean getCommercial() {
        return commercial;
    }

    public void setCommercial(Boolean commercial) {
        this.commercial = commercial;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getNumberPattern() {
        return numberPattern;
    }

    public void setNumberPattern(String numberPattern) {
        this.numberPattern = numberPattern;
    }

    public int getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(int operatorId) {
        this.operatorId = operatorId;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public double getSmsRates() {
        return smsRates;
    }

    public void setSmsRates(double smsRates) {
        this.smsRates = smsRates;
    }

    public boolean isSearchByCountryId() {
        return searchByCountryId;
    }

    public void setSearchByCountryId(boolean searchByCountryId) {
        this.searchByCountryId = searchByCountryId;
    }

    public boolean isSearchByOperatorId() {
        return searchByOperatorId;
    }

    public void setSearchByOperatorId(boolean searchByOperatorId) {
        this.searchByOperatorId = searchByOperatorId;
    }

    public boolean isSearchByBrandId() {
        return searchByBrandId;
    }

    public void setSearchByBrandId(boolean searchByBrandId) {
        this.searchByBrandId = searchByBrandId;
    }

    public boolean isSearchByAll() {
        return searchByAll;
    }

    public void setSearchByAll(boolean searchByAll) {
        this.searchByAll = searchByAll;
    }
    
    public static Comparator<SMSRouteDTO> prioriyCompair = new Comparator<SMSRouteDTO>() {
        @Override
        public int compare(SMSRouteDTO s1, SMSRouteDTO s2) {
            int rollno1 = s1.getPriority();
            int rollno2 = s2.getPriority();
            return rollno1 - rollno2;
            /*For ascending order*/
 /*For descending order*/

            //rollno2-rollno1;

        }
    };
}
