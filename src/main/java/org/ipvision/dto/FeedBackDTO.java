package org.ipvision.dto;

import java.util.List;
import java.util.Map;

public class FeedBackDTO {
	 private List<TestClass> dataRows;
	    Map<Integer, String> vendors;

	    public List<TestClass> getRates() {
	        return dataRows;
	    }
	    public void setRates(List<TestClass> rates) {
	        this.dataRows = rates;
	    }
	    public Map<Integer, String> getVendors() {
	        return vendors;
	    }
	    public void setVendors(Map<Integer, String> vendors) {
	        this.vendors = vendors;
	    }
}
