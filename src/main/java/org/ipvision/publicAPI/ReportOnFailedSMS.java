package org.ipvision.publicAPI;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.dto.SearchDTO;
import org.ipvision.repository.MessageRepository;
import org.ipvision.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 *
 * @author sujon
 */
public class ReportOnFailedSMS extends HttpServlet {

    @Autowired
    private MessageRepository messageRepository;

    ArrayList<MessagesDTO> list = new ArrayList<>();
    static Logger logger = Logger.getLogger("authCommunicationLogger");

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
                config.getServletContext());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @param request
     * @param response
     *
     * 1. routesId 2. recordPerPage 3. pageNo 4. column [Integer] 5. sort
     * [Integer] 6. sortByID [boolean]
     */
    public void doProcess(HttpServletRequest request, HttpServletResponse response) {
        try {
            PrintWriter out = response.getWriter();
            Gson gson = new Gson();
            ResponseDTO responseDTO = new ResponseDTO();
            boolean sucs = true;

            try {
                int routesId = Integer.valueOf(request.getParameter("routesId") != null ? request.getParameter("routesId") : "-1");
                int type = Integer.valueOf(request.getParameter("type") != null ? request.getParameter("type") : "-1");
                int recordPerPage = Integer.valueOf(request.getParameter("recordPerPage") != null ? request.getParameter("recordPerPage") : "10");
                String pageNoParam = request.getParameter("pageNo");
                int pageNo = (pageNoParam != null && pageNoParam.length() > 0 ? Integer.parseInt(pageNoParam) : 1);

                int column = Integer.valueOf(request.getParameter("column") != null ? request.getParameter("column") : "-1");
                int sort = Integer.valueOf(request.getParameter("sort") != null ? request.getParameter("sort") : "" + Constants.DESC_SORT);
//                String sortByIDParam = request.getParameter("sortByID");
//                boolean sortByID = (sortByIDParam != null && sortByIDParam.length() > 0 ? Boolean.valueOf(sortByIDParam) : false);

                SearchDTO searchDTO = new SearchDTO();
                //MessageScheduler scheduler = new MessageScheduler();
                if (routesId > 0) {
                    searchDTO.setSearchByRoutesId(true);
                    searchDTO.setRoutesId(routesId);
                }
                if (type > 0) {
                    searchDTO.setType(type);
                }

                searchDTO.setStart(pageNo);

                long start = (pageNo - 1) * recordPerPage;
                int limit = recordPerPage;
                searchDTO.setFailed(true);
                list = messageRepository.getMessagesList(start, limit, searchDTO);
//                list = scheduler.getFailedSMSList(column, sort, searchDTO);

                Integer total = messageRepository.getMessagesCount(searchDTO);
                CommonTask.prepareResponseDTO(pageNo, recordPerPage, list, responseDTO, total);

            } catch (Exception e) {
                logger.error("exception in ReportOnSentSMS " + e);
                sucs = false;
            }
            responseDTO.setSucs(sucs);
            out.println(gson.toJson(responseDTO));
        } catch (Exception e) {
            logger.error("exception [FailedSMSReportAction] --> " + e);
        }
    }
}
