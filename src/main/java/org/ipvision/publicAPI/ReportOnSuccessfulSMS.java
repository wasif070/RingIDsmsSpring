package org.ipvision.publicAPI;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.dto.SearchDTO;
import org.ipvision.repository.MessageRepository;
import org.ipvision.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

public class ReportOnSuccessfulSMS extends HttpServlet {

    @Autowired
    MessageRepository messageRepository;

    ArrayList<MessagesDTO> list = new ArrayList<>();
    static Logger logger = Logger.getLogger("authCommunicationLogger");

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
                config.getServletContext());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @param request
     * @param response
     *
     * 1. routesId 2. fromDate [yyyy-MM-dd] 3.toDate [yyyy-MM-dd] 4.
     * recordPerPage 5. pageNo 6. column [Integer] 7. sort [Integer] 8. sortByID
     * [boolean]
     */
    public void doProcess(HttpServletRequest request, HttpServletResponse response) {
        try {
            PrintWriter out = response.getWriter();
            Gson gson = new Gson();
            ResponseDTO responseDTO = new ResponseDTO();
            boolean sucs = true;

            try {
                int routesId = Integer.valueOf(request.getParameter("routesId") != null ? request.getParameter("routesId") : "-1");
                int type = Integer.valueOf(request.getParameter("type") != null ? request.getParameter("type") : "-1");
                String fromDate = request.getParameter("fromDate");
                String toDate = request.getParameter("toDate");
                int recordPerPage = Integer.valueOf(request.getParameter("recordPerPage") != null ? request.getParameter("recordPerPage") : "10");
                String pageNoParam = request.getParameter("pageNo");
                int pageNo = (pageNoParam != null && pageNoParam.length() > 0 ? Integer.parseInt(pageNoParam) : 1);
                int column = Integer.valueOf(request.getParameter("column") != null ? request.getParameter("column") : "-1");
                int sort = Integer.valueOf(request.getParameter("sort") != null ? request.getParameter("sort") : "" + Constants.DESC_SORT);
                String sortByIDParam = request.getParameter("sortByID");
                boolean sortByID = (sortByIDParam != null && sortByIDParam.length() > 0 ? Boolean.valueOf(sortByIDParam) : false);

                SearchDTO searchDTO = new SearchDTO();
                //MessageScheduler scheduler = new MessageScheduler();
                if (routesId > 0) {
                    searchDTO.setSearchByRoutesId(true);
                    searchDTO.setRoutesId(routesId);
                }
                if (fromDate != null && fromDate.length() > 0) {
                    searchDTO.setSerachByFromDate(true);
                    searchDTO.setFromDate(fromDate);
                }
                if (toDate != null && toDate.length() > 0) {
                    searchDTO.setSerachByToDate(true);
                    searchDTO.setToDate(toDate);
                }

                if (type > 0) {
                    searchDTO.setType(type);
                }

//            if (request.getParameter("action") != null) {
//                action = Integer.parseInt(request.getParameter("action"));
//            }
                long start = (pageNo - 1) * recordPerPage;
                int limit = recordPerPage;
                searchDTO.setSuccess(true);
//                dataListSize
                Integer total = messageRepository.getMessagesCount(searchDTO);
                list = messageRepository.getMessagesList(start, limit, searchDTO);

                CommonTask.prepareResponseDTO(pageNo, recordPerPage, list, responseDTO, total);

            } catch (Exception e) {
                logger.error("exception in ReportOnSentSMS " + e);
                sucs = false;
            }

            responseDTO.setSucs(sucs);
            out.println(gson.toJson(responseDTO));

        } catch (Exception e) {
            logger.error("exception in ReportOnSentSMS (2) -->  " + e);
        }
    }
}
