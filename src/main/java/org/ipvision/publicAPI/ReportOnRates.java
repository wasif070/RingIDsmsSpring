package org.ipvision.publicAPI;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.dto.SMSRouteDTO;
import org.ipvision.dto.TestClass;
import org.ipvision.repository.RateplanRepository;
import org.ipvision.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

public class ReportOnRates extends HttpServlet {

    @Autowired
    RateplanRepository rateplanRepository;

    ArrayList<MessagesDTO> list = new ArrayList<>();
    static Logger logger = Logger.getLogger("authCommunicationLogger");
    
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
                config.getServletContext());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @param request
     * @param response
     *
     * 1. routesId 2. fromDate [yyyy-MM-dd] 3.toDate [yyyy-MM-dd] 4.
     * recordPerPage 5. pageNo 6. column [Integer] 7. sort [Integer] 8. sortByID
     * [boolean]
     */
    public void doProcess(HttpServletRequest request, HttpServletResponse response) {
        try {
            PrintWriter out = response.getWriter();
            Gson gson = new Gson();
            ResponseDTO responseDTO = new ResponseDTO();
            boolean sucs = true;

            try {
                int countryId = Integer.valueOf(request.getParameter("countryId") != null ? request.getParameter("countryId") : "-1");
                int recordPerPage = Integer.valueOf(request.getParameter("recordPerPage") != null ? request.getParameter("recordPerPage") : "10");
                String pageNoParam = request.getParameter("pageNo");
                int pageNo = (pageNoParam != null && pageNoParam.length() > 0 ? Integer.parseInt(pageNoParam) : 1);
                int column = Integer.valueOf(request.getParameter("column") != null ? request.getParameter("column") : "-1");
                int sort = Integer.valueOf(request.getParameter("sort") != null ? request.getParameter("sort") : "" + Constants.DESC_SORT);

                Map<String, TestClass> map = new HashMap<>();
                //SMSRoutesTaskSchedular schedular = new SMSRoutesTaskSchedular();
                ArrayList<Map.Entry<Integer, SMSRouteDTO>> maps = rateplanRepository.getSMSRoutesList();
                for (Map.Entry<Integer, SMSRouteDTO> e : maps) {
                    SMSRouteDTO sms = e.getValue();
                    if (countryId > 0 && sms.getCountryId() != countryId) {
                        continue;
                    }
                    String key = sms.getOperatorName() + sms.getCountryName();
                    TestClass dto;
                    if (map.containsKey(key)) {
                        dto = map.get(key);
                    } else {
                        dto = new TestClass();
                        dto.setCountryName(sms.getCountryName());
                        map.put(key, dto);
                    }
                    dto.getBrands().put(sms.getBrandId(), sms.getSmsRates());
                }
                ArrayList ratelist = new ArrayList<>(map.values());
                TestClass.countryNameComparator countryNameComparator = new TestClass.countryNameComparator();
                countryNameComparator.sort = 0;
                Collections.sort(ratelist, countryNameComparator);

                CommonTask.prepareSubList(pageNo, recordPerPage, ratelist, responseDTO);

            } catch (Exception e) {
                logger.error("exception in ReportOnRates " + e);
                sucs = false;
            }

            responseDTO.setSucs(sucs);
            out.println(gson.toJson(responseDTO));

        } catch (Exception e) {
            logger.error("exception in ReportOnRates (2) -->  " + e);
        }
    }
}
