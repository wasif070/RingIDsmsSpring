/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.publicAPI;

import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 *
 * @author reefat
 */
public class CommonTask {

    static Logger logger = Logger.getLogger(CommonTask.class.getName());

    public static void prepareSubList(int pageNo, int recordPerPage, ArrayList list, ResponseDTO responseDTO) {
        int dataListSize = 0;
        if (pageNo < 1) {
            pageNo = 1;
        }
        if (list != null) {
            dataListSize = list.size();
            logger.debug("DataSize: " + dataListSize);
        } else {
            logger.error("list size 0");
            return;
        }
        int totalPages = 1;
        if (dataListSize > 0) {
            totalPages = dataListSize / recordPerPage;
            if (dataListSize % recordPerPage != 0) {
                totalPages++;
            }
        }
        if (totalPages < pageNo) {
            pageNo = totalPages;
        }
        int startinIndex = (pageNo - 1) * recordPerPage;
        int endIndex = pageNo * recordPerPage;

        responseDTO.setDataRows(new ArrayList<>(list.subList(startinIndex, (endIndex < dataListSize ? endIndex : dataListSize))));

        responseDTO.setTotalRecords(dataListSize);
        responseDTO.setTotalPages(totalPages);
        responseDTO.setCurrentPageNo(pageNo);
        responseDTO.setStartingRecordNo((pageNo - 1) * recordPerPage);
        responseDTO.setEndingRecordNo(pageNo * recordPerPage);
        responseDTO.setRecordPerPage(recordPerPage);
    }

    public static void prepareResponseDTO(int pageNo, int recordPerPage, ArrayList list, ResponseDTO responseDTO, Integer totalRecords) {
        int dataListSize = 0;
        if (pageNo < 1) {
            pageNo = 1;
        }
        if (list != null) {
            dataListSize = totalRecords;
            logger.debug("DataSize: " + dataListSize);
        } else {
            logger.error("list size 0");
            return;
        }
        int totalPages = 1;
        if (dataListSize > 0) {
            totalPages = dataListSize / recordPerPage;
            if (dataListSize % recordPerPage != 0) {
                totalPages++;
            }
        }
        if (totalPages < pageNo) {
            pageNo = totalPages;
        }
        responseDTO.setDataRows(list);

        responseDTO.setTotalRecords(dataListSize);
        responseDTO.setTotalPages(totalPages);
        responseDTO.setCurrentPageNo(pageNo);
        responseDTO.setStartingRecordNo((pageNo - 1) * recordPerPage);
        responseDTO.setEndingRecordNo(pageNo * recordPerPage);
        responseDTO.setRecordPerPage(recordPerPage);
    }
}
