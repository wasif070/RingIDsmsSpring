package org.ipvision.publicAPI;

import java.util.ArrayList;
import java.util.Map;
import org.ipvision.dto.CountryDTO;
import org.ipvision.dto.SMSRouteDTO;
import org.ipvision.dto.TestClass;


public class ResponseDTO {

    private Boolean sortByID;
    private String message;
    private ArrayList dataRows;
    private Integer totalRecords;
    private Integer totalPages;
    private Integer currentPageNo;
    private Integer startingRecordNo;
    private Integer endingRecordNo;
    private Integer recordPerPage;
    private String smsStatus;
    private Boolean sucs = true;
    private Map<Integer, String> vendors;
    private ArrayList<CountryDTO> countryDTO;
    private ArrayList<SMSRouteDTO> smsRoutesList;
    private ArrayList<TestClass> testList;

    public void setSortByID(boolean sortByID) {
        this.sortByID = sortByID;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setDataRows(ArrayList dataRows) {
        this.dataRows = dataRows;
    }

    public void setTotalRecords(int totalRecords) {
        this.totalRecords = totalRecords;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public void setCurrentPageNo(int currentPageNo) {
        this.currentPageNo = currentPageNo;
    }

    public void setStartingRecordNo(int startingRecordNo) {
        this.startingRecordNo = startingRecordNo;
    }

    public void setEndingRecordNo(int endingRecordNo) {
        this.endingRecordNo = endingRecordNo;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public void setSmsRoutesList(ArrayList<SMSRouteDTO> smsRoutesList) {
        this.smsRoutesList = smsRoutesList;
    }

    public void setTestList(ArrayList<TestClass> testList) {
        this.testList = testList;
    }

    public void setSmsStatus(String smsStatus) {
        this.smsStatus = smsStatus;
    }

    public void setSucs(boolean sucs) {
        this.sucs = sucs;
    }

    public void setVendors(Map<Integer, String> vendors) {
        this.vendors = vendors;
    }

    public void setCountryDTO(ArrayList<CountryDTO> countryDTO) {
        this.countryDTO = countryDTO;
    }
}
