package org.ipvision.publicAPI;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.ipvision.domain.SMSRoute;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.service.ISMSRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 *
 * @author sujon
 */
public class GetVendorList extends HttpServlet {

    @Autowired
    ISMSRouteService sMSRouteService;

    ArrayList<MessagesDTO> list = new ArrayList<>();
    static Logger logger = Logger.getLogger("authCommunicationLogger");
    
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
                config.getServletContext());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @param request
     * @param response
     *
     */
    public void doProcess(HttpServletRequest request, HttpServletResponse response) {
        try {
            PrintWriter out = response.getWriter();
            Gson gson = new Gson();
            ResponseDTO responseDTO = new ResponseDTO();
            boolean sucs = true;

            try {
                Map<Integer, String> brandList = new HashMap<>();
                ArrayList<SMSRoute> brandNameAndId = (ArrayList<SMSRoute>) sMSRouteService.findAll();
                for (SMSRoute next : brandNameAndId) {
                    brandList.put(next.getId().intValue(), next.getBrandName());
                }
                if (brandList.size() > 0) {
                    responseDTO.setVendors(brandList);
                }

            } catch (Exception e) {
                logger.error("exception in GetVendorList " + e);
                sucs = false;
            }
            responseDTO.setSucs(sucs);
            out.println(gson.toJson(responseDTO));
        } catch (Exception e) {
            logger.error("exception in GetVendorList " + e);
        }
    }
}
