/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.callBack;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletConfig;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.repository.MessageRepository;
import org.ipvision.service.ISMSRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 *
 * @author reefat
 */
@WebServlet(name = "SMSCountry", urlPatterns = {"/callBack/SMSCountry"})
public class SMSCountry extends HttpServlet {

    @Autowired
    ISMSRouteService sMSRouteService;
    @Autowired
    MessageRepository messageRepository;

    private static final ArrayList<Integer> failedResponse = new ArrayList<>();

    static {
        failedResponse.add(2);
        failedResponse.add(4);
        failedResponse.add(8);
        failedResponse.add(10);
        failedResponse.add(11);
    }

    private static Logger logger = Logger.getLogger("authCommunicationLogger");

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
                config.getServletContext());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp);
//        super.doGet(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    private void doProcess(HttpServletRequest request, HttpServletResponse response) {
        try {

            String jobno = request.getParameter("jobno");
            String mobilenumber = request.getParameter("mobilenumber");
            Integer statusCode = Integer.valueOf(request.getParameter("status"));
            String doneTime = request.getParameter("doneTime");
            String messagepart = request.getParameter("messagepart");
            logger.debug("------------- '/callBack/SMSCountry' called ---------------------");
            logger.debug("jobno --> " + jobno);
            logger.debug("mobilenumber --> " + mobilenumber);
            logger.debug("statusCode --> " + statusCode);
            logger.debug("doneTime --> " + doneTime);
            logger.debug("messagepart --> " + messagepart);

            logger.debug("status Text --> " + getText(statusCode));

            int brandId = sMSRouteService.getBrandIdByName("SMSCountry");
            MessagesDTO messagesDTO = messageRepository.getMessage(brandId, jobno);
            if (messagesDTO == null) {
                Thread.sleep(5000);
                messagesDTO = messageRepository.getMessage(brandId, jobno);
            }
            if (messagesDTO == null) {
                logger.error("messagesDTO not found for messageId[SMSCountry] --> " + jobno);
            } else if (statusCode == 3) {
                String responseTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
                logger.debug("response Date:" + responseTime);

                messagesDTO.setReceivedDate(responseTime);
                messagesDTO.setDlStatus(1);
                //db update
                messageRepository.updateDeliverySMSStatus(messagesDTO);
                //balance update                        
                //CommonTask.updateBalance(brandId, messagesDTO.getSmsRate());
                logger.debug("----------- All actions for successful delivery has been completed[SMSCountry] : jobno : " + jobno + " -----------");
            } else if (failedResponse.contains(statusCode)) {
                messagesDTO.setDlStatus(-1);
                //db update
                messageRepository.updateDeliverySMSStatus(messagesDTO);
                logger.error("Message sending failed [SMSCountry] --> " + getText(statusCode));
            }
//            Mobile_Number - Status - TimeStamp Description for Status
        } catch (Exception e) {
            logger.error("Exception in /callBack/SMSCountry --> " + e);
        }
    }

    private String getText(int code) {
        String statusText;
        switch (code) {
            case 0:
                statusText = "Message In Queue";
                break;
            case 1:
                statusText = "Submitted To Carrier";
                break;
            case 2:
                statusText = "Un Delivered";
                break;
            case 3:
                statusText = "Delivered";
                break;
            case 4:
                statusText = "Expired";
                break;
            case 8:
                statusText = "Rejected";
                break;
            case 9:
                statusText = "Message Sent";
                break;
            case 10:
                statusText = "Opted Out Mobile Number";
                break;
            case 11:
                statusText = "Invalid Mobile Number";
                break;
            default:
                statusText = "Status Code mismatched";
                break;
        }
        return statusText;
    }
}
