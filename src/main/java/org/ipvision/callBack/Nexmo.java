/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.callBack;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.repository.MessageRepository;
import org.ipvision.service.ISMSRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 *
 * @author IPVision Inc
 */
@WebServlet(name = "Nexmo", urlPatterns = {"/callBack/Nexmo"})
public class Nexmo extends HttpServlet {

    @Autowired
    ISMSRouteService sMSRouteService;
    @Autowired
    MessageRepository messageRepository;

    private static Logger logger = Logger.getLogger("authCommunicationLogger");

    /**
     * http://IPAddress/VirtualDirectory/page.aspx?msgId=@messageId&msisdn=@msisdn&deliveryStatus=@deliveryStatus&deliveryErrorCode=@deliveryErrorCode&SubmitDate=@SubmitDate&DelvDate=@delvdate
     *
     * http://IPAddress/VirtualDirectory/page.aspx?msgId=@messageId&msisdn=@msisdn&deliveryStatus=@deliveryStatus&deliveryErrorCode=@deliveryErrorCode&SubmitDate=@SubmitDate&DelvDate=@delvdate
     */
    /**
     * http://IPAddress/VirtualDirectory/page.aspx?msisdn=66837000111&to=12150000025
     * &network-code=52099&messageId=000000FFFB0356D2
     * &price=0.02000000&status=delivered
     * &scts=1208121359&err-code=0&message-timestamp=2012-08-12+13%3A59%3A37
     *
     * @param req
     * @param resp
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
                config.getServletContext());
    }
    

    private void doProcess(HttpServletRequest req, HttpServletResponse resp) {

        PrintWriter out = null;
        try {
            logger.debug("------------- '/callBack/Nexmo' called ---------------------");
            out = resp.getWriter();
//            String msgId = req.getParameter("msgId");
//            String msisdn = req.getParameter("msisdn");
//            String deliveryStatus = req.getParameter("deliveryStatus");
//            String deliveryErrorCode = req.getParameter("deliveryErrorCode");
//            String SubmitDate = req.getParameter("SubmitDate");
//            String DelvDate = req.getParameter("DelvDate");

//            http://38.108.92.154:8084/sms/callBack/nexmo?
//           ?msisdn=66837000111&to=12150000025
//&network-code=52099&messageId=000000FFFB0356D2
//&price=0.02000000&status=delivered
//&scts=1208121359&err-code=0&message-timestamp=2012-08-12+13%3A59%3A37
            String msisdn = req.getParameter("msisdn");
            String to = req.getParameter("to");//The SenderID you set in from in your request.
            String networkCode = req.getParameter("network-code");//The Mobile Country Code Mobile Network Code (MCCMNC) of the carrier this phone number is registered with.
            String msgId = req.getParameter("messageId");
            String deliveryStatus = req.getParameter("status");
            String deliveryErrorCode = req.getParameter("err-code");

            String SubmitDate = req.getParameter("SubmitDate"); //The Coordinated Universal Time (UTC) [YYMMDDHHMM]  -> For example, 1101181426 is 2011 Jan 18th 14:26.
            String DelvDate = req.getParameter("message-timestamp"); //YYYY-MM-DD HH:MM:SS --> For example, 2012-04-05 09:22:57.

            logger.debug("messageId --> " + msgId);
            logger.debug("msisdn --> " + msisdn);
            logger.debug("deliveryStatus --> " + deliveryStatus);
            logger.debug("deliveryErrorCode --> " + deliveryErrorCode);
            logger.debug("SubmitDate --> " + SubmitDate);
            logger.debug("DelvDate --> " + DelvDate);

            int brandId = sMSRouteService.getBrandIdByName("Nexmo");
//            String[] str = msgId.split("-");
//            msgId = str[1];
            MessagesDTO messagesDTO = messageRepository.getMessage(brandId, msgId);
            if (messagesDTO == null) {
                Thread.sleep(5000);
                messagesDTO = messageRepository.getMessage(brandId, msgId);
            }
            if (messagesDTO == null) {
                logger.error("messagesDTO not found for messageId[nexmo] --> " + msgId);
            } else if (deliveryStatus.equalsIgnoreCase("delivered")) {
                String responseTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                logger.debug("response Date:" + responseTime);

                messagesDTO.setReceivedDate(responseTime);
                messagesDTO.setDlStatus(1);
                //db update
                messageRepository.updateDeliverySMSStatus(messagesDTO);
                //balance update                        
                //CommonTask.updateBalance(brandId, messagesDTO.getSmsRate());
                //CommonTask.forceReload();
                logger.debug("----------- All actions for successful delivery has been completed[nexmo] : msgId : " + msgId + " -----------");
            } else {
                messagesDTO.setDlStatus(-1);
                //db update
                messageRepository.updateDeliverySMSStatus(messagesDTO);
                //CommonTask.forceReload();
                logger.error("Message sending failed [nexmo] --> deliveryErrorCode : " + deliveryErrorCode);
            }

            out.println(1);
        } catch (IOException ex) {
            logger.error("IOException in /callBack/nexmo --> " + ex);
        } catch (Exception ex) {
            logger.error("Exception in /callBack/nexmo --> " + ex);
        } finally {
            out.close();
        }
    }

    public static void main(String[] args) {
        String msgId = "123123-99999";
        String[] str = msgId.split("-");
        msgId = str[1];
        System.out.println("" + msgId);

    }

}
