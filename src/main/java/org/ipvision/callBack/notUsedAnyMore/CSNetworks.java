/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.callBack.notUsedAnyMore;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

import org.ipvision.dto.MessagesDTO;

import org.ipvision.repository.MessageRepository;
import org.ipvision.service.ISMSRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 *
 * @author IPVision Inc
 */
@WebServlet(name = "CSNetworks", urlPatterns = {"/callBack/CSNetworks"})
public class CSNetworks extends HttpServlet {

    @Autowired
    ISMSRouteService sMSRouteService;
    @Autowired
    MessageRepository messageRepository;

    private static Logger logger = Logger.getLogger("authCommunicationLogger");

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
                config.getServletContext());
    }
//    public void init(final ServletConfig config) throws ServletException {
//        super.init(config);
//        springContext = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
//        final AutowireCapableBeanFactory beanFactory = springContext.getAutowireCapableBeanFactory();
//        beanFactory.autowireBean(this);
//    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    private void doProcess(HttpServletRequest req, HttpServletResponse resp) {

        try {
            String msgId = req.getParameter("MESSID");
            String destinationAddress = req.getParameter("DESTADDR");
            String sourceAddress = req.getParameter("SOURCEADDR");
            int statusCode = Integer.valueOf(req.getParameter("STATUS"));

            logger.debug("------------- '/callBack/CSNetworks' called ---------------------");

            logger.debug("msgId --> " + msgId);
            logger.debug("destinationAddress [DESTADDR] --> " + destinationAddress);
            logger.debug("STATUS --> " + statusCode);
            logger.debug("sourceAddress [SOURCEADDR] --> " + sourceAddress);

            int brandId = sMSRouteService.getBrandIdByName("CSNetworks");

            MessagesDTO messagesDTO = messageRepository.getMessage(brandId, msgId);
            if (messagesDTO == null) {
                Thread.sleep(5000);
                messagesDTO = messageRepository.getMessage(brandId, msgId);
            }
            if (messagesDTO == null) {
                logger.error("messagesDTO not found for messageId[CSNetworks] --> " + msgId);
            } else if (statusCode == 1) {
                messagesDTO.setDlStatus(1);
                //db update

                //CommonTask().updateDB(messagesDTO);
                messageRepository.updateDeliverySMSStatus(messagesDTO);
                //balance update                        
                //CommonTask.updateBalance(brandId, messagesDTO.getSmsRate());
                logger.debug("----------- All actions for successful delivery has been completed[CSNetworks] : msgId : " + msgId + " -----------");
            } else if (statusCode > 2 && statusCode != 8) {
                messagesDTO.setDlStatus(-1);
                //db update
                //CommonTask().updateDB(messagesDTO);
                messageRepository.updateDeliverySMSStatus(messagesDTO);
                logger.error("Message sending failed [CSNetworks] --> " + getText(statusCode));
            }
        } catch (Exception e) {
            logger.error("Exception in /callBack/CSNetworks --> " + e);
        }

    }

    private String getText(int code) {
        String statusText;
        switch (code) {
            case 1:
                statusText = "Delivered";
                break;
            case 2:
                statusText = " In process";
                break;
            case 3:
                statusText = "Failed";
                break;
            case 4:
                statusText = "Deleted";
                break;
            case 5:
                statusText = "Expired";
                break;
            case 6:
                statusText = "Rejected";
                break;
            case 7:
                statusText = "Canceled";
                break;
            case 8:
                statusText = "Queued";
                break;
            case 9:
                statusText = "Orphaned";
                break;
            case 10:
                statusText = "Relayed";
                break;
            case 11:
                statusText = "Unknown";
                break;
            default:
                statusText = "Status Code mismatched";
                break;
        }
        return statusText;
    }

}
