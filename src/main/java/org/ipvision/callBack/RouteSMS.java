/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.callBack;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletConfig;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.repository.MessageRepository;
import org.ipvision.service.ISMSRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 *
 * @author reefat
 */
@WebServlet(name = "RouteSMS", urlPatterns = {"/callBack/RouteSMS"})
public class RouteSMS extends HttpServlet {

    @Autowired
    ISMSRouteService sMSRouteService;
    @Autowired
    MessageRepository messageRepository;

    private static final ArrayList<String> failedResponse = new ArrayList<>();

    static {
        failedResponse.add("UNKNOWN");
        failedResponse.add("EXPIRED");
        failedResponse.add("DELETED");
        failedResponse.add("UNDELIV");
        failedResponse.add("REJECTD");
    }
    private static Logger logger = Logger.getLogger("authCommunicationLogger");

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp);
//        super.doGet(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
                config.getServletContext());
    }

    private void doProcess(HttpServletRequest request, HttpServletResponse response) {
        try {
            logger.debug("------------- '/callBack/RouteSMS' called ---------------------");
            String smsSender = request.getParameter("sSender");
            String destinationMobileNo = request.getParameter("sMobileNo");

            /**
             * Status of the message.( 'UNKNOWN', 'ACKED', 'ENROUTE', 'DELIVRD',
             * 'EXPIRED', 'DELETED', 'UNDELIV', 'ACCEPTED', 'REJECTD')
             */
            String smsStatus = request.getParameter("sStatus");
            String msgId = request.getParameter("sMessageId");

            /**
             * Date-Time at which delivery report is received.
             *
             * Date format should be ‘yyyy-mm-dd HH:mm:ss’
             */
            String deliveryReportReceivedTime = request.getParameter("dtDone");

            /**
             * Date-Time when the message is submitted.
             *
             * Date format should be ‘yyyy-mm-dd HH:mm:ss’
             */
            String messageSubmissionTime = request.getParameter("dtSubmit");
            String perSMSCost = request.getParameter("iCostPerSms");
            String totalChargeDeducted = request.getParameter("iCharge");
            String mobileCountryCodeAndMobilenetworkCode = request.getParameter("iMCCMNC");

            logger.debug("smsSender [sSender] --> " + smsSender);
            logger.debug("destinationMobileNo [sMobileNo] --> " + destinationMobileNo);
            logger.debug("smsStatus [sStatus] --> " + smsStatus);
            logger.debug("msgId [sMessageId] --> " + msgId);
            logger.debug("deliveryReportReceivedTime [dtDone] --> " + deliveryReportReceivedTime);
            logger.debug("messageSubmissionTime --> " + messageSubmissionTime);
            logger.debug("perSMSCost --> " + perSMSCost);
            logger.debug("totalChargeDeducted --> " + totalChargeDeducted);
            logger.debug("mobileCountryCodeAndMobilenetworkCode --> " + mobileCountryCodeAndMobilenetworkCode);

            int brandId = sMSRouteService.getBrandIdByName("Route SMS(EURO)");
            MessagesDTO messagesDTO = messageRepository.getMessage(brandId, msgId);
            if (messagesDTO == null) {
                Thread.sleep(5000);
                messagesDTO = messageRepository.getMessage(brandId, msgId);
            }
            if (messagesDTO == null) {
                logger.error("messagesDTO not found for messageId[RouteSMS] --> " + msgId);
            } else if (smsStatus.equalsIgnoreCase("DELIVRD")) {
                String responseTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
                logger.debug("response Date:" + responseTime);

                messagesDTO.setReceivedDate(responseTime);
                messagesDTO.setDlStatus(1);
                //db update
                messageRepository.updateDeliverySMSStatus(messagesDTO);
                //balance update                        
                //CommonTask.updateBalance(brandId, messagesDTO.getSmsRate());
                logger.debug("----------- All actions for successful delivery has been completed[RouteSMS] : uniqueMessageId : " + msgId + " -----------");
            } else if (failedResponse.contains(smsStatus)) {
                messagesDTO.setDlStatus(-1);
                //db update
                messageRepository.updateDeliverySMSStatus(messagesDTO);
                logger.error("Message sending failed [RouteSMS] --> smsStatus : " + smsStatus);
            }
            response.setStatus(HttpServletResponse.SC_OK);

        } catch (Exception e) {
            logger.error("Exception in /callBack/RouteSMS --> " + e);
        }
    }

}
