/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.callBack;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletConfig;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.repository.MessageRepository;
import org.ipvision.service.ISMSRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 *
 * @author IPVision Inc
 */
@WebServlet(name = "MobiSMS", urlPatterns = {"/callBack/MobiSMS"})
public class MobiSMS extends HttpServlet {

    @Autowired
    ISMSRouteService sMSRouteService;
    @Autowired
    MessageRepository messageRepository;

    private static Logger logger = Logger.getLogger("authCommunicationLogger");

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
                config.getServletContext());
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    private void doProcess(HttpServletRequest req, HttpServletResponse resp) {
        PrintWriter out = null;
        try {
            out = resp.getWriter();
            String msgId = req.getParameter("id");
            String phone = req.getParameter("phone");
            String status = req.getParameter("status");
            String date = req.getParameter("date");
            String cost = req.getParameter("cost");
            String operatorid = req.getParameter("operatorid");

            logger.debug("------------- '/callBack/MobiSMS' called ---------------------");

            logger.debug("msgId [id] --> " + msgId);
            logger.debug("phone --> " + phone);
            logger.debug("deliveryStatus [status] --> " + status);
            logger.debug("date --> " + date);
            logger.debug("cost --> " + cost);
            logger.debug("operatorid --> " + operatorid);

            int statusCode = Integer.parseInt(status);
            logger.debug("status details : " + getText(statusCode));

            int brandId = sMSRouteService.getBrandIdByName("Mobi Web");
            MessagesDTO messagesDTO = messageRepository.getMessage(brandId, msgId);
            if (messagesDTO == null) {
                Thread.sleep(5000);
                messagesDTO = messageRepository.getMessage(brandId, msgId);
            }
            if (messagesDTO == null) {
                logger.error("messagesDTO not found for messageId[MobiSMS] --> " + msgId);
            } else if (statusCode == 1) {
                String responseTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
                logger.debug("response Date:" + responseTime);

                messagesDTO.setReceivedDate(responseTime);
                messagesDTO.setDlStatus(1);
                //db update
                messageRepository.updateDeliverySMSStatus(messagesDTO);
                //balance update                        
                //CommonTask.updateBalance(brandId, messagesDTO.getSmsRate());
                logger.debug("----------- All actions for successful delivery has been completed[MobiSMS] : msgId : " + msgId + " -----------");
            } else if (statusCode == 2 || statusCode == 3 || statusCode > 5) {
                messagesDTO.setDlStatus(-1);
                //db update
                messageRepository.updateDeliverySMSStatus(messagesDTO);
                logger.error("Message sending failed [MobiSMS] --> status : " + getText(statusCode));
            }

            out.println("OK");
        } catch (IOException ex) {
            logger.error("IOException in /callBack/MobiSMS --> " + ex);
        } catch (Exception ex) {
            logger.error("Exception in /callBack/MobiSMS --> " + ex);
        } finally {
            out.close();
        }
    }

    private String getText(int code) {
        String statusText;
        switch (code) {
            case 0:
                statusText = "No Status yet received";
                break;
            case 1:
                statusText = "SMS Delivered";
                break;
            case 2:
                statusText = "Failed Delivery (Erroneous Number)";
                break;
            case 3:
                statusText = "Delivery Failed (Message Expired in SMSC)";
                break;
            case 4:
                statusText = "Pending Delivery";
                break;
            case 5:
                statusText = "Expired";
                break;
            default:
                statusText = "Status Code mismatched";
                break;
        }
        return statusText;
    }

    private int getBrandIdByName(String mobi_Web) {
        return sMSRouteService.getBrandIdByName(mobi_Web);
    }

    public static void main(String[] args) {
        System.out.println("" + new MobiSMS().getBrandIdByName("Mobi Web"));
    }

}
