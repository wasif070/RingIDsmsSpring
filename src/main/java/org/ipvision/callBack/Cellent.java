/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.callBack;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.ipvision.dto.MessagesDTO;
import org.ipvision.repository.MessageRepository;
import org.ipvision.service.ISMSRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 *
 * @author IPVision Inc
 */
@WebServlet(name = "Cellent", urlPatterns = {"/callBack/Cellent"})
public class Cellent extends HttpServlet {

    @Autowired
    ISMSRouteService sMSRouteService;
    @Autowired
    MessageRepository messageRepository;
    
    private static Logger logger = Logger.getLogger("authCommunicationLogger");

    /**
     * http://IPAddress/VirtualDirectory/page.aspx?msgId=@messageId&msisdn=@msisdn&deliveryStatus=@deliveryStatus&deliveryErrorCode=@deliveryErrorCode&SubmitDate=@SubmitDate&DelvDate=@delvdate
     *
     * http://IPAddress/VirtualDirectory/page.aspx?msgId=@messageId&msisdn=@msisdn&deliveryStatus=@deliveryStatus&deliveryErrorCode=@deliveryErrorCode&SubmitDate=@SubmitDate&DelvDate=@delvdate
     */
    /**
     * http://IPAddress/VirtualDirectory/page.aspx?msgId=@messageId&msisdn;=@msisdn&deliveryStatus;=@deliveryStatus&deliveryErrorCode;=@deliveryErrorCode&SubmitDate;=@SubmitDate&DelvDate;=@delvdate
     *
     * @param req
     * @param resp
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
                config.getServletContext());
    }

    private void doProcess(HttpServletRequest req, HttpServletResponse resp) {

        PrintWriter out = null;
        try {
            logger.debug("------------- '/callBack/Cellent' called ---------------------");
            out = resp.getWriter();
//            String msgId = req.getParameter("msgId");
//            String msisdn = req.getParameter("msisdn");
//            String deliveryStatus = req.getParameter("deliveryStatus");
//            String deliveryErrorCode = req.getParameter("deliveryErrorCode");
//            String SubmitDate = req.getParameter("SubmitDate");
//            String DelvDate = req.getParameter("DelvDate");

//            http://38.108.92.154:8084/sms/callBack/Cellent?
//            msgid=@msgid&mobileno=@msisdn&status=@deliveryStatus&submitdate=@submitDate&delvdate=@delvdate&errorcode=@deliveryErrorCode
            String msgId = req.getParameter("msgid");
            String msisdn = req.getParameter("msisdn");
            String deliveryStatus = req.getParameter("deliveryStatus");
            String deliveryErrorCode = req.getParameter("deliveryErrorCode");
            String SubmitDate = req.getParameter("SubmitDate");
            String DelvDate = req.getParameter("delvDate");

            logger.debug("messageId --> " + msgId);
            logger.debug("msisdn --> " + msisdn);
            logger.debug("deliveryStatus --> " + deliveryStatus);
            logger.debug("deliveryErrorCode --> " + deliveryErrorCode);
            logger.debug("SubmitDate --> " + SubmitDate);
            logger.debug("DelvDate --> " + DelvDate);

            int brandId = sMSRouteService.getBrandIdByName("Cellent");
            String[] str = msgId.split("-");
            msgId = str[1];
            MessagesDTO messagesDTO = messageRepository.getMessage(brandId, msgId);
            if (messagesDTO == null) {
                Thread.sleep(5000);
                messagesDTO = messageRepository.getMessage(brandId, msgId);
            }
            if (messagesDTO == null) {
                logger.error("messagesDTO not found for messageId[Cellent] --> " + msgId);
            } else if (deliveryStatus.equalsIgnoreCase("DELIVRD")) {
                String responseTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
                logger.debug("response Date:" + responseTime);

                messagesDTO.setReceivedDate(responseTime);
                messagesDTO.setDlStatus(1);
                //db update
                messageRepository.updateDeliverySMSStatus(messagesDTO);
                //balance update                        
                //CommonTask.updateBalance(brandId, messagesDTO.getSmsRate());
                //CommonTask.forceReload();
                logger.debug("----------- All actions for successful delivery has been completed[Cellent] : msgId : " + msgId + " -----------");
            } else {
                messagesDTO.setDlStatus(-1);
                //db update
                messageRepository.updateDeliverySMSStatus(messagesDTO);
                //CommonTask.forceReload();
                logger.error("Message sending failed [Cellent] --> deliveryErrorCode : " + deliveryErrorCode);
            }

            out.println(1);
        } catch (IOException ex) {
            logger.error("IOException in /callBack/Cellent --> " + ex);
        } catch (Exception ex) {
            logger.error("Exception in /callBack/Cellent --> " + ex);
        } finally {
            out.close();
        }
    }

    public static void main(String[] args) {
        String msgId = "123123-99999";
        String[] str = msgId.split("-");
        msgId = str[1];
        
        System.out.println("" + msgId);

    }

}
