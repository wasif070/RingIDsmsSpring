$(document).ready(function () {
    smsRate.init();
});
var smsRate = {
    init: function () {
        smsRate.loadRate();
    },
    loadRate: function () {
        $.ajax({
            url: "GetRates",
            type: 'post',
            dataType: 'json',
            data: {
            },
            success: function (jsonData) {
                var rates = jsonData.dataRows;
                var vendors = jsonData.vendors;
                var keys = Object.keys(vendors);
                console.dir(jsonData);
                var text =
                        "<table class=\"table table-striped table-hover table-bordered\" >"
                        + "<thead>"
                        + "<tr>"
                        + "<th>"
                        + "CountryName"
                        + " <span class=\"fl_right\">"
                        + " <a class=\"asc\" href=\"javascript: setColumnForSorting('1','0')\"></a>"
                        + "<a href=\"javascript: setColumnForSorting('1','1')\" class=\"desc\"></a>"
                        + " </span>"
                        + "</th>";
                $.each(vendors, function (i, val) {
                    text = text + "<th>" + val + "</th>";
                });
                text = text + "</tr>"
                        + " </thead>"
                        + "  <tbody>";
                $.each(rates, function (key, value) {
                    text = text + " <tr class=\"even\">   "
                            + " <td align=\"center\">" + value.countryName + "</td>";
                    $.each(vendors, function (i, val) {
                        if (i in value.brands) {
                            text = text+ " <td align=\"center\" >" + value.brands[i] + "</td>";
                        } else {
                            text = text+ " <td align=\"center\" >" + "--" + "</td>";
                        }
                    });
                    //text = text + " <td>" + value.brands[0] + "</td>";
                    text = text + " </tr>";
                });
                text = text   + "</tbody>"
                        + "  </table>";
                $("#dataTableContainer").append(text);
            }
            , error: function (e, m, s) {
                alert(e.responseText);
            }
        });
    }
};