function paginatedInfo(event, operation) {
	var searchKey = document.getElementById('searchKeyField');
	var selectItems = document.getElementById('selectEmployeeField');
	var currPage = document.getElementById('currentPageField');

	document.getElementById('operationField').value = operation;
	document.getElementById('goPageField').value = document
			.getElementById("goPageSelectorId").value;
	document.getElementById('paginationForm').submit();
}