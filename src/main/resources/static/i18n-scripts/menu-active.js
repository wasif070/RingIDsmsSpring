//jQuery(document).ready(function(){
//	jQuery('ul li a').click(function(){
//	  alert("Add --> Active")
//    jQuery('li a').removeClass("active");
//	  jQuery(this).addClass("active");
//});
//});

$(document).ready(function() {
	$(function() {
		function stripTrailingSlash(str) {
			if (str.substr(-1) == '/') {
				return str.substr(0, str.length - 1);
			}
			return str;
		}

		var url = window.location.pathname;
		var activePage = stripTrailingSlash(url);

		$('ul li a').each(function() {
			var currentPage = stripTrailingSlash($(this).attr('href'));

			if (activePage == currentPage) {
				$(this).parent().addClass('active');
				$(this).parents('li').addClass('active');
			}
		});
	});
});